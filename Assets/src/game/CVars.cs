
using System;
using UnityEngine;


public class CVars
{
    private CPlayer mPlayer;
    public CNetworkManager mNetworkManager;
    public CMainClient mMainClient;
    public CMainServer mMainServer;

    //private CConvertInfluenceMap mMinionsInfluenceMap;
    //private CConvertInfluenceMap mEnemiesInfluenceMap;
    //private CHud mHud;
    //private CMinimap mMinimap;

    private static CVector2D mMaxBounds = new CVector2D();
    private static CVector2D mMinBounds = new CVector2D();
    private static CVars mInstance;
    private static CMap mMap;


    private CCanvas mCanvas;

    //TENGO QUE CHEQUEAR CUANTOS TIENEN EL MISMO X Y CUANTOS TIENEN EL M ISMO Y PARA SABER EL TAMANIO DEL MAPA!
    public CVars()
    {
        mInstance = this;
    }

    public void init()
    {

    }

    private void registerSingleton()
    {
    }

    public static CVars inst()
    {
        return mInstance;
    }

    public CVector2D MinBounds()
    {
        if (mMinBounds.x != 0 || mMinBounds.y != 0)
            return mMinBounds;
        else
        {
            mMinBounds.x = 0;
            mMinBounds.y = 0;
            return mMinBounds;
        }
    }
    public CVector2D MaxBounds()
    {
        if (mMaxBounds.x != 0 || mMinBounds.y != 0)
            return mMaxBounds;
        else
        {
            mMaxBounds.x = CVars.inst().getMap().getWorldWidth();
            mMaxBounds.y = CVars.inst().getMap().getWorldHeight();
            return mMaxBounds;
        }
    }


    // Getters & Setters
    public void setPlayer(CPlayer aPlayer)
    {
        mPlayer = aPlayer;
    }
    public CPlayer getPlayer()
    {
        return mPlayer;
    }
    public void setMap(CMap aMap)
    {
        mMap = aMap;
    }

    public CMap getMap()
    {
        return mMap;
    }


    public void setCanvas(CCanvas aCanvas)
    {
        mCanvas = aCanvas;
    }


    public CCanvas getCanvas()
    {
        return mCanvas;
    }

    public void setNetworkManager(CNetworkManager aNetworkManager)
    {
        mNetworkManager = aNetworkManager;
    }


    public CNetworkManager getNetworkManager()
    {
        return mNetworkManager;
    }


    public CMainClient getMyNetworkClientData()
    {
        return mMainClient;
    }

    public void setMyNetworkClientData(CMainClient aClient)
    {
        mMainClient = aClient;
    }


    public CMainServer getMyNetworkServerData()
    {
        return mMainServer;
    }

    public void setMyNetworkServerData(CMainServer aServer)
    {
        mMainServer = aServer;
    }




}