
public class CMapConstants
{
    public const uint PLAYER_START = 130;
    public const uint HOUSE_DOOR = 189;
    public const uint HOUSE = 190;
    public const uint PRIEST = 150;
    public const uint CHURCH = 210;
    public const uint ENEMY_PATH = 170;

    //Layers
    public const string CONVERTED = "converted";
    public const string ABOVE = "above";
    public const string PATHS = "paths";
    public const string OBJECTS = "objects";
    public const string ACCENTS = "accents";
    public const string BUILD2 = "build2";
    public const string BUILD1 = "build1";
    public const string FLOOR = "floor";

    public CMapConstants()
    {

    }

}
