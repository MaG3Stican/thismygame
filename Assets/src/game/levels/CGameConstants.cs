
public class CGameConstants
{
    public float scale = 0.5f;
    public const bool DEBUG = false;
    public const int FPS = 30;

    public const uint STAGE_WIDTH = 1000;
    public const uint STAGE_HEIGHT = 600;
    public const uint TILE_WIDTH = 64;
    public const uint TILE_HEIGHT = 64;

    // Positions of HUD
    // Minions Counter
    public const int MINIONS_COUNTER_X = 10;
    public const uint MINIONS_COUNTER_Y = STAGE_HEIGHT - 100;

    public const string SERVER_NAME = "thisMyStory";

    public static double CONVERSION_INTERVAL = CGameConstants.FPS * 0.3;

    public CGameConstants()
    {

    }

}
