
using System;
using System.Collections.Generic;
public class CLevelState : CGameState
{

    internal CPlayer mPlayer;
    internal CItemManager mItemManager;
    internal CGraphicsManager mGraphicManager;
    internal CProjectileManager mProjectileManager;
    internal CParticleManager mParticleManager;
    internal CHudManager mHudManager;

    // STATES
    public const uint START = 0;
    public const uint PLAY = 1;
    public const uint PAUSE = 2;
    public const uint WIN = 3;
    public const uint LOSE = 4;


    private List<List<int>> mPaths;
    public CLevelState()
        : base()
    {
        setState(START);

    }
    public void loadMap(object aMap)
    {
        // Map
        //mMap = new CTileMap();
        //CGame.inst().setMap(mMap);
        //CTileSet tTileSet = new CTileSet();
        //tTileSet.setTileSetMC(CAssets.TILES());
        //mMap.setTileSet(tTileSet);

        //var mTerrains:Vector.<String> = new Vector.<String>();
        //mTerrains.push(CTerrains.WALKABLE);
        //mTerrains.push(CTerrains.ENEMY_PATH);
        //mTerrains.push(CTerrains.PLATFORM);

        //mMap.loadTileMap(new XML(new aMap), mTerrains);

        ////Layers
        //mMap.loadMap(CMapConstants.FLOOR);
        //mMap.loadMap(CMapConstants.ACCENTS);
        //CLayer.addLayer('areas');
        //CLayer.addLayer('objects');
        //CLayer.addLayer('auras');
        //mMap.loadMap(CMapConstants.ABOVE);
        //mMap.loadMap(CMapConstants.CONVERTED);

        //var tFloorMap:Vector.<Vector.<CTile>> = mMap.getLayerByName(CMapConstants.FLOOR).getTileMap();
        //var tWalkableMap:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
        //for (var i:int = 0; i < tFloorMap.length; i++)
        //{
        //    var tRow:Vector.<int> = new Vector.<int>();
        //    for (var j:int = 0; j < tFloorMap[i].length; j++)
        //    {
        //        if (tFloorMap[i][j].isWalkable())
        //            tRow.push(0);
        //        else
        //            tRow.push(-1);
        //    }
        //    tWalkableMap.push(tRow);
        //}
        //mPaths = mMap.getLoader().getMapLayer(CMapConstants.PATHS);
    }

    public void loadStage()
    {
        mPlayer.setPosXYZ(50, 50);

        ////CVars.inst().getMinimap().addSimpleObject(mPlayer, 0xFF0000);
        //CVars.inst().setHud(mHud);
    }

    override public void init()
    {
        //new CConstructManager();
        reset();

    }

    public void reset()
    {
        int i;

        //CConstructManager.inst().flushObjects();
        //mPlayer.reset();

        //var tObjectsLayer:CLayer = CLayer.getLayerByName('objects');

        //CGraphicsManager.getInstance().flushConvertibles();

        //var tHouse:CHouse;
        //var tChurch:CChurch;
        //var tMinion:CMinion;
        //var tPriest:CPriest;
        //var tObjectsMap:Vector.<Vector.<int>> = mMap.getLoader().getMapLayer(CMapConstants.OBJECTS);
        //for (i = 0; i < tObjectsMap.length; i++)
        //{
        //    for (var j:int = 0; j < tObjectsMap[i].length; j++)
        //    {
        //        if (tObjectsMap[i][j] == CMapConstants.PLAYER_START)
        //        {
        //            mPlayer.setX((j + 0.5) * CGameConstants.TILE_WIDTH - mPlayer.getWidth() * 0.5);
        //            mPlayer.setY((i + 0.5) * CGameConstants.TILE_HEIGHT - mPlayer.getHeight() * 0.5);
        //        }
        //        else if (tObjectsMap[i][j] == CMapConstants.PRIEST)
        //        {
        //            tPriest = new CPriest(tObjectsLayer);
        //            tPriest.setXY((j + 0.5) * CGameConstants.TILE_WIDTH - 20 * 0.5, (i + 0.5) * CGameConstants.TILE_HEIGHT - 30 * 0.5);
        //            CGraphicsManager.getInstance().addMinion(tPriest);
        //        }
        //    }
        //}

        //// Load all houses and churches
        //new CBuildingsMap(tObjectsMap);

        //// Minions
        //for (i = 0; i < 0; i++)
        //{
        //    tMinion = new CMinion(tObjectsLayer);
        //    tMinion.setPosXYZ(CMath.randInt(50, 300 - tMinion.getWidth()), CMath.randInt(50, 300 - tMinion.getHeight()));
        //    CGraphicsManager.getInstance().addMinion(tMinion);
        //}

        setState(START);
    }

    public List<List<int>> getPaths()
    {
        return mPaths;
    }
    public bool winCondition()
    {
        return true;
        //CGraphicsManager.getInstance().getBuildings().countConverted();
        //return CGraphicsManager.getInstance().getBuildings().getConvertedLength() == CGraphicsManager.getInstance().getBuildings().getLength();
    }
    public bool loseCondition()
    {
        return mPlayer.isDead();
    }

    override public void setState(uint aState)
    {
        base.setState(aState);
    }

    override public void update()
    {

        if (getState() == START)
        {
            //Run start sequence
            //setState(PLAY);
        }
        else if (getState() == PLAY)
        {
            if (winCondition())
            {
                setState(WIN);
                return;
            }
            if (loseCondition())
            {
                setState(LOSE);
                return;
            }


        }
        else if (getState() == PAUSE)
        {

        }
        else if (getState() == WIN)
        {
            Console.WriteLine("You won!");
            reset();
            return;
        }
        else if (getState() == LOSE)
        {
            Console.WriteLine("You lost");
            reset();
            return;
        }
    }

    override public void render()
    {
        //    mCamera.render();
        //    mPlayer[0].render();
        //mMap.render();
        //mHud.render();
        //CGraphicsManager.getInstance().render();
        //CHelper.sortChildrenByY(CLayer.getLayerByName("objects"));
    }

    override public void destroy()
    {
        //mPlayer.destroy();
        //mMap.destroy();
        //mHud.destroy();
        //CGraphicsManager.getInstance().destroy();
        //CConstructManager.inst().destroy();
        //super.destroy();
    }

}