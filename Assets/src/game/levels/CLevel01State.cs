﻿using UnityEngine;
class CLevel01State : CLevelState
{

    bool initialized = false;

    public CLevel01State()
    {

    }

    public override void init()
    {
        if (initialized == false)
        {
            base.init();

            mProjectileManager = new CProjectileManager();
            mGraphicManager = new CGraphicsManager();
            mParticleManager = new CParticleManager();
            mHudManager = new CHudManager();


            CMap aMap = new CMap(CLayer.getLayer(ELayer.Ground), CAssets.TILESET_XML, CAssets.TILESET);
            CVars.inst().setMap(aMap);
            aMap.init(new CMapGenerationStrategy(10));
            //aMap.initUnityMap(CGameConstants.TILE_WIDTH, GameObject.Find("Map"), new CMapGeneration(40));


            mPlayer = new CPlayer(CLayer.getLayer(ELayer.Player));
            CVars.inst().setPlayer(mPlayer);
            mGraphicManager.addGraphic(mPlayer, EGraphics.Player);
            mGraphicManager.addToSkipList(EGraphics.Player);

            initialized = true;
            new CCamera(CVars.inst().getMap().getWorldWidth(), CVars.inst().getMap().getWorldHeight());

            CCamera.inst().follow(mPlayer);

            mParticleManager.initDummy();

            new CCanvas();

            mItemManager = new CItemManager();
            //mItemManager.addItem(new CTorch(CLayer.getLayer(ELayer.Artifacts), CVars.inst().MinBounds(), CVars.inst().MinBounds(), null, true), EItems.Torch);

            //randomStuff();
            for (var i = 0; i < 10; i++)
            {
                mGraphicManager.addGraphic(new CPeasant(CMap.inst().getSector(CMap.inst().randomXInMap(), CMap.inst().randomYInMap()), CLayer.getLayer(ELayer.Player)), EGraphics.Enemies);
            }
            //mGraphicManager.updateRenderOnce(EGraphics.Tiles);

            //CLight.CastLight(true, 30, EShapes.CIRCLE, new CVector2D(-70, 70));



            new CLegendText("This is a dark legend");
            //new CNetworkHud();

            new CStorm();


        }
    }

    public void randomStuff()
    {
        var howManyShapes = 10;
        for (int i = 0; i < howManyShapes; i++)
        {
            //CGenericGraphic gameObject = new CGenericGraphic();
            //var shape = CMath.randInt(0, 2);
            //if (shape == 0)
            //    gameObject.setGrahic(CCircle.DrawCircle(true, 30, EShapes.CIRCLE, CMath.randIntVec(CVars.inst().MinBounds(), CVars.inst().MaxBounds())));
            //else if (shape == 1)
            //    gameObject.setGrahic(CCircle.DrawCircle(true, 30, EShapes.TRIANGLE, CMath.randIntVec(CVars.inst().MinBounds(), CVars.inst().MaxBounds())));
            //else if (shape == 2)
            //    gameObject.setGrahic(CCircle.DrawCircle(true, 30, EShapes.RECT, CMath.randIntVec(CVars.inst().MinBounds(), CVars.inst().MaxBounds())));

            //gameObject.setVelX(CMath.randInt(1, 4));
            //gameObject.setVelY(CMath.randInt(1, 4));
            //gameObject.setLayer(CLayer.getLayer(ELayer.Artifacts));
            //gameObject.setShape(EShapes.RECT);
            //gameObject.setBounds(CVars.inst().MinBounds(), CVars.inst().MaxBounds());
            //gameObject.setBoundAction(EBoundBehaviour.WRAP);
            //gameObject.rotate(80);
            //mGraphicManager.addGraphic(gameObject, EGraphics.Shapes);

        }
    }

    public override void update()
    {
        base.update();
        CCamera.inst().update();
        mGraphicManager.update();
        mItemManager.update();
        mProjectileManager.update();
        mHudManager.update();
        CParticleManager.inst().update();
        CVars.inst().getMyNetworkClientData().clientUpdate();
        CGame.inst().setGameTime(CGame.inst().getGameTime() + 1);
        CStorm.inst().update();
    }

    public override void render()
    {
        base.render();
        mGraphicManager.render();
        CCamera.inst().render();
        mProjectileManager.render();
        mItemManager.render();
        mHudManager.render();
        CParticleManager.inst().render();
        CCanvas.inst().render();

        CStorm.inst().render();

        base.render();
    }

    public override void destroy()
    {
        base.destroy();
    }
}