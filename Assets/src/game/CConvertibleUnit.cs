﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum EConvertibleState
{

}

public class CConvertibleUnit : CGenericGraphic
{

    private Color CONVERTED_COLOR = Color.red;
    private Color NOT_CONVERTED_COLOR = Color.green;
    private Color mMyColor = Color.gray;

    private int mConversion;
    private int mMaxConversion;
    private bool mConverted;
    private int mCureRate;
    private int mConvertRate;
    private int mRange;
    private int mTime;

    private int mConvertTime = 0;
    private bool mBeingConverted = false;
    private CRect mMinimapToken;


    public CConvertibleUnit()
    {
        mConversion = 0;
        mConverted = false;
        mTime = 0;
    }


    public void registerWithMinimap()
    {
        CMinimap.inst().addObject(getShape(), mMyColor);
    }

    public void unregisterFromMinimap()
    {
        CMinimap.inst().removeObject(getShape());
    }

    public bool isBeingConverted()
    {
        return mBeingConverted;
    }

    public bool isConverted()
    {
        return mConverted;
    }

    public void setConverted(Boolean aConverted)
    {
        mConverted = aConverted;
        if (mConverted)
        {
            mMyColor = CONVERTED_COLOR;
        }
        else
        {
            mMyColor = NOT_CONVERTED_COLOR;
        }
    }

    public int getConversion()
    {
        return mConversion;
    }

    public void setConversion(int aConversion)
    {
        mConversion = aConversion;
    }

    public void setConvertRate(int aConvertRate)
    {
        mConvertRate = aConvertRate;
    }

    public int getConvertRate()
    {
        return mConvertRate;
    }

    public void setCureRate(int aCureRate)
    {
        mCureRate = aCureRate;
    }

    public int getCureRate()
    {
        return mCureRate;
    }

    public void setMaxConversion(int aMaxConversion)
    {
        mMaxConversion = aMaxConversion;
    }

    public int getMaxConversion()
    {
        return mMaxConversion;
    }

    public void applyConversion(int aConvertRate)
    {
        mBeingConverted = true;
        if (isConverted())
            setConversion(getConversion() - aConvertRate);
        else
            setConversion(getConversion() + aConvertRate);
    }

    public void convert(CConvertibleUnit aConvertible)
    {
        if (mConvertTime >= CGameConstants.CONVERSION_INTERVAL)
        {
            aConvertible.applyConversion(getConvertRate());
            mConvertTime = 0;
        }
        else
            mConvertTime++;
    }

    public void updateConversion()
    {
        if (mConverted && mTime > CGameConstants.FPS * 0.5)
        {
            mConversion += mCureRate;
            mTime = 0;
        }
        else if (!mConverted && mTime > CGameConstants.FPS * 0.5)
        {
            mConversion -= mCureRate;
            mTime = 0;
        }

        if (mConversion >= mMaxConversion)
        {

            mConversion = mMaxConversion;
            if (!mConverted)
            {
                setConverted(true);
            }
        }
        else if (mConversion <= 0)
        {
            mConversion = 0;
            if (mConverted)
                setConverted(false);
        }
        mTime++;
    }

    override public void update()
    {
        base.update();
        updateConversion();
        mBeingConverted = false;
    }

    public void setState(EConvertibleState aNumber)
    {
        setTimeState(0);
    }

    override public void destroy()
    {
        base.destroy();
        setDead(true);
        mMinimapToken = null;
    }

}