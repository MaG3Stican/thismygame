﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public enum EPeasantState
{
    IDLE = 0,
    WALKING = 1,
    BEING_CONVERTED = 2,
    CONVERTING = 3
}

class CPeasant : CGenericGraphic
{
    private CDialogBubble mDialogue;
    private EPeasantState mState = EPeasantState.IDLE;

    public CPeasant(CSector aSector, CLayer aLayer)
    {

        setShape(EShapes.RECT);
        setWidth(145);
        setHeight(145);
        setPivot(65, 145);

        setFeet(new CRect(40, 40, 60, 100));

        //we add half the width and half the heights so that the unit can move until the center of its graphic shape
        setBounds(aSector.getRect().x, aSector.getRect().y, aSector.getRect().width + aSector.getRect().x, aSector.getRect().height + aSector.getRect().y);
        setPos(aSector.getRandomPointInside(getWidth(), getHeight()));

        setGraphic(CHelper.get2DReadyObject("Peasant" + getId()));
        var r = getGraphic().GetComponent<SpriteRenderer>();

        mAnimation.loadAnimation(r, CAssets.PEASANT_BEING_CONVERTED, EAnimationSpeed.FAST_ANIMATION);
        mAnimation.loadAnimation(r, CAssets.PEASANT_WALKING, EAnimationSpeed.FAST_ANIMATION);
        mAnimation.loadAnimation(r, CAssets.PEASANT_IDLE, EAnimationSpeed.FAST_ANIMATION);

        setBoundAction(EBoundBehaviour.STOP);
        setLayer(aLayer);

        //patrolling data
        CGenericBehaviour.CBehaviourProperties beh = new CGenericBehaviour.CBehaviourProperties();
        beh.aBehaviourArea = new CRect(Convert.ToUInt32(aSector.getRect().width + aSector.getRect().x), Convert.ToUInt32(aSector.getRect().height + aSector.getRect().y), aSector.getRect().x, aSector.getRect().y);
        beh.aChaseSpeed = 1f;
        beh.aChaseWaitTime = 2f;
        beh.aPatrolSpeed = 1f;
        beh.aPatrolWaitTime = 1f;
        beh.aRangeOfAttack = 3 * CGameConstants.TILE_HEIGHT;
        beh.aVisionRange = 6 * CGameConstants.TILE_HEIGHT; ;

        addBehaviour(new CPatrolAndFollow(this, beh, CVars.inst().getPlayer(), CPatrolAndFollow.EActionsType.Close));


        mFlipMe = true;

        mDebug = false;

        mCollides = false;

        mDialogue = new CDialogBubble(getPos(), CDialogBubble.pickRandomDialogue());

        mMouseTrigger = EMouseTriggers.mTriggersDialogueOnHover;


    }

    public override void update()
    {

        if (isActive())
        {
            flipLogic();

            var beh = getBehaviours();
            for (int i = 0; i < beh.Count; i++)
            {
                beh[i].update();
            }
            mDialogue.setPos(new CVector3D(getX(), getY(), mDialogue.getZ()));

            mAnimation.update();
            base.update();
        }
    }

    public override void setGraphicState(EGraphicState aGraphicState)
    {
        if (aGraphicState == EGraphicState.MOVINGTOWARDSPOINT)
            setState(EPeasantState.WALKING);
        else if (aGraphicState == EGraphicState.NONE)
            setState(EPeasantState.IDLE);

        base.setGraphicState(aGraphicState);
    }

    private void setState(EPeasantState aState)
    {
        mState = aState;
        setTimeState(0);

        if (aState == EPeasantState.WALKING)
            mAnimation.play(CAssets.PEASANT_WALKING.parsedName(), true, false);
        else if (aState == EPeasantState.IDLE)
            mAnimation.play(CAssets.PEASANT_IDLE.parsedName(), true, false);
        else if (aState == EPeasantState.BEING_CONVERTED)
            mAnimation.play(CAssets.PEASANT_IDLE.parsedName(), true, false);
        else if (aState == EPeasantState.CONVERTING)
            mAnimation.play(CAssets.PEASANT_IDLE.parsedName(), true, false);

    }


    public override void render()
    {
        mAnimation.render();
        base.render();
    }

    public override void destroy()
    {
        base.destroy();
    }
}
