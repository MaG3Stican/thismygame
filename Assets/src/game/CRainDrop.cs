﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CRainDrop : CGenericGraphic
{
    public CRainDrop()
    {
        setGraphic(CHelper.get2DReadyObject("RainDrop" + getId()));
        var r = getGraphic().GetComponent<SpriteRenderer>();
        mAnimation.loadAnimation(r, CAssets.RAIN_DROP, EAnimationSpeed.FAST_ANIMATION);

        mAnimation.play(CAssets.RAIN_DROP.parsedName(), true);

        setShape(EShapes.RECT);
        setWidth(CGameConstants.TILE_WIDTH);
        setHeight(CGameConstants.TILE_HEIGHT);
    }

    public override void update()
    {
        mAnimation.update();
        base.update();
    }

    public override void render()
    {
        mAnimation.render();
        base.render();
    }

    public override void destroy()
    {
        base.destroy();
    }
}
