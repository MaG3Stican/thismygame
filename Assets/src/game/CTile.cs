﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CGameTile : CGenericTile
{
    public CGameTile(CLayer aLayer, GameObject aGraphic)
        : base (aLayer,aGraphic)
    {
        CGraphicsManager.inst().addGraphic(this, EGraphics.Tiles);
    }
}
