﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CTorch : CGenericItem
{
    private bool mFollowPlayer = false;

    public CTorch(CLayer aLayer, CVector2D aMin, CVector2D aMax, CVector2D aPos = null, bool aFollowPlayer = false)
        : base()
    {

        // setGraphic(mAnimation.loadAnimation("Sprites/torch/PTorch", EAnimationSpeed.VERY_SLOW_ANIMATION));

        setLayer(aLayer);
        setBounds(aMin, aMax);
        mFollowPlayer = aFollowPlayer;
        setPivot(10, 20);
        setWidth(32);
        setHeight(64);
        setShape(EShapes.RECT);

        //CParticle.setParticleSystem(getGraphic(), getPivot());
    }

    public override void update()
    {
        if (mFollowPlayer)
        {
            var pos = CVars.inst().getPlayer().getPos();
            setXY(pos.x + getPivot().x, pos.y + getPivot().y);
        }

        base.update();
    }

    public override void render()
    {
        base.render();
    }

    public override void destroy()
    {
        base.destroy();
    }
}
