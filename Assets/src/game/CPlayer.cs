
using System;
using System.Collections.Generic;
using UnityEngine;

public enum EPlayerStates
{
    REBIRTH = 0,
    IDLE = 1,
    WALKING = 2,
    DYING = 3,
    CONVERTING = 4,
    SENDING = 5
}



public class CPlayer : CGenericPlayer
{
    EPlayerStates mState = EPlayerStates.IDLE;


    private const double ANIM_DELAY = CGameConstants.FPS * 0.1;

    private const uint WIDTH = 192;
    private const uint HEIGHT = 192;
    private const int PIVOT_X = 96;
    private const int PIVOT_Y = 192;

    private const int WALKING_SPEED = 7;

    private uint mViewDistance = 0;

    public CPlayer(CLayer aLayer)
    {
        setShape(EShapes.RECT);

        setPivot(PIVOT_X, PIVOT_Y);
        setWidth(WIDTH);
        setHeight(HEIGHT);

        setBounds(0, 0, CMap.inst().getWorldWidth(), CMap.inst().getWorldHeight());

        setFeet(new CRect(40, 40, 60, 144));

        setGraphic(CHelper.get2DReadyObject("Player"));

        var r = getGraphic().GetComponent<SpriteRenderer>();

        mAnimation.loadAnimation(r, CAssets.PLAYER_DEMON_CONVERTING, EAnimationSpeed.NORMAL_ANIMATION);
        mAnimation.loadAnimation(r, CAssets.PLAYER_DEMON_DYING, EAnimationSpeed.NORMAL_ANIMATION);
        mAnimation.loadAnimation(r, CAssets.PLAYER_DEMON_IDLE, EAnimationSpeed.NORMAL_ANIMATION);
        mAnimation.loadAnimation(r, CAssets.PLAYER_DEMON_SENDING, EAnimationSpeed.NORMAL_ANIMATION);
        mAnimation.loadAnimation(r, CAssets.PLAYER_DEMON_WALK, EAnimationSpeed.NORMAL_ANIMATION);


        setLayer(aLayer);
        setState(EPlayerStates.IDLE);

        setBoundAction(EBoundBehaviour.STOP);
        activate();
        setSpeed(WALKING_SPEED);

        setXY(20, 20);

        mFlipMe = true;

        mAbilities.Add(new CControlAbility(getPos(), getPivot()));

        mCollides = true;
    }


    public void reset()
    {
        setState(EPlayerStates.REBIRTH);
        setDead(false);
        //move to closes respawn
    }




    override public void update()
    {
        flipLogic();


        if (shootPressed())
        {
            var angle = (int)lookAtGetAngle(getTarget());
            // new CGenericProjectile(CHelper.get2DReadyObject("death_ball", "Sprites/spells/death_ball"), getPos(), angle, 2, CLayer.getLayer(ELayer.Projectiles), 30);
        }
        // State machine
        if (mState == EPlayerStates.IDLE)
        {
            if (upPressed() || downPressed() || leftPressed() || rightPressed())
            {
                setState(EPlayerStates.WALKING);
            }
        }
        else if (getState() == EPlayerStates.WALKING)
        {
            if (!upPressed() && !downPressed() && !leftPressed() && !rightPressed())
            {
                setState(EPlayerStates.IDLE);
            }
        }
        else if (getState() == EPlayerStates.DYING)
        {
            if (mAnimation.isEnded())
            {
                reset();
            }
        }


        manageStates();
        mAnimation.update();
        base.update();
    }

    override public void render()
    {
        base.render();
        mAnimation.render();
    }

    public void setState(EPlayerStates aState)
    {
        setTimeState(0);
        mState = aState;

        if (getState() == EPlayerStates.IDLE)
        {
            mAnimation.play(CAssets.PLAYER_DEMON_IDLE.parsedName(), true);
            stopMove();
        }
        else if (getState() == EPlayerStates.WALKING)
        {
            mAnimation.play(CAssets.PLAYER_DEMON_WALK.parsedName(), true);
        }
        else if (getState() == EPlayerStates.CONVERTING)
        {
            mAnimation.play(CAssets.PLAYER_DEMON_CONVERTING.parsedName(), true);
            stopMove();
        }
        else if (getState() == EPlayerStates.DYING)
        {
            mAnimation.play(CAssets.PLAYER_DEMON_DYING.parsedName(), true);
            stopMove();
        }
        else if (getState() == EPlayerStates.REBIRTH)
        {
            mAnimation.play(CAssets.PLAYER_DEMON_DYING.parsedName(), true, true);
            stopMove();
        }
    }

    private void manageStates()
    {

        if (getState() == EPlayerStates.IDLE)
        {
            stopMove();
        }
        else if (getState() == EPlayerStates.WALKING)
        {
            if (!upPressed() && !downPressed())
                setVelY(0);
            if (!leftPressed() && !rightPressed())
                setVelX(0);

            if (upPressed())
            {
                setVelY(-WALKING_SPEED);
            }
            else if (downPressed())
            {
                setVelY(WALKING_SPEED);
            }
            if (leftPressed())
            {
                setVelX(-WALKING_SPEED);
            }
            else if (rightPressed())
            {
                setVelX(WALKING_SPEED);
            }
        }
        else if (getState() == EPlayerStates.CONVERTING)
        {
            stopMove();
        }
        else if (getState() == EPlayerStates.DYING)
        {

            stopMove();
        }



    }












    // KEYS
    public bool upPressed()
    {
        return CKeyPoll.pressed(KeyCode.W) || CKeyPoll.pressed(KeyCode.UpArrow);
    }

    public bool downPressed()
    {
        return CKeyPoll.pressed(KeyCode.S) || CKeyPoll.pressed(KeyCode.DownArrow);
    }

    public bool leftPressed()
    {
        return CKeyPoll.pressed(KeyCode.A) || CKeyPoll.pressed(KeyCode.LeftArrow);
    }

    public bool rightPressed()
    {
        return CKeyPoll.pressed(KeyCode.D) || CKeyPoll.pressed(KeyCode.RightArrow);
    }

    public bool shootPressed()
    {
        if (base.interactionIsPressed() && base.interactionIsArrow())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public EPlayerStates getState()
    {
        return mState;
    }


    // Getters & Setters
    public void setViewDistance(uint aViewDistance)
    {
        mViewDistance = aViewDistance;
    }

    public uint getViewDistance()
    {
        return mViewDistance;
    }


    private CVector3D getTarget()
    {
        return new CVector3D(CMouse.getMouseX(), CMouse.getMouseY());
    }


    override public CVector3D getCenter()
    {
        return new CVector3D(getX(), getY() - getHeight() * 0.5);
    }

    public bool isNormalState()
    {
        return getState() != EPlayerStates.DYING;
    }

}
