﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CMain : MonoBehaviour
{

    //before instantiating
    void Awake()
    {

    }
    //after being isntantiated
    void OnEnable()
    {
    }
    //on first frame execution
    void Start()
    {


        new CGame();
        new CVars();
        new CMouse();

        CGame.inst().setFPS(60);

        var result1 = Instantiate(new GameObject()) as GameObject;
        var clientcom = result1.AddComponent<CMainClient>();
        clientcom.init(ENetworkMode.local, "", CGameConstants.SERVER_NAME, EClientType.real);


        var result2 = Instantiate(new GameObject()) as GameObject;
        var servercomp = result2.AddComponent<CMainServer>();
        servercomp.init(CGameConstants.SERVER_NAME, Guid.NewGuid().ToString());



        var state = new CLevel01State();
        CGame.inst().setState(state);
        //var hud = new CPlayingHud();
        //CGame.inst().setHud(hud);
        CGame.inst().getState().init();
    }
    //when the application is paused
    void OnApplicationPause()
    {

    }
    //more frecuent than update, it executes multiple times per frame
    void FixedUpdate()
    {
        CMouse.inst().update();
        CMouse.inst().render();
    }
    //executed once per frame
    void Update()
    {
        CGame.inst().update();
        CGame.inst().render();
    }
    //executed after every update , this is where to draw stuff
    void LateUpdate()
    {
        //draw

    }
    void OnGUI()
    {
        //CGame.inst().getHud().update();
        //CGame.inst().getHud().render();
    }
    void OnDestroy()
    {

    }

}