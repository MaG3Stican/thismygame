﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CMiniMapObject : CGenericGraphic
{
    private CShape reference;

    public CMiniMapObject(CShape aShape, Color aColor)
    {
        setGraphic(CShape.DrawShape(true, 0, EShapes.POLYGON, new CVector3D(), aColor, CLayer.getLayer(ELayer.MinimapObject)));
    }

    public override void update()
    {

        base.update();
    }

    public override void render()
    {
        base.render();
    }
}