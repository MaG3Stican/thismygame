﻿

using System;
using System.Collections.Generic;
using System.Linq;

public class CHudManager
{
    // Singleton.
    private static CHudManager mInst = null;

    // List of Items to be managed.
    private Dictionary<EHudItems, CVectorExt> mHudItems = new Dictionary<EHudItems, CVectorExt>();
    List<EHudItems> mSkipList = new List<EHudItems>();

    public CHudManager()
    {
        registerSingleton();
    }

    public static CHudManager inst()
    {
        return mInst;
    }

    private void registerSingleton()
    {
        if (mInst == null)
        {
            mInst = this;
        }
        else
        {
            throw new Exception("Cannot create another instance of Singleton CItemManager.");
        }
    }

    public int getCount(EHudItems aItem)
    {
        return mHudItems[aItem].arr.Count;
    }

    public void update()
    {
        for (int index = 0; index < mHudItems.Count; index++)
        {
            var item = mHudItems.ElementAt(index);
            item.Value.update();
        }
    }

    public CGameObject collides(CGameObject aGameObject, EHudItems aItem)
    {
        return mHudItems[aItem].collides(aGameObject);
    }

    public void flush(EHudItems aItem)
    {
        mHudItems[aItem].destroy();
    }

    //if nothing is specified, removes first object of array
    private void removeItem(EHudItems aItem, CGenericGraphic aObject = null)
    {
        mHudItems[aItem].remove(aObject);
    }

    public void render()
    {
        for (int index = 0; index < mHudItems.Count; index++)
        {
            var item = mHudItems.ElementAt(index);
            item.Value.render();
        }

        activateNearbyGraphics();
    }

    public void addHudItem(CGenericGraphic aObject, EHudItems aHudItem)
    {
        if (mHudItems.ContainsKey(aHudItem))
        {
            mHudItems[aHudItem].append(aObject);
        }
        else
        {
            mHudItems.Add(aHudItem, new CVectorExt());
            mHudItems[aHudItem].append(aObject);
        }
    }

    public void destroy()
    {
        if (mInst != null)
        {
            for (int index = 0; index < mHudItems.Count; index++)
            {
                var item = mHudItems.ElementAt(index);
                item.Value.destroy();
            }
        }
    }



    public void activateNearbyGraphics()
    {
        for (int index = 0; index < mHudItems.Count; index++)
        {
            var item = mHudItems.ElementAt(index);

            if (!mSkipList.Contains(item.Key))
            {
                item.Value.checkIfRenderNeedBe();
            }
        }
    }


    internal void updateRenderOnce(EHudItems eGraphics)
    {
        var item = mHudItems[eGraphics];
        item.update();
        item.render();
        mHudItems[eGraphics] = new CVectorExt();
    }

    public void addToSkipList(EHudItems aHudItemToSkip)
    {
        mSkipList.Add(aHudItemToSkip);
    }
}

