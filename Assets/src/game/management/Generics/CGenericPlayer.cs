
using System;
using System.Collections.Generic;
public class CGenericPlayer : CConvertibleUnit
{

    internal List<CAbility> mAbilities = new List<CAbility>();

    public CGameObject mRrespawn = new CGameObject();

    public CGenericPlayer()
    {
        setDead(false);
    }


    override public void destroy()
    {
        base.destroy();
    }
    override public void render()
    {
        foreach (CAbility ab in mAbilities)
        {
            ab.render();
        }
        base.render();
    }
    override public void update()
    {
        foreach (CAbility ab in mAbilities)
        {
            ab.update();
        }
        base.update();
    }

    override public void sendMessage(EMessage aMessage)
    {
        base.sendMessage(aMessage);
    }


    public void moveToRespawn()
    {
        if (mRrespawn.getCircle() != null)
        {
            setPos(mRrespawn.getCircle().mCenter);
        }
        else if (mRrespawn.getRectangle() != null)
        {
            setX(mRrespawn.getX() + (mRrespawn.getWidth() * 0.5));
            setY(mRrespawn.getY() + (mRrespawn.getHeight() * 0.5));
        }
        else
        {
            throw new Exception("You havent set the shape of the respawn!");
        }
    }


    override protected void manageStates()
    {
        base.manageStates();
    }

}