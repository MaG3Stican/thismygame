
using System;
using System.Collections.Generic;
using System.Linq;
public class CItemManager
{
    // Singleton.
    private static CItemManager mInst = null;

    // List of Items to be managed.
    private Dictionary<EItems, CVectorExt> mItems = new Dictionary<EItems, CVectorExt>();

    public CItemManager()
    {
        registerSingleton();
    }

    public static CItemManager inst()
    {
        return mInst;
    }

    private void registerSingleton()
    {
        if (mInst == null)
        {
            mInst = this;
        }
        else
        {
            throw new Exception("Cannot create another instance of Singleton CItemManager.");
        }
    }

    public int getCount(EItems aItem)
    {
        return mItems[aItem].arr.Count;
    }

    public void update()
    {
        for (int index = 0; index < mItems.Count; index++)
        {
            var item = mItems.ElementAt(index);
            item.Value.update();
        }
    }

    public CGameObject collides(CGameObject aGameObject, EItems aItem)
    {
        return mItems[aItem].collides(aGameObject);

    }

    public void flush(EItems aItem)
    {
        mItems[aItem].destroy();
    }

    //if nothing is specified, removes first object of array
    private void removeItem(EItems aItem, CGenericGraphic aObject = null)
    {
        mItems[aItem].remove(aObject);

    }

    public void render()
    {
        for (int index = 0; index < mItems.Count; index++)
        {
            var item = mItems.ElementAt(index);
            item.Value.render();
        }
    }

    public void addItem(CGenericItem aObject, EItems aItem)
    {
        if (mItems.ContainsKey(aItem))
        {
            mItems[aItem].append(aObject);
        }
        else
        {
            mItems.Add(aItem, new CVectorExt());
            mItems[aItem].append(aObject);
        }

    }

    public void destroy()
    {
        if (mInst != null)
        {
            for (int index = 0; index < mItems.Count; index++)
            {
                var item = mItems.ElementAt(index);
                item.Value.destroy();
            }
        }
    }
}