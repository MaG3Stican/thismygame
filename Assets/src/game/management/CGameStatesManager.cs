

	public class CGameStatesManager 
	{
		private CGameState  mCurrentState;
		private CGameState mPreviousState;
		
		public  CGameStatesManager() 
		{
			mCurrentState = new CGameState();
			mPreviousState = new CGameState();
		}
		
		public void changeState(CGameState newGameState)	
		{
			mPreviousState = mCurrentState;
			mCurrentState = newGameState;
			newGameState.init();
		}
		
		public void changeToPreviousState()
		{
			mCurrentState = mPreviousState;
		}
		
		public void updateCurrentState()
		{
			mCurrentState.update();
		}
		
		public void renderCurrentState()
		{
			mCurrentState.render();
		}
	}