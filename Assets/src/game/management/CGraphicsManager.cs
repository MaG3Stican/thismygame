

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

//lets do as if this is a struct, because we need to modify it but structs are non mutable sadly
public class SGraphicsOrderMetadata
{
    //this is the index in the dicitonary representing the egraphics enum type
    public int parentIndex;
    //this is the index of .arr of the cvectorext class
    public int internalIndex;
    public double top;
    public CVector2D position;
    public bool processed;
    public bool isTile;

}

public class CGraphicsManager
{
    // Singleton.
    private static CGraphicsManager mInst = null;
    int mHowManyActiveObjects = 0;
    public static SGraphicsOrderMetadata[] mHowGraphicsShouldBeOrdered;

    // List of Items to be managed.
    private Dictionary<EGraphics, CVectorExt> mGraphics = new Dictionary<EGraphics, CVectorExt>();

    List<EGraphics> mSkipList = new List<EGraphics>();

    public CGraphicsManager()
    {
        registerSingleton();
    }

    public static CGraphicsManager inst()
    {
        return mInst;
    }

    private void registerSingleton()
    {
        if (mInst == null)
        {
            mInst = this;
        }
        else
        {
            throw new Exception("Cannot create another instance of Singleton CItemManager.");
        }
    }

    public int getCount(EGraphics aItem)
    {
        return mGraphics[aItem].arr.Count;
    }

    public void update()
    {
        mHowManyActiveObjects = 0;
        for (int index = 0; index < mGraphics.Count; index++)
        {
            var item = mGraphics.ElementAt(index);
            mHowManyActiveObjects += item.Value.update(item.Key == EGraphics.Tiles);
        }
        reorderGraphics();
    }

    public CGameObject collides(CGameObject aGameObject, EGraphics aItem)
    {
        return mGraphics[aItem].collides(aGameObject);

    }

    public void flush(EGraphics aItem)
    {
        mGraphics[aItem].destroy();
    }

    //if nothing is specified, removes first object of array
    private void removeItem(EGraphics aItem, CGenericGraphic aObject = null)
    {
        mGraphics[aItem].remove(aObject);

    }

    public void render()
    {

        for (int index = 0; index < mGraphics.Count; index++)
        {
            var item = mGraphics.ElementAt(index);
            item.Value.render();

        }

        activateNearbyGraphics();

    }

    public void addGraphic(CGenericGraphic aObject, EGraphics aGraphic)
    {
        if (mGraphics.ContainsKey(aGraphic))
        {
            mGraphics[aGraphic].append(aObject);
        }
        else
        {
            mGraphics.Add(aGraphic, new CVectorExt());
            mGraphics[aGraphic].append(aObject);
        }
    }

    public void destroy()
    {
        if (mInst != null)
        {
            for (int index = 0; index < mGraphics.Count; index++)
            {
                var item = mGraphics.ElementAt(index);
                item.Value.destroy();
            }
        }
    }


    public void activateNearbyGraphics()
    {
        for (int index = 0; index < mGraphics.Count; index++)
        {
            var item = mGraphics.ElementAt(index);

            if (!mSkipList.Contains(item.Key))
            {
                item.Value.checkIfRenderNeedBe();
            }
        }
    }


    internal void updateRenderOnce(EGraphics eGraphics)
    {
        var item = mGraphics[eGraphics];
        item.update();
        item.render();
        mGraphics[eGraphics] = new CVectorExt();
    }


    public CVectorExt getGraphics(EGraphics aGraphic)
    {
        return mGraphics[aGraphic];
    }

    public void addToSkipList(EGraphics aGraphicToSkip)
    {
        mSkipList.Add(aGraphicToSkip);
    }


    internal void reorderGraphics()
    {
        mHowGraphicsShouldBeOrdered = new SGraphicsOrderMetadata[mHowManyActiveObjects];

        CRect rec;
        int index = 0;
        int index2 = 0;
        int activeIndex = 0;
        double top2 = 0;
        int max = mGraphics.Count;
        for (index = 0; index < max; index++)
        {
            var item = mGraphics.ElementAt(index);

            var graphics = item.Value;

            for (index2 = 0; index2 < graphics.arr.Count; index2++)
            {

                if (graphics.arr[index2].isActive())
                {
                    var graphic = graphics.arr[index2];
                    var shape = graphics.arr[index2].getShape();

                    var toContinue = true;


                    if (item.Key == EGraphics.Tiles)
                    {
                        if (((CTile)graphic).amIUseful())
                        {
                            toContinue = true;
                        }
                        else
                        {
                            toContinue = false;
                        }
                    }

                    if (toContinue)
                    {
                        if (shape is CRect)
                        {
                            rec = ((CRect)shape);

                            var result = new SGraphicsOrderMetadata() { internalIndex = index, parentIndex = index2, top = graphic.getSmartY(), position = new CVector2D(graphic.getSmartX(), graphic.getSmartY()), isTile = item.Key == EGraphics.Tiles };

                            mHowGraphicsShouldBeOrdered[activeIndex] = result;

                            graphic.mGraphicOrderMetadata = result;
                        }
                        else if (shape is CCircle)
                        {
                            top2 = CMath.getTopOfCircle((CCircle)shape);

                            var result = new SGraphicsOrderMetadata() { internalIndex = index, parentIndex = index2, top = top2, position = new CVector2D(graphic.getSmartX(), graphic.getSmartY()), isTile = item.Key == EGraphics.Tiles }; ;
                            mHowGraphicsShouldBeOrdered[activeIndex] = result;

                            graphic.mGraphicOrderMetadata = result;

                        }
                        else if (shape is CCircle)
                        {
                            top2 = CMath.getTopOfPolygon((CPolygon)shape);
                            var result = new SGraphicsOrderMetadata() { internalIndex = index, parentIndex = index2, top = top2, position = new CVector2D(graphic.getSmartX(), graphic.getSmartY()), isTile = item.Key == EGraphics.Tiles };
                            mHowGraphicsShouldBeOrdered[activeIndex] = result;

                            graphic.mGraphicOrderMetadata = result;
                        }

                        activeIndex++;
                    }
                }
            }
        }



        // sort for their  position
        Array.Sort(mHowGraphicsShouldBeOrdered,
            delegate(SGraphicsOrderMetadata firstPair,
            SGraphicsOrderMetadata nextPair)
            {
                return firstPair.position.x.CompareTo(nextPair.position.x);
            }
        );

        // sort for their top position
        Array.Sort(mHowGraphicsShouldBeOrdered,
            delegate(SGraphicsOrderMetadata firstPair,
            SGraphicsOrderMetadata nextPair)
            {
                return firstPair.top.CompareTo(nextPair.top);
            }
        );

        var ii = 0;
        var limit = mHowGraphicsShouldBeOrdered.Length;
        var topZ = 0;
        var lastY = 0d;
        var triesToFindAdjacentTiles = 2;
        for (ii = 0; ii < limit; ii++)
        {
            var metadata = mHowGraphicsShouldBeOrdered[ii];
            if (metadata.position != null)
            {
                if (metadata.processed == false)
                {
                    var someGraphic = mGraphics.ElementAt(metadata.internalIndex).Value.arr[metadata.parentIndex];

                    someGraphic.mGraphicOrderMetadata.processed = true;
                    var instance = new CVector1D() { mDimension = topZ };



                    //check for adjacent tiles if they are similar or not based on their name (aka tile set number)
                    if (someGraphic is CGenericTile)
                    {
                        var tile = new CGenericTile();
                        tile = (CGenericTile)someGraphic;
                        //this is so tiles always are in front of other objects

                        findAdjacentSimilarTiles(instance,
                                                tile,
                                                triesToFindAdjacentTiles,
                                                new CDirection() { BOTTOM = true, TOP = true, RIGHT = true, LEFT = true });

                        if (lastY != tile.getY())
                        {
                            topZ -= CMap.MAXIMUM_SECTION_LAYERS + 2;
                            lastY = tile.getY();
                        }
                        someGraphic.setZOffset(instance);
                    }
                    else
                    {
                        //this is so that in between these Z values (from the current to the current - maximumsecitonlayers) we can also place all the subsequent tiles that should be above 
                        topZ -= CMap.MAXIMUM_SECTION_LAYERS + 2;

                        someGraphic.setZOffset(instance);
                    }


                }
            }
        }
    }

    private bool isAdjacentToTheRightOrLeft(CGenericTile A, CGenericTile B)
    {
        string aName = A.getName();
        string aNumber = Regex.Match(aName, @"\d+").Value;
        string bName = B.getName();

        string bNumber = Regex.Match(bName, @"\d+").Value;

        if (Math.Abs(Convert.ToInt32(aNumber) - Convert.ToInt32(bNumber)) == 1)
            return true;
        else
            return false;
    }

    private bool isAdjacentToTheBottomOrTop(CGenericTile A, CGenericTile B)
    {
        string aName = A.getName();
        string aNumber = Regex.Match(aName, @"\d+").Value;
        string bName = B.getName();
        string bNumber = Regex.Match(bName, @"\d+").Value;

        var resultToCheck = Math.Abs(Convert.ToInt32(aNumber) + CMap.inst().mTileSetSize.x);

        if (resultToCheck == Convert.ToInt32(bNumber))
            return true;
        else
            return false;
    }

    //this is in charge of procesing the z value of tiles that are similar
    private void findAdjacentSimilarTiles(CVector1D aZoffset, CGenericTile aCurrentTile, int aTriesLeft, CDirection aDirections)
    {
        CGenericTile nextTile;

        var neighbors = CMap.inst().getNeighbors(aCurrentTile, EDiagonalMovement.Never);

        //if the count is less than 2 its not a relevant tile so just continue
        if (aDirections.BOTTOM && neighbors.bottom != null && neighbors.bottom.Count > 1)
        {
            nextTile = neighbors.bottom[CMap.MAXIMUM_BACKGROUND_LAYERS];
            if (nextTile.mGraphicOrderMetadata != null && !nextTile.mGraphicOrderMetadata.processed && isAdjacentToTheBottomOrTop(aCurrentTile, nextTile))
            {
                //this is so that tiles in front of this one will be rendered in front
                for (int index = CMap.MAXIMUM_BACKGROUND_LAYERS; index < neighbors.bottom.Count; index++)
                {
                    CGenericGraphic tile = neighbors.bottom[index];
                    tile.mGraphicOrderMetadata.processed = true;
                    tile.setZOffset(aZoffset.moveDimension(-index));
                }

                //keep moving forward
                findAdjacentSimilarTiles(aZoffset, neighbors.bottom[CMap.MAXIMUM_BACKGROUND_LAYERS], aTriesLeft, new CDirection { BOTTOM = true, LEFT = true, RIGHT = true, TOP = false });
            }
        }
        if (aDirections.TOP && neighbors.top != null && neighbors.top.Count > 1)
        {

            nextTile = neighbors.top[CMap.MAXIMUM_BACKGROUND_LAYERS];
            if (nextTile.mGraphicOrderMetadata != null && !nextTile.mGraphicOrderMetadata.processed && isAdjacentToTheBottomOrTop(aCurrentTile, nextTile))
            {
                //this is so that tiles in front of this one will be rendered in front
                for (int index = CMap.MAXIMUM_BACKGROUND_LAYERS; index < neighbors.top.Count; index++)
                {
                    CGenericGraphic tile = neighbors.top[index];
                    tile.mGraphicOrderMetadata.processed = true;
                    tile.setZOffset(aZoffset.moveDimension(-index));
                }
                //keep moving forward
                findAdjacentSimilarTiles(aZoffset, neighbors.top[CMap.MAXIMUM_BACKGROUND_LAYERS], aTriesLeft, new CDirection { BOTTOM = false, LEFT = true, RIGHT = true, TOP = true });
            }
        }
        if (aDirections.LEFT && neighbors.left != null && neighbors.left.Count > 1)
        {
            nextTile = neighbors.left[CMap.MAXIMUM_BACKGROUND_LAYERS];
            if (nextTile.mGraphicOrderMetadata != null && !nextTile.mGraphicOrderMetadata.processed && isAdjacentToTheRightOrLeft(aCurrentTile, nextTile))
            {
                //this is so that tiles in front of this one will be rendered in front
                for (int index = CMap.MAXIMUM_BACKGROUND_LAYERS; index < neighbors.left.Count; index++)
                {
                    CGenericGraphic tile = neighbors.left[index];
                    tile.mGraphicOrderMetadata.processed = true;
                    tile.setZOffset(aZoffset.moveDimension(-index));
                }

                //keep moving forward
                findAdjacentSimilarTiles(aZoffset, neighbors.left[CMap.MAXIMUM_BACKGROUND_LAYERS], aTriesLeft, new CDirection { BOTTOM = true, LEFT = true, RIGHT = false, TOP = true });
            }
        }
        if (aDirections.RIGHT && neighbors.right != null && neighbors.right.Count > 1)
        {
            nextTile = neighbors.right[CMap.MAXIMUM_BACKGROUND_LAYERS];

            if (nextTile.mGraphicOrderMetadata != null && !nextTile.mGraphicOrderMetadata.processed && isAdjacentToTheRightOrLeft(aCurrentTile, nextTile))
            {
                //this is so that tiles in front of this one will be rendered in front
                for (int index = CMap.MAXIMUM_BACKGROUND_LAYERS; index < neighbors.right.Count; index++)
                {
                    CGenericGraphic tile = neighbors.right[index];
                    tile.mGraphicOrderMetadata.processed = true;
                    tile.setZOffset(aZoffset.moveDimension(-index));
                }

                //keep moving forward
                findAdjacentSimilarTiles(aZoffset, neighbors.right[CMap.MAXIMUM_BACKGROUND_LAYERS], aTriesLeft, new CDirection { BOTTOM = true, LEFT = false, RIGHT = true, TOP = true });
            }
        }

    }
}

