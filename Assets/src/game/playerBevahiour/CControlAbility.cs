﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CControlAbility : CAbility
{
    public CControlAbility(CVector3D aPos, CVector2D aPivot)
    {
        mParticleSystem = new CParticle(CAssets.CONTROL_ABILITY, aPos, CLayer.getLayer(ELayer.Particles), aPivot, "Control_Ability");

        mParticleSystem.deactivate();

        setLayer(CLayer.getLayer(ELayer.Particles));

        mParticleSystem.mSystem.startSize = 40;
    }


    public override void render()
    {
        if (CVars.inst().getPlayer().shootPressed())
        {
            setPos(new CVector3D((double)CMouse.getMouseX(), (double)CMouse.getMouseY(), getLayer().Z));

            if (mParticleSystem.isActive())
            {
                mParticleSystem.render();
            }
        }
        //do stuff with particles
        /* ParticleSystem.Particle[] particles = new ParticleSystem.Particle[100];
         int length = mParticleSystem.mSystem.GetParticles(particles);
         int i = 0;
         while (i < length)
         {
             particles[i].position = getPos().toVec3();
             i++;
         }
         mParticleSystem.mSystem.SetParticles(particles, length);*/


        base.render();
    }

    public override void update()
    {
        if (CVars.inst().getPlayer().shootPressed())
        {
            setActive(true);
            mParticleSystem.activate();
        }

        base.update();
    }
}

