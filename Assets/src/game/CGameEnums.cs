﻿

public enum EItems
{
    Torch = 0
}

public enum EGraphics
{
    Tiles = 0,
    Shapes = 1,
    Enemies = 2,
    Artifacts = 3,
    RainSplashes = 4,
    RainDrops = 5,
    Player = 6
}

public enum EProjectiles
{
    Bullet = 0
}


public static class ETorchAnimations
{
    public const string Loop = "torchLoop";
}


public static class EMapSector
{
    public const float GrassRock = 0.0f;
    public const float Church = 0.1f;
    public const float BrickGrass = 0.2f;
    public const float Plants = 0.95f;
}


public static class EMapLayers
{
    public const string Church = "map_church";
    public const string GrassRock = "map_grass_rock";
    public const string BrickGrass = "map_brick";
    public const string Plants = "map_plants";
    public const string MapLayout = "map_layout";
}

public enum EMessage
{
    NONE = -1,
    ENEMY_DIED = 0,
    ENEMY_KILLED = 1,
    COLLIDED_WITH_ENEMY = 2,
    COLLECTED_RESOURCE = 3,
    ABSORBED = 4,
    IN_RANGE_OF_ABSORBPTION = 5,
    COLLIDED_WITH_BULLET = 6,
    COLLIDED_WITH_PLAYER = 7,
    COLLECTED_POWERUP = 8,
    COLLIDED_WITH_BUILDING = 9
}

public static class ETerrains
{
    public const string ENEMY_PATH = "enemyPath";
    public const string WALKABLE = "walkable";
    public const string PLATFORM = "mPlatform";
}

//leave a space of 2 so that all objects can go up and down
public enum ELayer
{
    None = 0,
    Ground = -1,
    Section = -2,
    SubSection = -3,
    Buildings = -4,
    Artifacts = -5,
    Light = -6,
    Player = -7,
    Projectiles = -8,
    Particles = -1993,
    Dialogue = -1994,
    DialogueText = -1995,
    Minimap = -1996,
    MinimapObject = -1997,
    Hud = -1998,
    Camera = -1999,

}
public enum EParticleTypes
{
    Abilities = 0,
    Dummy = 1
}




public enum EHudItems
{
    LABEL = 0,
    BUTTON = 1,
    STATUS_BAR = 2,
    TEXT_DIALOGUE = 3,
    LORE = 4
}