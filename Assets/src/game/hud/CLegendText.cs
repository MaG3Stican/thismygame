﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;



public class CLegendText : CGenericTextbox
{

    int mCurrentLegendIndex = -1;
    List<string> mLegend = new List<string>() { 
        "In a dark story of fantasy they recited",
        "\"Oh master of shadows\"",
        "\"We in desperation claim to you\"",
        "\"With our last hopes we summon you\"",
        "\"In the last attempt from freedom\"",
        "\"Rise from your ashes once more!\""
    };

    private CVector2D mSize = new CVector2D(1000, 200);
    int waitForHowManyCharactersBeforeDoingEffect = 5;
    public CLegendText(string aText)
        : base(CLayer.getLayer(ELayer.Hud), 10)
    {

        text = CCanvas.inst().addSmartLabel(
            new SHudProperties(aText)
            {
                appendToMainCanvas = true,
                centeredX = true,
                centeredY = false,
                color = Color.red,
                fontPro = Resources.Load<TextMeshProFont>(CAssets.HORROR_FONT),
                fontSize = 100,
                id = getId(),
                pos = mDefaultPos,
                size = mSize
            });
        setState(EWordState.None);

        CHudManager.inst().addHudItem(this, EHudItems.LORE);
        CHudManager.inst().addToSkipList(EHudItems.LORE);

        waitForHowManyCharactersBeforeDoingEffect = (int)Math.Floor(aText.Length * 0.2);
    }

    public void dropCharacter(CharacterInfo aChar)
    {


    }

    public override void update()
    {
        if (mCurrentLegendIndex < mLegend.Count - 1)
        {
            manageStates();

            if (getState() == EWordState.None)
                setState(EWordState.ReadyForProcessing);
            else if (mState == EWordState.Processing)
            {
                if (base.mFinishedWithCurrentText)
                    setState(EWordState.FinishedProcessing);
            }
            else if (mLayDownFor == -1)
                setState(EWordState.None);
            else if (mState == EWordState.ReadyForProcessing)
                setState(EWordState.Processing);
            else if (mState == EWordState.FinishedProcessing)
                setState(EWordState.Idle);

            base.update();
        }

    }



    protected override void setState(EWordState mCurrentState)
    {
        base.setState(mCurrentState);
    }

    protected override void manageStates()
    {
        if (mState == EWordState.FinishedProcessing)
        {
            base.setLettersState(ELetterState.None);
            base.makeLettersTransparent();
            text.ForceMeshUpdate();
            base.mLayDownFor = 110;
        }
        else if (mState == EWordState.Idle)
        {
            mLayDownFor--;
        }
        else if (mState == EWordState.None)
        {
            mLayDownFor = 0;
            base.init(getCurrentLegendPhrase());
            base.makeLettersTransparent();
        }
        else if (mState == EWordState.Processing)
        {
            base.makeLettersAppearOneByOne();
        }
        else if (mState == EWordState.ReadyForProcessing)
        {
            setLettersFinishingReason(ELetterDyingReason.OutOfCamera);
        }
    }

    public override void render()
    {

        base.render();
    }

    public string getCurrentLegendPhrase()
    {
        mCurrentLegendIndex++;
        return mLegend[mCurrentLegendIndex];
    }


    public bool isMomentToSpawnPlayer()
    {
        //index 6 is supposed to be after the summoning finished
        return mCurrentLegendIndex > 5;
    }





}
