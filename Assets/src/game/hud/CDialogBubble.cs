﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;


public class CDialogBubble : CGenericTextbox
{
    //the limit of characters is about as much as : "My head is spinning."
    static List<string> mDialoguePool = new List<string>(){
        "Arghh arghh I'm hungry",
        "Thy punny flesh is mine",
        "Where's my leg?",
        "Meeaaat meeaaat..",
        "Hunger for.. battle?",
        "Where's you, master?",
        "I'm weeaak.. Give me liver",
        "Num num, vomit.",
        "My head is spinning.",
        "Where's master? Master Master!",
        "I just wanna be your frieeend",
        "I smell like poo",
        "I feel like poo",
    };
    //seconds
    CVector2D mSize = new CVector2D(103, 60);

    public CDialogBubble(CVector3D aPosition, string aText)
        : base(CLayer.getLayer(ELayer.Dialogue))
    {
        setGraphic(CHelper.get2DReadyObject("Dialogue bubble" + getId()));
        var r = getGraphic().GetComponent<SpriteRenderer>();
        mAnimation.loadAnimation(r, CAssets.DIALOGUE_BUBBLE, EAnimationSpeed.NORMAL_ANIMATION);

        text = CCanvas.inst().addSmartLabel(new SHudProperties
        {
            appendToMainCanvas = false,
            color = Color.black,
            fontPro = Resources.Load<TextMeshProFont>(CAssets.DIALOG_FONT),
            id = getId(),
            pos = aPosition,
            text = aText,
            fontSize = 16,
            size = mSize,
            layer = CLayer.getLayer(ELayer.DialogueText)
        });

        setPivot(new CVector2D(24, 16));


        setWidth((uint)mSize.x);
        setHeight((uint)(mSize.y));


        setState(EWordState.None);
        CHudManager.inst().addHudItem(this, EHudItems.TEXT_DIALOGUE);

        mAnimation.play(CAssets.DIALOGUE_BUBBLE.parsedName(), true);
    }

    public override void update()
    {
        if (isActive())
        {

            manageStates();


            if (getState() == EWordState.None)
                setState(EWordState.ReadyForProcessing);
            else if (mState == EWordState.Processing)
            {
                if (base.mFinishedWithCurrentText)
                {
                    setState(EWordState.FinishedProcessingIdleTime);
                }
            }
            else if (mState == EWordState.FinishedProcessingIdleTime)
            {
                if (mFinishedProcessingIdleTime == -1)
                {
                    setState(EWordState.FinishedProcessing);
                }
            }
            else if (mState == EWordState.Idle)
            {
                if (mLayDownFor == -1)
                {
                    setState(EWordState.None);
                    //fake reactivation
                    getGraphic().SetActive(true);
                    text.gameObject.SetActive(true);
                }
            }
            else if (mState == EWordState.ReadyForProcessing)
                setState(EWordState.Processing);
            else if (mState == EWordState.FinishedProcessing)
            {
                setState(EWordState.Idle);
                //fake deactivation
                getGraphic().SetActive(false);
                text.gameObject.SetActive(false);
            }

            mAnimation.update();
            base.update();
        }
    }

    public override void render()
    {
        if (isActive())
        {
            var pos = getPos().toVec3();
            pos.x += (float)(mSize.x * 0.5f);
            pos.y += (float)(mSize.y * 0.5f);
            pos.x += (float)getPivot().x;
            pos.y += (float)getPivot().y;
            pos.y *= -1;
            text.transform.position = pos;
            base.render();
            mAnimation.render();
        }
    }

    public bool processedCurrentText()
    {
        return mAllLettersFinishedProcessing;
    }


    internal static string pickRandomDialogue()
    {
        var index = CMath.randInt(0, mDialoguePool.Count - 1);
        return mDialoguePool[index];
    }


    protected override void setState(EWordState aCurrentState)
    {
        if (aCurrentState == EWordState.FinishedProcessingIdleTime)
        {
            mFinishedProcessingIdleTime = 210;
        }
        if (aCurrentState == EWordState.FinishedProcessing)
        {
            mLayDownFor = 350;
        }
        if (aCurrentState == EWordState.None)
        {
            mLayDownFor = 0;
        }


        base.setState(aCurrentState);
    }


    protected override void manageStates()
    {
        if (mState == EWordState.FinishedProcessing)
        {
            base.setLettersState(ELetterState.None);
            base.makeLettersTransparent();
            text.ForceMeshUpdate();
        }
        else if (mState == EWordState.Idle)
        {
            mLayDownFor--;
        }
        else if (mState == EWordState.None)
        {
            base.init(pickRandomDialogue());
            base.makeLettersTransparent();
        }
        else if (mState == EWordState.FinishedProcessingIdleTime)
        {
            mFinishedProcessingIdleTime--;
        }
        else if (mState == EWordState.Processing)
        {
            base.makeLettersAppearOneByOne();
        }
        else if (mState == EWordState.ReadyForProcessing)
        {
            setLettersFinishingReason(ELetterDyingReason.OutOfCamera);
        }
    }




    //HARDCODE
    public override void setPos(CVector3D pos)
    {
        pos.y = pos.y - 30;
        pos.x = pos.x + 18;
        base.setPos(pos);
    }






}
