﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CAbility : CGameObject
{
    internal CParticle mParticleSystem;

    public CAbility()
    {

    }

    internal void stop()
    {
        mParticleSystem.stopParticle();
    }

    public override void render()
    {

        mParticleSystem.setPos(getPos());
        mParticleSystem.render();
        base.render();
    }

    public override void update()
    {
        mParticleSystem.update();
        base.update();
    }


    public void stopAbility()
    {
        mParticleSystem.stopParticle();
    }
    public void playAbility()
    {
        mParticleSystem.playParticle();
    }

}
