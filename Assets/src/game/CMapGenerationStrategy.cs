﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CMapGenerationStrategy : CMapStrategy
{

    Dictionary<string, float> objectsChance = new Dictionary<string, float>() 
                                          {{ CAssets.BRICK_GRASS ,  EMapSector.BrickGrass },
                                          { CAssets.CHURCH,  EMapSector.Church},
                                          { CAssets.GRASS,  EMapSector.GrassRock},
                                          { CAssets.PLANTS,  EMapSector.Plants}};

    public override void init()
    {
        base.init();

    }

    public CMapGenerationStrategy(int aMapSize = 10)
    {
        //order from smaller to bigger
        var list = objectsChance.Keys.ToList();
        list.Sort();
        var holder = new Dictionary<string, float>();
        foreach (var key in list)
        {
            holder.Add(key, objectsChance[key]);
        }
        objectsChance = holder;


        CVars.inst().getMap().loadInitialSector(EMapLayers.MapLayout, CLayer.getLayer(ELayer.Ground));
        List<CGenericTile> sectionTiles = new List<CGenericTile>();
        double rightMostTile = 0;
        double bottomMostTile = 0;
        double leftMostTile = 0;
        double topMostTile = 0;

        double height = 0;
        double width = 0;

        CVector2D mSectionSize = new CVector2D();

        float whatToDo = -1f;
        float chance = 0;
        string sectionName = "";
        for (var i = 0; i < aMapSize; i++)
        {
            whatToDo = -1f;
            chance = CMath.randFloat();

            for (var ii = 0; ii < objectsChance.Values.Count && whatToDo == -1f; ii++)
            {
                if (objectsChance.ElementAt(ii).Value > chance)
                {
                    whatToDo = objectsChance.ElementAt(ii).Value;
                }
            }
            //fallback value in case nothing was found
            if (whatToDo == -1f)
                whatToDo = 0f;


            if (whatToDo == EMapSector.BrickGrass)
            {
                sectionTiles = CVars.inst().getMap().loadMapSector(EMapLayers.BrickGrass, CLayer.getLayer(ELayer.Artifacts));
                sectionName = EMapLayers.BrickGrass;

            }
            else if (whatToDo == EMapSector.Church)
            {
                sectionTiles = CVars.inst().getMap().loadMapSector(EMapLayers.Church, CLayer.getLayer(ELayer.Artifacts));
                sectionName = EMapLayers.Church;

            }
            else if (whatToDo == EMapSector.GrassRock)
            {
                sectionTiles = CVars.inst().getMap().loadMapSector(EMapLayers.GrassRock, CLayer.getLayer(ELayer.Artifacts));
                sectionName = EMapLayers.GrassRock;

            }
            else if (whatToDo == EMapSector.Plants)
            {
                sectionTiles = CVars.inst().getMap().loadMapSector(EMapLayers.Plants, CLayer.getLayer(ELayer.Artifacts));
                sectionName = EMapLayers.Plants;

            }
            rightMostTile = sectionTiles.OrderByDescending(z => z.getX()).FirstOrDefault().getX();
            bottomMostTile = sectionTiles.OrderByDescending(z => z.getY()).FirstOrDefault().getY();
            leftMostTile = sectionTiles.OrderBy(z => z.getX()).FirstOrDefault().getX();
            topMostTile = sectionTiles.OrderBy(z => z.getY()).FirstOrDefault().getY();

            height = bottomMostTile - topMostTile + CVars.inst().getMap().getTileHeight();
            width = rightMostTile - leftMostTile + CVars.inst().getMap().getTileWidth();

            mSectionSize.x = width;
            mSectionSize.y = height;



            //first is the lowest value, since x and y are inverted lowest value is actually maximum value
            CVars.inst().getMap().placeSection(sectionTiles, mSectionSize, sectionName, CLayer.getLayer(ELayer.Artifacts));


        }
    }
}










