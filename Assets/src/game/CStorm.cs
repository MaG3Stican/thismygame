﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CStorm
{
    int mRainDrops = 0;
    int mRainSplashes = 0;
    private static CStorm mInst;
    int mNumberOfRainSplashes = 10;
    int mNumberOfRainDrops = 30;

    public CStorm()
    {
        if (mInst == null)
        {
            mInst = this;
        }
        else
        {
            throw new Exception("You are trying to create a new instance of cstorm class");
        }

        CGraphicsManager.inst().addToSkipList(EGraphics.RainDrops);
    }

    public static CStorm inst()
    {
        return mInst;
    }

    public void update()
    {
        if (mRainSplashes < mNumberOfRainSplashes)
        {
            var splash = new CRainSplash();
            splash.setPos(CMap.inst().getTileGlobalWithPixels(CMap.inst().randomXInMap(), CMap.inst().randomYInMap()).First().getPos());
            splash.setLayer(CLayer.getLayer(ELayer.Artifacts));
            CGraphicsManager.inst().addGraphic(splash, EGraphics.RainSplashes);
            mRainSplashes++;
        }
        if (mRainDrops < mNumberOfRainDrops)
        {
            var drop = new CRainDrop();
            drop.setPos(CMap.inst().getTileGlobalWithPixels(CMap.inst().randomXInMap(), CMap.inst().randomYInMap()).First().getPos());
            drop.setLayer(CLayer.getLayer(ELayer.Artifacts));
            drop.setVel(new CVector3D(-2, 2));
            drop.setBoundAction(EBoundBehaviour.WRAP);
            drop.setBounds(CVars.inst().MinBounds(), CVars.inst().MaxBounds());
            CGraphicsManager.inst().addGraphic(drop, EGraphics.RainDrops);

            mRainDrops++;
        }

        foreach (CRainSplash splash in CGraphicsManager.inst().getGraphics(EGraphics.RainSplashes).arr)
        {
            splash.mAnimation.isEnded();
        }
        foreach (CRainDrop splash in CGraphicsManager.inst().getGraphics(EGraphics.RainDrops).arr)
        {
            splash.mAnimation.isEnded();
        }
    }
    public void render()
    {
    }
}
