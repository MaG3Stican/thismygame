﻿
using System.IO;
using UnityEngine;
class CAssets
{
    public CAssets()
    {

    }
    public static string TILESET = "Sprites/tileset";

    public static string TILESET_XML = "Sprites/maps/map01";


    public static string PEASANT_IDLE = "Sprites/enemies/peasant/idle";
    public static string PEASANT_WALKING = "Sprites/enemies/peasant/walking";
    public static string PEASANT_BEING_CONVERTED = "Sprites/enemies/peasant/beingconverted";

    public static string BRICK_GRASS = "Sprites/maps/brick_map";

    public static string PLANTS = "Sprites/maps/plants_map";

    public static string GRASS = "Sprites/maps/grass_map";

    public static string CHURCH = "Sprites/maps/church_map";

    public static string CONTROL_ABILITY = "Sprites/abilities/control_particle";

    public static string HORROR_FONT = "Fonts/NemoEditted";

    public static string DIALOG_FONT = "Fonts/CevicheEditted";

    public static string BLOOD_TEXTURE = "Sprites/fonts/blood";

    public static string RAIN_DROP = "Sprites/environment/Rain-drop";

    public static string RAIN_SPLASH = "Sprites/environment/Rain-in-floor";

    public static string PLAYER_PRIEST_WALK = "Sprites/player/player_priest";

    public static string PLAYER_DEMON_WALK = "Sprites/player/Walking Animation/Player-walking-";
    public static string PLAYER_DEMON_IDLE = "Sprites/player/Idle Animation/Player-idle-";
    public static string PLAYER_DEMON_SENDING = "Sprites/player/Sending Animation/Player-Sending";
    public static string PLAYER_DEMON_DYING = "Sprites/player/Dying Animation/Player-Dying-";
    public static string PLAYER_DEMON_CONVERTING = "Sprites/player/Converting Animation/Player-Converting-";

    public static string DIALOGUE_BUBBLE = "Sprites/hud/Dialogue-Bubble";
    //mouse
    public static string POINTER_ARROW = "Sprites/hud/pointer/pointer_arrow";
    public static string POINTER_SWORD = "Sprites/hud/pointer/pointer_sword";
    public static string POINTER_HAND = "Sprites/hud/pointer/pointer_hand";
    public static string POINTER_DIALOGUE = "Sprites/hud/pointer/pointer_dialogue";

    public static string MINI_MAP = "Sprites/hud/pointer/pointer_dialogue";

    //partiles
    public static string PINK_PARTICLE = "Sprites/particles/pink";
    public static string RED_PARTICLE = "Sprites/particles/red";
}