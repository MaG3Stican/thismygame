
using System;
using System.Collections.Generic;
using UnityEngine;

public enum EMouseButton
{
    Left = 0,
    Right = 1,
    Middle = 2
}

public enum EMouseState
{
    Arrow = 0,
    Hand = 1,
    Dialogue = 2,
    Sword = 3
}




public class CMouse : CGenericGraphic
{
    private int __scale = 2;
    private int mCurrentPointerIndex = 0;
    private int mMaxCurrentPointerIndex = 0;
    private Texture2D mPointerArrow;
    private Texture2D mPointerSword;
    private List<Texture2D> mPointerHand;
    private List<Texture2D> mPointerDialogue;
    private CShape mShapeToCheck = null;

    private SpriteRenderer mRenderer;

    private EMouseState mState = EMouseState.Arrow;

    static CMouse mInst = null;
    public CMouse()
    {
        if (mInst == null)
        {
            setGraphic(CHelper.get2DReadyObject("mouse"));
            mRenderer = getGraphic().GetComponent<SpriteRenderer>();
            mCollides = false;
            mInst = this;
            mPointerArrow = Resources.Load<Texture2D>(CAssets.POINTER_ARROW);


            mPointerSword = Resources.Load<Texture2D>(CAssets.POINTER_SWORD);

            mPointerHand = CHelper.loadSequenceTexture2d(CAssets.POINTER_HAND);
            mPointerDialogue = CHelper.loadSequenceTexture2d(CAssets.POINTER_DIALOGUE);

            mCollides = false;
            setLayer(CLayer.getLayer(ELayer.Hud));
        }

        else
            throw new Exception("You are trying to create a second instance of the mouse");
    }


    static public bool pressed(EMouseButton aKeyCode)
    {
        return Input.GetMouseButton((int)aKeyCode);
    }

    static public bool firstPressed(EMouseButton aKeyCode)
    {
        return Input.GetMouseButtonDown((int)aKeyCode);
    }
    //run this during update method
    static public bool notPressed(EMouseButton aKeyCode)
    {
        return Input.GetMouseButtonUp((int)aKeyCode);
    }

    static public float getMouseX()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
    }

    static public float getMouseY()
    {
        return Camera.main.ScreenToWorldPoint(Input.mousePosition).y * -1;
    }
    static public float getMouseZ()
    {
        return Input.mousePosition.z;
    }

    internal EMouseState getState()
    {
        mCurrentPointerIndex = 0;
        return mState;
    }


    public override void update()
    {
        setX(getMouseX());
        setY(getMouseY());
        if (mShapeToCheck == null)
        {
            setState(EMouseState.Arrow);
        }
        else
        {
            //if this happens do nothing
            if (!CMath.isPointInPolygon(mShapeToCheck, new CVector2D(getMouseX(), getMouseY())))
            {
                mShapeToCheck = null;
                setState(EMouseState.Arrow);
            }
        }

        //these states have animations so we use the update function for the purpose of updating those animations
        if (mState == EMouseState.Dialogue)
        {
            Cursor.SetCursor(mPointerDialogue[getCurrentIndex()], Vector2.zero, CursorMode.Auto);
        }
        else if (mState == EMouseState.Hand)
        {
            Cursor.SetCursor(mPointerHand[getCurrentIndex()], Vector2.zero, CursorMode.Auto);
        }

        base.update();
    }

    private int getCurrentIndex()
    {
        if (getTimeState() % 20 == 0)
        {
            //the minus one is because all indexes should start in 0..
            if (mCurrentPointerIndex < mMaxCurrentPointerIndex - 1)
            {
                mCurrentPointerIndex++;
            }
            else
            {
                mCurrentPointerIndex = 0;
            }
        }

        return mCurrentPointerIndex;
    }

    public void setState(EMouseState aState)
    {
        if (aState == EMouseState.Arrow)
        {
            mMaxCurrentPointerIndex = 0;
            Cursor.SetCursor(mPointerArrow, Vector2.zero, CursorMode.Auto);
        }
        else if (aState == EMouseState.Sword)
        {
            mMaxCurrentPointerIndex = 0;
            Cursor.SetCursor(mPointerSword, Vector2.zero, CursorMode.Auto);
        }
        else if (aState == EMouseState.Dialogue)
        {
            mMaxCurrentPointerIndex = mPointerDialogue.Count;
        }
        else if (aState == EMouseState.Hand)
        {
            mMaxCurrentPointerIndex = mPointerHand.Count;
        }

        mState = aState;
    }

    public override void render()
    {
        base.render();
    }

    public static CMouse inst()
    {
        return mInst;
    }

    internal static void checkIntersectionAndSetStateOnTrue(CShape aShape, EMouseTriggers mMouseTrigger)
    {
        if (CMath.isPointInPolygon(aShape, new CVector2D(getMouseX(), getMouseY())))
        {
            CMouse.inst().checkForThisShapeNow(aShape);
            if (mMouseTrigger == EMouseTriggers.mTriggersDialogueOnHover)
            {
                CMouse.inst().setState(EMouseState.Dialogue);
            }
            if (mMouseTrigger == EMouseTriggers.mTriggersHandOnHover)
            {
                CMouse.inst().setState(EMouseState.Hand);
            }
            if (mMouseTrigger == EMouseTriggers.mTriggersSwordOnHover)
            {
                CMouse.inst().setState(EMouseState.Sword);
            }
        }
    }

    private void checkForThisShapeNow(CShape aShape)
    {
        mShapeToCheck = aShape;
    }
}