
using System;
using UnityEngine;
public class CKeyPoll
{
    static public bool pressed(KeyCode aKeyCode)
    {
        return Input.GetKey(aKeyCode);
    }

    static public bool firstPress(KeyCode aKeyCode)
    {
        return Input.GetKeyDown(aKeyCode);
    }
    //run this during update method
    static public bool released(KeyCode aKeyCode)
    {
        return Input.GetKeyUp(aKeyCode);
    }

    static public bool anyKeyPressedFirstTime()
    {
        return Input.anyKeyDown;
    }

    static public bool anyKeyPressed()
    {
        return Input.anyKey;
    }
}