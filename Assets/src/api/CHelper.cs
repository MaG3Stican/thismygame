
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;
using System.Net.Sockets;
using System;

public class CHelper : MonoBehaviour
{


    static Sprite[] cachedSprites = new Sprite[] { };

    public CHelper()
    {
    }

    public static string getLocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
            }
        }
        return localIP;
    }

    public static byte[] objToBytes(object obj)
    {
        MemoryStream o = new MemoryStream();
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(o, obj);
        return o.GetBuffer();
    }

    public static object BytesToObj(byte[] data)
    {
        if (data == null || data.Length == 0)
            return null;
        var ins = new MemoryStream(data);
        BinaryFormatter bf = new BinaryFormatter();
        return bf.Deserialize(ins);
    }


    public static UnityEngine.Object Instantiate(ref GameObject aGameObject, ENetworkMode aMode)
    {
        if (aMode == ENetworkMode.local)
        {
            return Instantiate(aGameObject);
        }
        else if (aMode == ENetworkMode.online)
        {
            return Network.Instantiate(aGameObject, new Vector3(0, 0, 0), aGameObject.transform.rotation, 0);
        }
        else
        {
            return null;
        }
    }

    static public CVector2D pointToVector(CVector2D aPoint)
    {
        return new CVector2D(aPoint.x, aPoint.y);
    }

    public static void sortChildrenByY(SpriteRenderer aContainer)
    {
        //aContainer.
        //aContainer.renderer.sortingOrder
        //int i;
        //ArrayList childList = new ArrayList();
        //i = aContainer.numChildren;

        //objeto.setY(getY() + )
        //// TODO: Ordenar por gety() del game object (porque ya tiene sumada la z=altura).
        //childList.sortOn("y", Array.NUMERIC);
        //i = aContainer.numChildren;
        //while (i--)
        //{
        //    if (childList[i] != aContainer.getChildAt(i))
        //    {
        //        aContainer.setChildIndex(childList[i], i);
        //    }
        //}
        //spriteRenderer.sortingOrder = (int)Camera.main.WorldToScreenPoint (spriteRenderer.bounds.min).y * -1;

    }

    public static Sprite createSpriteFromAnother(SpriteRenderer renderer)
    {
        Texture2D old = renderer.sprite.texture;
        Texture2D left = new Texture2D((int)(old.width), old.height, old.format, false);
        Color[] colors = old.GetPixels(0, 0, (int)(old.width), old.height);
        left.SetPixels(colors);
        left.Apply();
        Sprite sprite = Sprite.Create(left,
               new Rect(0, 0, left.width, left.height),
               new Vector2(0.5f, 0.5f),
               40);
        return sprite;
    }

    public static GameObject loadSpritesToGameObject(GameObject obj, string spritePath)
    {
        var sprites = Resources.LoadAll<Sprite>(spritePath);
        List<Sprite> backSprite = sprites.ToList(); ;

        float initialX = 0;
        float initialY = 0;

        foreach (Sprite s in backSprite)
        {
            SpriteRenderer render = new SpriteRenderer();
            render = obj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            render.sprite = s;
            render.transform.position.Set(initialX, initialY, 0);
            initialX += s.border.x;
        }
        return obj;
    }

    public static GameObject get2DReadyObject(string aName = "", string aPath = "")
    {
        GameObject obj;
        if (aPath != "")
        {
            obj = Instantiate(Resources.Load(aPath, typeof(GameObject))) as GameObject;
        }
        else
        {
            obj = new GameObject();
            SpriteRenderer render = new SpriteRenderer();
            render = obj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
        }

        if (aName != "")
        {
            if (obj == null)
            {
                var asd = "";
            }
            obj.name = aName;
        }
        return obj;
    }
    public static GameObject getMeshed2DReadyObject(string aName = "", string aPath = "")
    {
        GameObject obj;
        if (aPath != "")
        {
            obj = Instantiate(Resources.Load(aPath, typeof(GameObject))) as GameObject;
        }
        else
        {
            obj = new GameObject();
            obj.AddComponent(typeof(LineRenderer));
            obj.AddComponent(typeof(MeshRenderer));
            obj.AddComponent(typeof(MeshFilter));
        }

        if (aName != "")
        {
            obj.name = aName;
        }
        return obj;
    }

    public static GameObject loadSpriteToGameObject(GameObject obj, string spritePath, int index, bool cache, bool light = true)
    {
        if (obj == null)
        {
            obj = new GameObject();
        }

        //TODO REFACTOR THIS
        if (cachedSprites.Count() == 0 && cache)
        {
            cachedSprites = Resources.LoadAll<Sprite>(spritePath);

            SpriteRenderer render = new SpriteRenderer();
            render = obj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            //check if inside bounds of array :
            try
            {
                var value = cachedSprites[index];
                obj.name = value.name;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            render.sprite = cachedSprites[index];
            if (light)
            {
                render.material = Resources.Load<Material>("Materials/lighted2dSprite");
            }
            else
            {
                render.material = Resources.Load<Material>("Materials/nolight2d");
            }

        }
        else if (!cache)
        {
            var localSprites = Resources.LoadAll<Sprite>(spritePath);

            SpriteRenderer render = new SpriteRenderer();
            render = obj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            //check if inside bounds of array :
            try
            {
                var value = localSprites[index];
                obj.name = value.name;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            if (light)
            {
                render.material = Resources.Load<Material>("Materials/lighted2dSprite");
            }
            else 
            {
                render.material = Resources.Load<Material>("Materials/nolight2d");
            }

            render.sprite = localSprites[index];
        }
        else
        {
            SpriteRenderer render = new SpriteRenderer();
            render = obj.AddComponent(typeof(SpriteRenderer)) as SpriteRenderer;
            //check if inside bounds of array :
            try
            {
                var value = cachedSprites[index];
                obj.name = value.name;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            render.sprite = cachedSprites[index];
            if (light)
            {
                render.material = Resources.Load<Material>("Materials/lighted2dSprite");
            }
            else
            {
                render.material = Resources.Load<Material>("Materials/nolight2d");
            }
        }
        return obj;
    }

    public static List<Sprite> loadSprites(string aSpritePath)
    {
        return Resources.LoadAll<Sprite>(aSpritePath).ToList();
    }

    public static CAnimation loadSpriteAnimation(string aSpritePath)
    {
        var sprites = Resources.LoadAll<Sprite>(aSpritePath).ToList();

        CAnimation animation = new CAnimation();

        return animation;
    }

    internal static GameObject createAndGetCanvas()
    {
        GameObject canvas = Instantiate(Resources.Load("Prefabs/Canvas")) as GameObject;

        return canvas;
    }

    internal static void flip(GameObject gameObject, bool wasFlipped)
    {
        var quaternion = gameObject.transform.rotation;
        var angles = quaternion.eulerAngles;

        if (wasFlipped)
        {
            angles.y += 180;
        }
        else
        {
            angles.y -= 180;
        }

        quaternion.eulerAngles = angles;
        gameObject.transform.rotation = quaternion;
    }


    internal static List<Texture2D> loadSequenceTexture2d(string aPath)
    {
        List<Texture2D> sequence = new List<Texture2D>();
        int startsWith = 0;
        int permitedIterationsOfNull = 3;
        int currentIterationsOfNull = 0;
        bool allFound = false;
        while (!allFound)
        {
            if (currentIterationsOfNull <= permitedIterationsOfNull)
            {
                var result = Resources.Load<Texture2D>(aPath + startsWith);
                if (result != null)
                {
                    sequence.Add(result);
                }
                else
                {
                    currentIterationsOfNull++;
                }
                startsWith++;
            }
            else
            {
                if (sequence.Count > 0)
                {
                    return sequence;
                }
                throw new Exception("The file does not have a propper sequence, a sample of a sequence is : flower1 flower2 flower3");
            }
        }

        return sequence;
    }


    internal static List<Sprite> loadSequenceSprites(string aPath)
    {
        List<Sprite> sequence = new List<Sprite>();
        int startsWith = 0;
        int permitedIterationsOfNull = 3;
        int currentIterationsOfNull = 0;
        bool allFound = false;
        while (!allFound)
        {
            if (currentIterationsOfNull <= permitedIterationsOfNull)
            {
                var result = Resources.Load<Sprite>(aPath + startsWith);
                if (result != null)
                {
                    sequence.Add(result);
                }
                else
                {
                    currentIterationsOfNull++;
                }
                startsWith++;
            }
            else
            {
                if (sequence.Count > 0)
                {
                    return sequence;
                }
                throw new Exception("The file does not have a propper sequence, a sample of a sequence is : flower1 flower2 flower3");
            }
        }

        return sequence;
    }


}