﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public enum ELetterState
{
    None = 0,
    FinishedProcessing = 1,
    ReadyForProcessing = 2,
    MutatingColors = 3,
    WaitingForProcessingTime = 4,
    DiedState = 5

}

public enum ELetterDyingReason
{
    None = 0,
    OutOfCamera = 1,
    ReachedMaxLifeTime = 2
}

public class CGenericLetter : CGenericGraphic
{
    public int index;
    public int maxLifeTime;
    public int currentLifeTime;
    public int characterIndex;
    public List<int> vertices;
    public bool startBehaviour;
    public List<CVector3D> initialPos;
    public ELetterState mState;
    public ELetterDyingReason mDyingReason;


    public CGenericLetter()
    {
        mCollides = false;
    }

    public CGenericLetter(CLayer aLayer, int aMyLifeTime, int aCharacterIndex, ELetterDyingReason aReasonForDying, CVector2D aSize)
    {
        maxLifeTime = aMyLifeTime;
        index = 0;
        currentLifeTime = 0;
        characterIndex = aCharacterIndex;
        vertices = new List<int>();
        initialPos = new List<CVector3D>();
        mState = ELetterState.None;
        mDyingReason = aReasonForDying;
        setShape(EShapes.RECT);
        setWidth((uint)aSize.x);
        setHeight((uint)aSize.y);

        setLayer(aLayer);
    }

    public override void update()
    {
        manageStates();

        if (mState == ELetterState.None)
            setState(ELetterState.WaitingForProcessingTime);

        base.update();
    }

    public override void render()
    {
        base.render();
    }


    public void setState(ELetterState aState)
    {
        setTimeState(0);
        mState = aState;
    }


    public ELetterState getState()
    {
        return mState;
    }

    internal bool finishedProcessing()
    {
        if (mDyingReason == ELetterDyingReason.OutOfCamera)
        {
            return isActive();
        }
        else if (mDyingReason == ELetterDyingReason.ReachedMaxLifeTime)
        {
            return currentLifeTime == maxLifeTime;
        }
        else
        {
            return false;
        }
    }

    protected override void manageStates()
    {
        if (mState == ELetterState.WaitingForProcessingTime)
            if (getTimeState() > 0 && getTimeState() == maxLifeTime)
                setState(ELetterState.ReadyForProcessing);
        if (mState == ELetterState.None)
            currentLifeTime = 0;


        base.manageStates();
    }
}