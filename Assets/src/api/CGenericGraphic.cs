﻿

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum EGraphicState
{
    NONE = 0,
    MOVINGTOWARDSPOINT = 1
}

public enum EMouseTriggers
{
    mTriggersNone = 0,
    mTriggersHandOnHover = 1,
    mTriggersDialogueOnHover = 2,
    mTriggersSwordOnHover = 3,
}

public class CGenericGraphic : CGameObject
{
    public SGraphicsOrderMetadata mGraphicOrderMetadata;
    public bool mOneTime = true;
    public bool mFirstTimeActive = false;
    public bool mFlipMe;
    public bool mCollides;
    public bool mDebug = false;
    private bool mRotate = false;
    private bool mAttack = false;
    internal bool mFlipped = false;

    object mLastDebugObject;
    public CVector3D mDestination = new CVector3D();

    public List<CTile> tilesTop = new List<CTile>();
    public List<CTile> tilesBottom = new List<CTile>();
    public List<CTile> tilesLeft = new List<CTile>();
    public List<CTile> tilesRight = new List<CTile>();
    public CAnimation mAnimation { get; private set; }

    protected EMouseTriggers mMouseTrigger = EMouseTriggers.mTriggersNone;

    private static string mDebugId = "";



    public int mAbsorptionPercentage = 0;

    CVector1D mZOffset = new CVector1D();



    private double mDistance;
    private EGraphicState mGraphicState = EGraphicState.NONE;
    private List<CVector3D> mHorizontalPoints = new List<CVector3D>();
    private List<CVector3D> mVerticalPoints = new List<CVector3D>();


    public CGenericGraphic()
        : base()
    {

        mAnimation = new CAnimation();
    }




    public void enemyWasKilled()
    {

    }

    public void setAnimation(CAnimation aAnimation)
    {
        mAnimation = aAnimation;
    }


    internal void moveToCenter()
    {
        setX(getX() + getPivot().x);
        setY(getY() + getPivot().y);
    }

    public virtual void activate()
    {
        mZOffset.mDimension = 0;
        setActive(true);
    }

    public virtual void deactivate()
    {
        mZOffset.mDimension = 0;
        setActive(false);
    }

    public void rotate(int aAngle = 0)
    {
        mRotate = true;
        setAngle(aAngle);
    }

    override public void update()
    {
        if (mGraphicState == EGraphicState.MOVINGTOWARDSPOINT)
        {
            if (2 > mDistance)
            {
                setGraphicState(EGraphicState.NONE);
                getVel().setXYZ(0, 0, 0);
            }
            mDistance -= getVel().magnitude();
            //if we cannot move forward, lets say we reached the point we were going to
            if (mLimitReached)
            {
                mDistance = 0;
            }
        }


        if (mRotate)
        {
            var quaternion = getGraphic().transform.rotation;
            var angles = quaternion.eulerAngles;
            angles.z += (float)getAngle();
            quaternion.eulerAngles = angles;
            getGraphic().transform.rotation = quaternion;
        }





        if (collides())
        {
            int tilesY;
            int tilesX;
            double currentY;
            double currentX;
            var tilesAboveAndBehind = new List<CGenericTile>();
            var tiles = new List<CGenericTile>();
            // ↙
            tiles = (CMap.inst().getTileGlobalWithPixels(getSmartX(), getSmartY() + getHeightSmart()));
            if (checkIfCollides(tiles))
            {
                if (getVel().pointsDownLeft)
                {
                    stopMove();
                }
                else
                {
                    //fix position because maybe the player got stuck
                }
            }
            // ↘
            tiles = (CMap.inst().getTileGlobalWithPixels(getSmartX() + getWidthSmart(), getSmartY() + getHeightSmart()));
            if (checkIfCollides(tiles))
            {
                if (getVel().pointsDownRight)
                {
                    stopMove();
                }
                else
                {
                    //fix position because maybe the player got stuck
                }
            }
            // ↖
            tiles = (CMap.inst().getTileGlobalWithPixels(getSmartX(), getSmartY()));
            if (checkIfCollides(tiles))
            {
                if (getVel().pointsUpLeft)
                {
                    stopMove();
                }
                else
                {
                    //fix position because maybe the player got stuck
                }
            }
            // ↗
            tiles = (CMap.inst().getTileGlobalWithPixels(getSmartX() + getWidthSmart(), getSmartY()));
            if (checkIfCollides(tiles))
            {
                if (getVel().pointsUpRight)
                {
                    stopMove();
                }
                else
                {
                    //fix position because maybe the player got stuck
                }
            }
            // →
            //get not one but all tiles in the right side 
            tilesY = (int)Math.Floor(getHeightSmart() * CMap.mCachedDivisionFactor);
            while (tilesY > -1)
            {
                currentY = getSmartY() + (CGameConstants.TILE_HEIGHT * tilesY);
                tiles = (CMap.inst().getTileGlobalWithPixels(getSmartX() + getWidthSmart(), currentY));
                if (checkIfCollides(tiles))
                {
                    if (getVel().pointsRight)
                    {
                        stopMove();
                    }
                    else
                    {
                        //fix position because maybe the player got stuck
                    }
                }
                tilesY--;
            }
            // ←
            //get not one but all tiles in the left side 


            tilesY = (int)Math.Floor(getHeightSmart() * CMap.mCachedDivisionFactor);
            while (tilesY > -1)
            {
                currentY = getSmartY() + (CGameConstants.TILE_HEIGHT * tilesY);
                tiles = (CMap.inst().getTileGlobalWithPixels(getSmartX() - 1, currentY));
                if (checkIfCollides(tiles))
                {
                    if (getVel().pointsLeft)
                    {
                        stopMove();
                    }
                    else
                    {
                        //fix position because maybe the player got stuck
                    }
                }
                tilesY--;
            }
            // ↑
            //get not one but all tiles in the top side 
            tilesX = (int)Math.Floor(getWidthSmart() * CMap.mCachedDivisionFactor);
            while (tilesX > -1)
            {

                //getSmartY()
                currentX = getSmartX() + (CGameConstants.TILE_WIDTH * tilesX);
                tiles = (CMap.inst().getTileGlobalWithPixels(currentX, getSmartY() - 1));
                if (checkIfCollides(tiles))
                {
                    if (getVel().pointsUp)
                    {
                        stopMove();
                    }
                    else
                    {
                        //fix position because maybe the player got stuck
                    }
                }
                tilesX--;
            }
            // ↓
            //get not one but all tiles in the bottom side 
            tilesX = (int)Math.Floor(getWidthSmart() * CMap.mCachedDivisionFactor);
            while (tilesX > -1)
            {
                currentX = getSmartX() + (CGameConstants.TILE_WIDTH * tilesX);
                tiles = (CMap.inst().getTileGlobalWithPixels(currentX, getSmartY() + getHeightSmart()));
                if (checkIfCollides(tiles))
                {
                    if (getVel().pointsDown)
                    {
                        stopMove();
                    }
                    else
                    {
                        //fix position because maybe the player got stuck
                    }
                }
                tilesX--;
            }
        }

        base.update();
    }

    public double getSmartX()
    {
        if (getFeet() != null)
        {
            if (getFeet() is CRect)
                return getX() + ((CRect)getFeet()).x;
            else
                return getX();
        }
        else
            return getX();
    }
    public double getSmartY()
    {
        if (getFeet() != null)
        {
            if (getFeet() is CRect)
                return getY() + ((CRect)getFeet()).y;
            else
                return getY();
        }
        else
            return getY();
    }


    //has to be executed before update
    internal void flipLogic()
    {

        if (mFlipMe)
        {
            if (!isFlipped())
            {
                if (getVel().pointsLeft)
                {
                    flip();
                }
            }
            if (isFlipped())
            {
                if (getVel().pointsRight)
                {
                    flip();
                }
            }
        }
    }



    override public void render()
    {
        base.render();



        if (mMouseTrigger != EMouseTriggers.mTriggersNone)
        {
            CMouse.checkIntersectionAndSetStateOnTrue(getShape().copy(), mMouseTrigger);
        }

        if (mCollides)
        {
            if (getShapeType() == EShapes.NONE)
            {
                throw new Exception("You must set a shape for every graphic! Otherwhise it cannot collide");
            }
            if (getWidth() == 0 && getHeight() == 0 && getRadious() == 0)
            {
                Debug.Log("this object doesnt have width");
            }
        }
        if (isActive())
        {

            if (getGraphic() != null)
            {

                Vector3 vec = new Vector3((float)(getPos().x), (float)(getPos().y) * -1, (mZOffset.mDimension != 0 ? mZOffset.mDimension : (float)getZ()));

                if (isFlipped())
                {
                    vec.x += getWidth();
                }

                getGraphic().transform.position = vec;
            }
        }



        if (mDebug)
        {
            if (getGraphic() != null)
            {

                if (mDebugId == getId())
                {
                    var dbug = "";
                    Debug.Break();
                }

                var canvas = CVars.inst().getCanvas();

                //debug button
                var btn = canvas.getGuiItem<UnityEngine.UI.Button>(getId() + "debugButton");
                //var textBox = canvas.getGuiItem<UnityEngine.UI.Button>(getId() + "debugTextBox");
                if (btn != null)
                {
                    var holder = getPos().copy();
                    var btnPos = holder.moveY(-70);
                    holder.y *= -1;

                    btn.transform.position = btnPos.toVec3();

                    //var txtBoxPos = holder.moveX(-50);
                    //textBox.transform.position = txtBoxPos.toVec3();
                }
                else
                {
                    btn = canvas.addButton(getPos().copy().moveY(-70), getId() + "debugButton", "Debug this guy");
                    //var txtBox = canvas.addTextBox(getPos().copy().moveX(-70), getId() + "debugTextBox", "");

                    btn.onClick.AddListener(delegate
                    {
                        mDebugId = getId();
                    });

                    btn.onClick.AddListener(delegate
                    {
                        Debug.Log("asdasd");
                    });

                }



                var txt = canvas.getGuiItem<UnityEngine.UI.Text>(getId());
                if (txt != null)
                {
                    txt.text = "Going to x : " + mDestination.x + " and y : " + mDestination.y;
                    var holder = getPos().toVec3();
                    holder.y *= -1;
                    txt.transform.position = holder;
                }
                else
                    canvas.addLabel(new SHudProperties("Going to x : " + mDestination.x + " and y : " + mDestination.y) { pos = getPos(), id = getId() });



                txt = canvas.getGuiItem<UnityEngine.UI.Text>(getId() + "distance");
                if (txt != null)
                {
                    txt.text = "The distance is : " + mDistance;
                    var holder = getPos().copy().moveY(90).toVec3();
                    holder.y *= -1;
                    txt.transform.position = holder;
                }
                else
                {
                    canvas.addLabel(new SHudProperties() { pos = getPos().copy().moveY(90), id = getId() + "distance", text = "The distance is : " + mDistance });
                }

            }

            mLastDebugObject = mDestination;
        }


    }


    private bool checkIfCollides(List<CGenericTile> tiles)
    {
        //get first tile because the actual tile does not matter, but the operations we want to perform on it
        var tile = tiles.SingleOrDefault(c => !c.isWalkable());

        if (tile != null)
        {
            CShape mShape;
            if (getFeet() != null)
            {
                mShape = getFeet();
                if (mShape is CRect)
                {
                    CRect shape = mShape as CRect;
                    var x = shape.x + getX();
                    var y = shape.y + getY();
                    mShape = new CRect(shape.width, shape.height, x, y);

                }
            }
            else
            {
                mShape = getShape();
            }
            return CMath.shapeAndPolygonCollision(mShape, tile.getCollisionableShape());
        }
        else
        {
            return false;
        }
    }


    private bool collides()
    {
        return mCollides;
    }


    override public void destroy()
    {
        base.destroy();
    }


    public List<CVector3D> addHorizontalPoints(uint aX, int aY, int aHowManyPoints)
    {
        if (aHowManyPoints > 0)
        {
            mHorizontalPoints.Add(new CVector3D(aX, aY));
            addHorizontalPoints((uint)(aX + CGameConstants.TILE_WIDTH), aY, aHowManyPoints - 1);
        }
        return mHorizontalPoints;
    }

    public List<CVector3D> addVerticalPoints(int aX, uint aY, int aHowManyPoints)
    {
        if (aHowManyPoints > 0)
        {
            mVerticalPoints.Add(new CVector3D(aX, aY));
            addVerticalPoints(aX, (uint)(aY + CGameConstants.TILE_HEIGHT), aHowManyPoints - 1);
        }
        return mVerticalPoints;
    }

    public List<CTile> getTiles(List<CVector3D> aPoints)
    {
        List<CTile> tiles = new List<CTile>();
        for (int i = 0; i < aPoints.Count; i++)
        {
            CTile tile = (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(aPoints[i].x), (int)(aPoints[i].y));

            tile.setDebug(true);
            tiles.Add(tile);
        }
        return tiles;
    }


    public bool inspectTiles(List<CTile> aTiles)
    {
        for (int i = 0; aTiles.Count > i; i++)
        {
            //if false, iterate till the end
            if (!Convert.ToBoolean(aTiles[i].getDynamicProperty(ETerrains.WALKABLE)))
            {
                return true;
            }
        }
        return false;
    }

    //Move to the requested tile, you can pass the current tile the user is in, if the method detects its the same, it doesnt move you
    public CTile goToTile(EDirection aDirection, CTile previousTile)
    {
        CTile tile = new CTile();
        if (aDirection == EDirection.DOWN)
        {
            tile = getTileDown();
        }
        else if (aDirection == EDirection.UP)
        {
            tile = getTileUp();
        }
        else if (aDirection == EDirection.LEFT)
        {
            tile = getTileLeft();
        }
        else if (aDirection == EDirection.RIGHT)
        {
            tile = getTileRight();
        }

        //this is to check if the next tile is the current tile we are on
        if (previousTile != null && tile != null && getCurrentTile().getPos().x == tile.getPos().x && getCurrentTile().getPos().y == tile.getPos().y)
        {
            return previousTile;
        }
        //move to the next tile
        else
        {
            goToAndStop(tile.getCenter());
            return tile;
        }
    }



    public void goToAndStop(CVector3D aWhereToGo, float aVel = 1)
    {
        lookAt(aWhereToGo.x, aWhereToGo.y);
        setVel(new CVector3D(aVel, aVel, 0));
        getVel().setAngle(getAngle());

        //the current poosition of the object is not the ttop left but the centeer
        var dist = getPos().copy().moveX((int)(getWidth() * 0.5)).moveY((int)(getHeight() * 0.5)) - aWhereToGo;


        mDistance = dist.magnitude();
        mDestination = aWhereToGo;


        setGraphicState(EGraphicState.MOVINGTOWARDSPOINT);

    }

    public void goToAndStop(CVector2D aWhereToGo, float aVel = 1)
    {
        lookAt(aWhereToGo.x, aWhereToGo.y);
        setVel(new CVector3D(aVel, aVel, 0));
        getVel().setAngle(getAngle());

        var dist = getPos() - new CVector3D(aWhereToGo.x, aWhereToGo.y);
        dist.x = Math.Abs(dist.x);
        dist.y = Math.Abs(dist.y);

        mDistance = dist.magnitude();

        mDestination = aWhereToGo.toVec3d();

        setGraphicState(EGraphicState.MOVINGTOWARDSPOINT);
    }


    public bool reachedDestination()
    {
        return getGraphicState() == EGraphicState.NONE;
    }



    public CTile getTileDown()
    {
        int height = CMap.mTileSize;
        return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x), (int)(getPos().y) + height);
    }

    public CTile getTileLeft()
    {
        int width = CMap.mTileSize;
        return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x) - width, (int)(getPos().y));
    }

    public CTile getTileRight()
    {
        int width = CMap.mTileSize;
        return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x) + width, (int)(getPos().y));
    }

    public CTile getTileUp()
    {
        int height = CMap.mTileSize;
        return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x), (int)(getPos().y) - height);
    }

    public CTile getCurrentTile()
    {
        int height = CMap.mTileSize;
        int width = CMap.mTileSize;
        return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x), (int)(getPos().y));
    }

    public CTile getFutureTile(EDirection aDirection)
    {
        uint height = CGameConstants.TILE_HEIGHT;
        uint width = CGameConstants.TILE_WIDTH;
        if (aDirection == EDirection.DOWN)
        {
            return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)((getPos().x) - (2 * width)), (int)(getPos().y));
        }
        else if (aDirection == EDirection.LEFT)
        {
            return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x), (int)((getPos().y) - (2 * height)));
        }
        else if (aDirection == EDirection.RIGHT)
        {
            return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)(getPos().x), (int)((getPos().y) + (2 * height)));
        }
        else if (aDirection == EDirection.UP)
        {
            return (CTile)CVars.inst().getMap().getTileGlobalNonWalkableWithPixels((int)((getPos().x) + (2 * width)), (int)(getPos().y));
        }

        return null;
    }

    public virtual void setGraphicState(EGraphicState aGraphicState)
    {
        mGraphicState = aGraphicState;
    }


    public void attack()
    {
        mAttack = true;
    }
    public void stopAttacking()
    {
        mAttack = false;
    }

    public EGraphicState getGraphicState()
    {
        return mGraphicState;
    }


    public void setFlipped(bool aFlipped)
    {
        mFlipped = aFlipped;

    }

    public bool isFlipped()
    {
        return mFlipped;
    }

    internal void flip()
    {
        var wasFlipped = mFlipped;
        mFlipped = !mFlipped;

        //this means is looking right
        if (wasFlipped)
        {

            var material = getGraphic().GetComponent<SpriteRenderer>().material;



            CHelper.flip(getGraphic(), true);
            //fix position
            var vec3d = getGraphic().transform.position;
            var x = vec3d.x;
            x -= getWidth();
            setX(x);


        }
        //this means is looking left
        else
        {

            CHelper.flip(getGraphic(), false);


        }
    }

    public bool mMoveToCenter { get; set; }



    protected bool interactionIsPressed()
    {
        return CMouse.pressed(EMouseButton.Left);
    }
    protected bool interactionIsArrow()
    {
        return CMouse.inst().getState() == EMouseState.Arrow;
    }

    public void setZOffset(CVector1D aOffset)
    {
        mZOffset = aOffset;
    }
}