using System;
using System.Collections.Generic;
using UnityEngine;
public class CMinimap : CGameObject
{
    private CGenericGraphic mMapBackground;
    private CRect mCameraOutline;

    private double mRatio = 1;

    private CCamera mCamera;
    private CMap mMap;

    private List<CShape> mObjects;
    private List<Sprite> mReferences;

    private bool mHandled = false;

    private static CMinimap mInst;

    public static CMinimap inst()
    {
        return mInst;
    }

    public CMinimap(uint aWidth, CLayer aLayer)
    {
        if (mInst == null)
        {
            mObjects = new List<CShape>();
            mReferences = new List<Sprite>();

            mCamera = CCamera.inst();
            mMap = CMap.inst();

            mRatio = aWidth / mMap.getWorldWidth();

            setWidth(aWidth);
            setHeight((uint)(mMap.getWorldHeight() * mRatio));


            mMapBackground = new CGenericGraphic();
            mMapBackground.setGraphic(Resources.Load<GameObject>(CAssets.MINI_MAP));

            //mCameraOutline = new CRect(mCamera.getWidth() * mRatio, mCamera.getHeight() * mRatio, 0x62FF89);
            setLayer(aLayer);

            mInst = this;
        }

        else
        {
            throw new Exception("You are trying to create a new instance of the minimap which is not allowed");
        }
    }

    public void addObject(CShape aObject, Color aColor)
    {
        mObjects.Add(aObject);

    }
    public void removeObject(CShape aObject)
    {
        if (mObjects.Contains(aObject))
        {
            mObjects.Remove(aObject);
        }
    }

    //Getters & Setters
    public double getRatio()
    {
        return mRatio;
    }

    override public void update()
    {
        base.update();

        /*
        if (CMouse.pressed())
        {
            if (mouseOver())
            {
                mHandled = true;
                mCamera.setXY(Math.floor((CMouse.getMouseX() - getX()) / mRatio - mCamera.getWidth() * 0.5), Math.floor((CMouse.getMouseY() - getY()) / mRatio - mCamera.getHeight() * 0.5));
                mCamera.setStateX(CCamera.HANDLED);
                mCamera.setStateY(CCamera.HANDLED);
            }
        }
        //if (CMouse.release() && mHandled)
        //{
            //mHandled = false;
            //mCamera.setStateX(CCamera.KEEPING_UP);
            //mCamera.setStateY(CCamera.KEEPING_UP);
        //}*/
    }

    override public void render()
    {
        base.render();
        mCameraOutline.x = getX() + mCamera.getX() * mRatio;
        mCameraOutline.y = getY() + mCamera.getY() * mRatio;
        mMapBackground.setX(getX());
        mMapBackground.setY(getY());
    }
}