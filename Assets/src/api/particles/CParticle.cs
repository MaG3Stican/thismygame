
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CParticle : CGenericGraphic
{
    CVector2D lastMousePos = new CVector2D();
    public ParticleSystem mSystem;

    public CParticle(Color aColor, CVector3D initPosition, int restrictionDegrees, int angle, int magnitude, CLayer layer, string aName = "")
    {
        setLayer(layer);

        setShape(EShapes.CIRCLE);
        setRadious(4);


    }

    public CParticle(string aGraphicName, CVector3D initPosition, CLayer layer, CVector2D pivot = null, string aName = "")
    {
        setLayer(layer);

        setShape(EShapes.CIRCLE);
        setRadious(4);
        setGraphic((GameObject)CHelper.Instantiate(new GameObject(aName)));

        if (pivot == null)
            pivot = new CVector2D(0, 0);

        var mat = Resources.Load<Material>(aGraphicName);

        var graph = getGraphic();
        mSystem = graph.AddComponent<ParticleSystem>();

        var rotation = graph.transform.rotation;
        rotation.x = -180;
        graph.transform.rotation = rotation;

        mSystem.loop = true;
        mSystem.playbackSpeed = 1;
        mSystem.renderer.material = mat;
        mSystem.startSize = 15;
        /* SerializedObject so = new SerializedObject(mSystem);
         so.FindProperty("ShapeModule.angle").floatValue = 90f;
         so.ApplyModifiedProperties();*/
    }


    public CParticle(CVector2D aPosition, CVector2D aVelocity, CVector2D aAcceleration, CLayer aLayer, GameObject aParticleGraphic, CVector2D aMinBound, CVector2D aMaxBound)
    {
        setPos(aPosition.toVec3d());
        setVelXY(aVelocity);
        setAccelXY(aAcceleration);
        setLayer(aLayer);
        setGraphic(aParticleGraphic);

        setBounds(aMinBound, aMaxBound);
        setBoundAction(EBoundBehaviour.DIE);

        mCollides = false;
    }

    public CParticle(CVector2D aPosition, CVector2D aVelocity, CLayer aLayer, GameObject aParticleGraphic, CVector2D aMinBound, CVector2D aMaxBound)
    {
        setPos(aPosition.toVec3d());
        setVelXY(aVelocity);
        setLayer(aLayer);
        setGraphic(aParticleGraphic);
        setBounds(aMinBound, aMaxBound);
        setBoundAction(EBoundBehaviour.DIE);

        mCollides = false;
    }


    public void submitToFields(List<CField> fields)
    {
        double totalAccelerationX = 0;
        double totalAccelerationY = 0;

        for (var i = 0; i < fields.Count; i++)
        {
            var field = fields[i];

            // find the distance between the particle and the field
            var vectorX = field.getX() - getX();
            var vectorY = field.getY() - getY();

            // calculate the force (gravity equations of highschool physics)
            var force = field.getMass() / Math.Pow(vectorX * vectorX + vectorY * vectorY, 1.5);

            // add to the total acceleration the force adjusted by distance
            totalAccelerationX += vectorX * force;
            totalAccelerationY += vectorY * force;
        }

        // update
        setAccelX(totalAccelerationX);
        setAccelY(totalAccelerationY);
    }


    override public void update()
    {
        base.update();
    }

    override public void render()
    {
        base.render();
    }

    override public void destroy()
    {
        base.destroy();
    }

    public void stopParticle()
    {
        mSystem.Stop();
    }

    public void playParticle()
    {
        mSystem.Play();
    }
}