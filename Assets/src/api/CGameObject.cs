
using System;
using System.Collections.Generic;
using UnityEngine;

public class CGameObject : IGameObject
{


    protected bool mLimitReached = false;
    private CVector3D mPos = new CVector3D();
    private CVector3D mVel = new CVector3D();
    private int mMass = 0;
    private CVector3D mAccel = new CVector3D();

    protected Guid mId;



    private GameObject mGameObject;
    //private CAnimation mAnimation = new CAnimation();
    private CSoundAndMusic mSound = new CSoundAndMusic();
    private CSoundAndMusic mMusic = new CSoundAndMusic();
    private CVector3D mOldPos = new CVector3D();
    private CVector3D mOldVel = new CVector3D();

    private List<CGenericBehaviour> mBehaviours = new List<CGenericBehaviour>();

    //private List<CGenericBehaviour> mBehaviours = new List<CGenericBehaviour>();
    private CVector2D mInitPosition = new CVector2D();
    private double mAngle = 0;
    private CCircle mCircle;
    private CRect mRectangle;
    private CShape mFeet = null;
    private bool mActive = true;
    private string mName = "";
    private double mGuid = 0;

    private CVector2D mMax = new CVector2D(CMath.INFINITY, CMath.INFINITY);
    private CVector2D mMin = new CVector2D(-CMath.INFINITY, -CMath.INFINITY);
    private int mSpeed = 0;

    private EShapes mShape = EShapes.NONE;
    //private var CShapes:CShapes = new CShapes();
    private uint mState = 0;
    private uint mTimeState = 0;

    private EMessage mMessage = EMessage.NONE;
    private EBoundBehaviour mBoundBehaviour = EBoundBehaviour.NONE;

    private uint mHeight;
    private uint mWidth;
    private uint mRadious;

    private bool mDead = false;
    private bool mRedirect = false;

    private CLayer mLayer;
    private int mFlipOffset = 0;

    private double mFriction = 1.0;
    private double mMaxSpeed = 9999;
    private CVector2D mRegistryPoint = new CVector2D(0, 0);
    private CVector2D mPivot = new CVector2D(0, 0);

    public CGameObject()
    {
        mId = Guid.NewGuid();
    }

    public void play()
    {

    }

    public void setGraphic(GameObject aRenderer)
    {
        mGameObject = aRenderer;
    }


    public GameObject getGraphic()
    {
        return mGameObject;
    }


    public double getX()
    {
        return mPos.x;
    }

    public double getY()
    {
        return mPos.y;
    }

    public double getZ()
    {
        return mPos.z;
    }

    public CVector3D getPos()
    {
        return mPos;
    }

    public double getVelX()
    {
        return mVel.x;
    }

    public double getVelY()
    {
        return mVel.y;
    }


    public int getMass()
    {
        return mMass;
    }

    public CVector3D getVel()
    {
        return mVel;
    }

    public double getAccelX()
    {
        return mAccel.x;
    }

    public double getAccelY()
    {
        return mAccel.y;
    }

    public CVector3D getAccel()
    {
        return mAccel;
    }

    public CVector2D getLocalCoordinates()
    {
        //if (mc != null)
        //{
        //    return mc.localToGlobal(new flash.geom.Point(mc.x, mc.y));
        //}
        //else if (mSprite != null)
        //{
        //    return mSprite.localToGlobal(new flash.geom.Point(mSprite.x, mSprite.y));
        //}
        return null;
    }


    public virtual void setActive(bool aActive)
    {
        mActive = aActive;
        if (mGameObject != null)
        {
            mGameObject.SetActive(aActive);
        }

    }

    public bool isActive()
    {
        return mActive;
    }

    public void setLayer(CLayer aLayer)
    {
        mLayer = aLayer;
        setZ(aLayer.Z);
    }

    public CLayer getLayer()
    {
        return mLayer;
    }

    public void setCircle(CCircle aCircle)
    {
        mCircle = aCircle;
    }

    public void setMass(int aMass)
    {
        mMass = aMass;
    }

    public CCircle getCircle()
    {
        mCircle.mCenter.x = getX();
        mCircle.mCenter.y = getY();
        return mCircle;
    }

    public void setRectangle(CRect aRectangle)
    {
        mRectangle = aRectangle;
    }

    public CRect getRectangle()
    {
        mRectangle.x = (float)getX();
        mRectangle.y = (float)getY();
        return mRectangle;
    }

    public void setTimeState(uint aTimeState)
    {
        mTimeState = aTimeState;
    }

    public uint getTimeState()
    {
        return mTimeState;
    }

    public void setX(double aX)
    {
        //MOVES REGISTRY POINT  TO THE CENTER
        mPos.x = aX;

        if (mShape == EShapes.CIRCLE)
        {
            mCircle.mCenter.x = aX;
        }
        else if (mShape == EShapes.RECT)
        {
            mRectangle.x = aX; // (aX + (mRectangle.width * 0.5));

        }

    }

    public void setY(double aY)
    {
        mPos.y = aY;

        if (mShape == EShapes.CIRCLE)
        {
            mCircle.mCenter.y = aY;
        }
        else if (mShape == EShapes.RECT)
        {
            mRectangle.y = aY; // (aY + (mRectangle.height * 0.5));
        }
    }

    public void setZ(double aZ)
    {
        mPos.z = (aZ);

    }


    public void setXY(double aX, double aY)
    {
        mPos.x = aX;
        mPos.y = aY;
        if (mShape == EShapes.CIRCLE)
        {
            mCircle.mCenter.y = aY;
            mCircle.mCenter.x = aX;
        }
        else if (mShape == EShapes.RECT)
        {
            mRectangle.y = aY; // (aY + (mRectangle.height * 0.5));
            mRectangle.x = aX; // (aX + (mRectangle.width * 0.5));
        }
    }

    public virtual void setPos(CVector3D pos)
    {
        mPos.x = pos.x;
        mPos.y = pos.y;
        mPos.z = pos.z;

        if (mShape == EShapes.CIRCLE)
        {
            mCircle.mCenter.y = mPos.y;
            mCircle.mCenter.x = mPos.x;
            mCircle.mCenter.z = mPos.z;
        }
        else if (mShape == EShapes.RECT)
        {
            mRectangle.y = mPos.y; //(mPos.y + (mRectangle.height * 0.5));
            mRectangle.x = mPos.x; //(mPos.x + (mRectangle.width * 0.5));
        }
    }

    public void setPosXYZ(double aX = 0, double aY = 0, double aZ = 0)
    {
        mPos.x = aX;
        mPos.y = aY;
        mPos.z = aZ;
        if (mShape == EShapes.CIRCLE)
        {
            mCircle.mCenter.y = mPos.y;
            mCircle.mCenter.x = mPos.x;
            mCircle.mCenter.z = mPos.z;
        }
        else if (mShape == EShapes.RECT)
        {
            mRectangle.y = mPos.y; //(mPos.y + (mRectangle.height * 0.5));
            mRectangle.x = mPos.x; //(mPos.x + (mRectangle.width * 0.5));
        }
    }

    public void setInitPos(CVector2D pos)
    {
        mInitPosition.x = pos.x;
        mInitPosition.y = pos.y;

        if (mShape == EShapes.CIRCLE)
        {
            mCircle.mCenter.y = mInitPosition.y;
            mCircle.mCenter.x = mInitPosition.x;
        }
        else if (mShape == EShapes.RECT)
        {
            mRectangle.y = mInitPosition.y; //(mPos.y + (mRectangle.height * 0.5));
            mRectangle.x = mInitPosition.x; //(mPos.x + (mRectangle.width * 0.5));
        }
    }

    public void setShape(EShapes aShape)
    {
        mShape = aShape;
        if (aShape == EShapes.RECT)
        {
            mRectangle = new CRect();
        }
        else if (aShape == EShapes.CIRCLE)
        {
            mCircle = new CCircle();
        }
    }

    public CShape getShape()
    {
        if (mShape == EShapes.CIRCLE)
        {
            return mCircle;
        }
        else if (mShape == EShapes.RECT)
        {
            return mRectangle;
        }
        else
        {
            throw new Exception("You did not check that this object should have a shape");
        }
    }

    public EShapes getShapeType()
    {
        return mShape;
    }

    public void setVelX(double aVelX)
    {
        mVel.x = aVelX;
    }

    public void setVelY(double aVelY)
    {
        mVel.y = aVelY;
    }

    public void setVelXYZ(double aVelX, double aVelY, double aVelZ = 0)
    {
        mVel.x = aVelX;
        mVel.y = aVelY;
        mVel.z = aVelZ;
    }

    public void setVelXY(CVector2D aVel)
    {
        mVel.x = aVel.x;
        mVel.y = aVel.y;
    }
    public void setAccelXY(CVector2D aAccel)
    {
        mAccel.x = aAccel.x;
        mAccel.y = aAccel.y;
    }
    //velocity is constant
    public void setVel(CVector3D aVel)
    {
        mVel.x = aVel.x;
        mVel.y = aVel.y;
        mVel.z = aVel.z;
    }
    public void setVel(int aVel)
    {
        CVector3D vel = new CVector3D();
        vel.setAngleMag(mPos.getAngle(), aVel);
        mVel = vel;
    }


    //opposite side and adjacent side is the distance we must travel
    public void setSpeed(int aSpeed)
    {
        mSpeed = aSpeed;
    }

    public int getSpeed()
    {
        return mSpeed;
    }

    public void setAccelX(double aAccelX)
    {
        mAccel.x = aAccelX;
    }

    public void setAccelY(double aAccelY)
    {
        mAccel.y = aAccelY;
    }

    //acceleration decays over time
    public void setAccelXYZ(double aAccelX, double aAccelY, double aAccelZ = 0)
    {
        mAccel.x = aAccelX;
        mAccel.y = aAccelY;
        mAccel.z = aAccelZ;
    }

    public void setAccel(CVector3D aAccel)
    {
        aAccel.mult(-1);
        mAccel = aAccel;
    }

    public void setOldData()
    {
        mOldPos.x = mPos.x;
        mOldPos.y = mPos.y;
        mOldPos.z = mPos.z;
        mOldVel.x = mVel.x;
        mOldVel.y = mVel.y;
        mOldVel.z = mVel.z;
    }

    public virtual void update()
    {
        //add acceleration to velocity, multiplicate by friction, check if fixage is needed
        mVel.addVec(mAccel);
        mVel.mult(mFriction);
        mVel.truncate(mMaxSpeed);
        mPos.addVec(mVel);
        setPos(mPos);

        bool touchesLeft = getX() < mMin.x;
        bool touchesRight = getX() > mMax.x;
        bool touchesUp = getY() < mMin.y;
        bool touchesDown = getY() > mMax.y;

        if (mBoundBehaviour == EBoundBehaviour.BOUNCE)
        {
            if (getX() + mRegistryPoint.x + mWidth > mMax.x)
            {
                setX(mMax.x - mWidth - mRegistryPoint.x);
                setVelX(getVelX() * -1);
            }

            if (getX() + mRegistryPoint.x < mMin.x)
            {
                setX(mMin.x - mRegistryPoint.x);
                setVelX(getVelX() * -1);
            }

            if (getY() + mRegistryPoint.y + mHeight > mMax.y)
            {
                setY(mMax.y - mRegistryPoint.y - mHeight);
                setVelY(getVelY() * -1);
            }
            if (getY() + mRegistryPoint.y < mMin.y)
            {
                setY(mMin.y - mRegistryPoint.y);
                setVelY(getVelY() * -1);
            }
        }
        if (mBoundBehaviour == EBoundBehaviour.WRAP)
        {
            if (getX() > mMax.x)
            {
                setX(mMin.x);
            }
            if (getX() < mMin.x)
            {
                setX(mMax.x);
            }
            if (getY() > mMax.y)
            {
                setY(mMin.y);
            }
            if (getY() < mMin.y)
            {
                setY(mMax.y);
            }
        }
        //stops when bounds of the map are reached
        if (mBoundBehaviour == EBoundBehaviour.STOP || mBoundBehaviour == EBoundBehaviour.DIE)
        {
            var entered = false;
            if (getX() + mWidth > mMax.x)
            {
                setX(mMax.x - mWidth);
                setVelX(0);
                setAccelX(0);
                mLimitReached = true;
                entered = true;
            }

            if (getX() < mMin.x)
            {
                setX(mMin.x);
                setVelX(0);
                setAccelX(0);
                mLimitReached = true;
                entered = true;
            }

            if (getY() + mHeight > mMax.y)
            {
                setY(mMax.y - mHeight);
                setVelY(0);
                setAccelY(0);
                mLimitReached = true;
                entered = true;
            }

            if (getY() < mMin.y)
            {
                setY(mMin.y);
                setVelY(0);
                setAccelY(0);
                mLimitReached = true;
                entered = true;
            }
            if (!entered)
            {
                mLimitReached = false;
            }
        }
        mTimeState++;
    }

    public void stopMove()
    {
        setVelX(0);
        setVelY(0);
        setAccelX(0);
        setAccelY(0);
    }

    public void setBoundAction(EBoundBehaviour aBoundAction)
    {
        mBoundBehaviour = aBoundAction;
    }

    public void setBounds(double aMinX, double aMinY, double aMaxX, double aMaxY)
    {
        mMin.x = aMinX;
        mMin.y = aMinY;
        mMax.x = aMaxX;
        mMax.y = aMaxY;
    }


    public void setBounds(CVector2D aMin, CVector2D aMax)
    {
        mMin.x = aMin.x;
        mMin.y = aMin.y;
        mMax.x = aMax.x;
        mMax.y = aMax.y;
    }

    public CVector2D getMaxBounds()
    {
        var copy = mMax.copy();
        return copy;
    }

    public CVector2D getMinBoounds()
    {
        var copy = mMin.copy();
        return copy;
    }

    public virtual void setState(uint aState)
    {
        mState = aState;
        mTimeState = 0;
    }

    public uint getState()
    {
        return mState;
    }

    protected virtual void manageStates()
    {

    }

    public virtual void sendMessage(EMessage aMessage)
    {
        mMessage = aMessage;
    }

    public virtual void render()
    {

    }

    public virtual void destroy()
    {

    }

    public void setDead(bool aDead)
    {
        mDead = aDead;
    }

    public bool isDead()
    {
        return mDead;
    }

    public void kill()
    {
        mDead = true;
    }

    public void redirect()
    {
        mRedirect = true;
    }

    public uint getHeight()
    {
        return mHeight;
    }

    public void setHeight(uint aHeight)
    {
        mHeight = aHeight;
        if (mRectangle != null)
        {
            mRectangle.height = aHeight;
        }
    }

    public uint getWidth()
    {
        return mWidth;
    }

    public void setWidth(uint aWidth)
    {
        mWidth = aWidth;
        if (mRectangle != null)
        {
            mRectangle.width = aWidth;
        }
    }

    public uint getRadious()
    {
        return mRadious;
    }

    public void setRadious(uint aRadious)
    {
        mRadious = aRadious;
        if (mCircle != null)
        {
            mCircle.mRadious = aRadious;
        }
    }

    public void setAngle(double degrees)
    {
        mAngle = CMath.clampDeg(degrees);
    }

    public double getAngle()
    {
        return mAngle;
    }


    public void lookAt(double ax, double ay)
    {
        double dx = ax - getX();
        double dy = ay - getY();
        setAngle(CMath.rad2deg(Math.Atan2(dy, dx)));
    }

    public double lookAtGetAngle(double ax, double ay)
    {
        double dx = ax - getX();
        double dy = ay - getY();
        return CMath.rad2deg(Math.Atan2(dy, dx));
    }

    public double lookAtGetAngle(CVector3D aPosition)
    {
        double dx = aPosition.x - getX();
        double dy = aPosition.y - getY();
        return CMath.clampDeg(CMath.rad2deg(Math.Atan2(dy, dx)));
    }

    public void setName(string aName)
    {
        mGameObject.name = aName;
    }

    public string getName()
    {
        return mGameObject.name;
    }

    public void setRegistryPointXY(int x, int y)
    {
        mRegistryPoint.x = x;
        mRegistryPoint.y = y;
    }
    public void setRegistryPoint(CVector2D vec)
    {
        mRegistryPoint = vec;
    }
    public CVector2D getRegistryPoint()
    {
        return mRegistryPoint;
    }

    public void setPivot(int x, int y)
    {
        mPivot.x = x;
        mPivot.y = y;
    }
    public void setPivot(CVector2D vec)
    {
        mPivot = vec;
    }
    public CVector2D getPivot()
    {
        return mPivot;
    }

    public void setGuid(double aGuid)
    {
        mGuid = aGuid;
    }

    public double getGuid()
    {
        return mGuid;
    }

    public double distanceTo(CGameObject aGameObject)
    {
        CVector3D tThisPos = new CVector3D();
        CVector3D tObjectPos = new CVector3D();
        if (mShape == EShapes.CIRCLE)
        {
            tThisPos = new CVector3D(getX(), getY());
        }
        else if (mShape == EShapes.RECT)
        {
            tThisPos = new CVector3D(getX() + getWidth() * 0.5, getY() + getWidth() * 0.5);
        }

        if (aGameObject.mShape == EShapes.CIRCLE)
        {
            tObjectPos = new CVector3D(aGameObject.getX(), aGameObject.getY());
        }
        else if (aGameObject.mShape == EShapes.RECT)
        {
            tObjectPos = new CVector3D(aGameObject.getX() + aGameObject.getWidth() * 0.5, aGameObject.getY() + aGameObject.getWidth() * 0.5);
        }

        //return tThisPos.substract(tObjectPos).norm();
        return Math.Sqrt((tThisPos.x - tObjectPos.x) * (tThisPos.x - tObjectPos.x) + (tThisPos.y - tObjectPos.y) * (tThisPos.y - tObjectPos.y));
    }

    public virtual CVector3D getCenter()
    {
        if (mShape == EShapes.CIRCLE)
            return new CVector3D(getX(), getY());
        else if (mShape == EShapes.RECT)
            return new CVector3D(getX() + getRegistryPoint().x + getWidth() * 0.5, getY() + getRegistryPoint().y + getHeight() * 0.5);
        return null;
    }

    public bool convertToBitmap()
    {
        //BitmapData bmd1;
        //if (bmp != null)
        //{
        //    return true;
        //}
        //else if (mc != null)
        //{
        //    bmd1 = new BitmapData(mc.height, mc.width, false, 0x000000);
        //    bmd1.draw(mc);
        //    bmp = new Bitmap(bmd1);
        //    bmp.x = mc.x;
        //    bmp.y = mc.y;
        //}
        //else if (mSprite != null)
        //{
        //    bmd1 = new BitmapData(mSprite.height, mSprite.width, false, 0x000000);
        //    bmd1.draw(mSprite);
        //    bmp = new Bitmap(bmd1);
        //    bmp.x = mSprite.x;
        //    bmp.y = mSprite.y;
        //}

        return false;
    }

    public void addBehaviour(CGenericBehaviour aBehaviour)
    {
        mBehaviours.Add(aBehaviour);
    }

    public List<CGenericBehaviour> getBehaviours()
    {
        return mBehaviours;
    }

    public CSoundAndMusic getMusic()
    {
        return mMusic;
    }

    public CSoundAndMusic getSound()
    {
        return mSound;
    }

    public void setMusic(CSoundAndMusic aMusic)
    {
        mMusic = aMusic;
    }

    public void setSound(CSoundAndMusic aSound)
    {
        mSound = aSound;
    }

    internal string getId()
    {
        return mId.ToString();
    }

    internal void setFeet(CRect aFeet)
    {
        mFeet = aFeet;
    }


    internal CShape getFeet()
    {
        return mFeet;
    }

    internal double getHeightSmart()
    {
        if (mFeet != null)
        {
            if (mFeet is CRect)
            {
                return ((CRect)mFeet).height;
            }
            else
            {
                return getHeight();
            }
        }
        else
        {
            return getHeight();
        }
    }

    internal float getWidthSmart()
    {
        if (mFeet != null)
        {
            if (mFeet is CRect)
            {
                return ((CRect)mFeet).width;
            }
            else
            {
                return getWidth();
            }
        }
        else
        {
            return getWidth();
        }
    }

}