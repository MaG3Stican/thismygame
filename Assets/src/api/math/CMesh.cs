﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CMesh
{
    public List<Vector3> vertices = new List<Vector3>();
    public List<int> triangles = new List<int>();
    public List<Vector2> uv = new List<Vector2>();
    public List<Vector3> normals = new List<Vector3>();
    private MeshFilter meshFilter;
    public Mesh internalMesh;


    public CMesh()
    {

    }

    public CMesh(MeshFilter meshFilter, Color aColor)
    {
        this.meshFilter = meshFilter;

        Mesh mesh = new Mesh();
        mesh = meshFilter.mesh;
        mesh.Clear();
        internalMesh = mesh;
        var renderer = (MeshRenderer)meshFilter.renderer;
        renderer.material = Resources.Load("Materials/Additive", typeof(Material)) as Material;
        renderer.material.color = aColor;
    }



    public void completeMeshWithVerticesData()
    {
        System.Random rand = new System.Random();

        int tri = 0;
        //add triangles
        vertices.ForEach(b => { triangles.Add(tri); tri++; });
        //add uv
        vertices.ForEach(b => uv.Add(new Vector2(rand.Next(2), rand.Next(2))));
        //ad normals
        var normalized = vertices[vertices.Count - 1].normalized;
        //this is done because we are looking opposite to were unity thinks normals should be pointing at
        normalized *= -1;
        vertices.ForEach(b => normals.Add(normalized));
    }
}
