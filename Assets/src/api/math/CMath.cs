
using ClipperLib;
using System;
using System.Collections.Generic;
using UnityEngine;
using Path = System.Collections.Generic.List<ClipperLib.IntPoint>;
using Paths = System.Collections.Generic.List<System.Collections.Generic.List<ClipperLib.IntPoint>>;
using System.Linq;
public class CMath
{
    public const double INFINITY = int.MaxValue;

    public CMath()
    {

    }

    public static double getTopOfCircle(CCircle aCircle)
    {
        return aCircle.mCenter.y - aCircle.mRadious;
    }


    public static bool shapeAndPolygonCollision(CShape aShape, CPolygon aCollider)
    {
        if (aShape != null)
        {
            Paths subj = new Paths(1);
            if (aShape is CCircle)
            {


            }
            else if (aShape is CRect)
            {
                var rec = aShape as CRect;
                subj.Add(new Path(4));
                subj[0].Add(new IntPoint(rec.x, rec.y));
                subj[0].Add(new IntPoint(rec.x + rec.width, rec.y));
                subj[0].Add(new IntPoint(rec.x + rec.width, rec.y + rec.height));
                subj[0].Add(new IntPoint(rec.x, rec.y + rec.height));

                UnityEngine.Debug.DrawLine(new Vector3((float)rec.x, (float)rec.y * -1), new Vector3((float)rec.x + rec.width, (float)rec.y * -1));
                UnityEngine.Debug.DrawLine(new Vector3((float)rec.x + rec.width, (float)rec.y * -1), new Vector3((float)rec.x + rec.width, (float)(rec.y + rec.height) * -1));
                UnityEngine.Debug.DrawLine(new Vector3((float)rec.x + rec.width, (float)(rec.y + rec.height) * -1), new Vector3((float)rec.x, (float)(rec.y + rec.height) * -1));
                UnityEngine.Debug.DrawLine(new Vector3((float)rec.x, (float)(rec.y + rec.height) * -1), new Vector3((float)rec.x, (float)rec.y * -1));
            }
            else
            {
                throw new Exception("You specified an invalid shape");
            }

            Paths clip = new Paths(1);
            clip.Add(new Path(aCollider.mPoints.Count));

            Vector3 prev = new Vector3();
            foreach (CVector2D point in aCollider.mPoints)
            {
                clip[0].Add(new IntPoint(point.x, point.y * -1));
                if (prev.x != 0)
                {
                    UnityEngine.Debug.DrawLine(new Vector3((float)prev.x, (float)prev.y), new Vector3((float)point.x, (float)point.y));
                }
                prev = new Vector3((float)point.x, (float)point.y);
            }

            //Paths solution = new Paths();

            //Clipper c = new Clipper();

            Paths solution = new Paths();

            Clipper c = new Clipper();
            c.AddPaths(subj, PolyType.ptSubject, true);
            c.AddPaths(clip, PolyType.ptClip, true);
            c.Execute(ClipType.ctIntersection, solution,
              PolyFillType.pftEvenOdd, PolyFillType.pftEvenOdd);


            if (solution.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            throw new Exception("You specified a null shape");
        }
    }


    public static float vectorToAngle(CVector2D vector)
    {
        return (float)Math.Atan2(vector.y, vector.x);
    }

    public static float vectorToAngle(CVector3D vector)
    {
        return (float)Math.Atan2(vector.y, vector.x);
    }

    public static double norm(double aX, double aY)
    {
        return Math.Sqrt(aX * aX + aY * aY);
    }

    public static uint dist(double x1, double y1, double x2, double y2)
    {
        return (uint)norm(x2 - x1, y2 - y1);
    }

    public static bool rectRectCollision(CRect aRect1, CRect aRect2)
    {
        if ((((aRect1.x + aRect1.width) > aRect2.x) && (aRect1.x < (aRect2.x + aRect2.width))) && (((aRect1.y + aRect1.height) > aRect2.y) && (aRect1.y < (aRect2.y + aRect2.height))))
            return true;
        else
            return false;
    }

    public static bool contains(CRect rect1,
                                  CRect rect2)
    {

        double x0 = rect1.x;
        double y0 = rect1.y;
        double x = rect2.x;
        double y = rect2.y;
        double w = rect2.width;
        double h = rect2.height;

        return ((x >= x0) && (y >= y0)
                && ((x + w) <= (x0 + rect1.width))
                && ((y + h) <= (y0 + rect1.height)));

    }

    public static bool circleRectCollision(double aX1, double aY1, double aW1, double aH1, double aX2, double aY2, double aR2)
    {
        if (aX2 >= aX1 && aX2 <= aX1 + aW1)
        {
            return aY2 + aR2 >= aY1 && aY2 - aR2 <= aY1 + aH1;
        }
        else if (aY2 >= aY1 && aY2 <= aY1 + aH1)
        {
            return aX2 + aR2 >= aX1 && aX2 - aR2 <= aX1 + aW1;
        }
        else if (aX2 < aX1 && aY2 < aY1)
        {
            return dist(aX1, aY1, aX2, aY2) <= aR2;
        }
        else if (aX2 > aX1 + aW1 && aY2 < aY1)
        {
            return dist(aX1 + aW1, aY1, aX2, aY2) <= aR2;
        }
        else if (aX2 < aX1 && aY2 > aY1 + aH1)
        {
            return dist(aX1, aY1 + aH1, aX2, aY2) <= aR2;
        }
        else if (aX2 > aX1 + aW1 && aX2 > aY1 + aH1)
        {
            return dist(aX1 + aW1, aY1 + aH1, aX2, aY2) <= aR2;
        }
        return false;
    }
    public static bool circleRectCollision(CRect aRect, CCircle aCircle)
    {
        double aX1 = aRect.x, aY1 = aRect.y, aW1 = aRect.width, aH1 = aRect.height;
        double aX2 = aCircle.mCenter.x, aY2 = aCircle.mCenter.y, aR2 = aCircle.mRadious;

        if (aX2 >= aX1 && aX2 <= aX1 + aW1)
        {
            return aY2 + aR2 >= aY1 && aY2 - aR2 <= aY1 + aH1;
        }
        else if (aY2 >= aY1 && aY2 <= aY1 + aH1)
        {
            return aX2 + aR2 >= aX1 && aX2 - aR2 <= aX1 + aW1;
        }
        else if (aX2 < aX1 && aY2 < aY1)
        {
            return dist(aX1, aY1, aX2, aY2) <= aR2;
        }
        else if (aX2 > aX1 + aW1 && aY2 < aY1)
        {
            return dist(aX1 + aW1, aY1, aX2, aY2) <= aR2;
        }
        else if (aX2 < aX1 && aY2 > aY1 + aH1)
        {
            return dist(aX1, aY1 + aH1, aX2, aY2) <= aR2;
        }
        else if (aX2 > aX1 + aW1 && aX2 > aY1 + aH1)
        {
            return dist(aX1 + aW1, aY1 + aH1, aX2, aY2) <= aR2;
        }
        return false;
    }

    public static int randInt(int aLow, int aHigh)
    {
        return UnityEngine.Random.Range(aLow, aHigh); //Math.Floor(Math.Random() * (1 + aHigh - aLow)) + aLow;
    }
    public static int randInt(double aLow, double aHigh)
    {
        return UnityEngine.Random.Range((int)aLow, (int)aHigh); //Math.Floor(Math.Random() * (1 + aHigh - aLow)) + aLow;
    }

    public static CVector2D randIntVec(CVector2D aV1, CVector2D aV2)
    {
        CVector2D newVec = new CVector2D();
        newVec.x = UnityEngine.Random.Range((int)aV1.x, (int)aV2.x); //Math.Floor(Math.Random() * (1 + aHigh - aLow)) + aLow;
        newVec.y = UnityEngine.Random.Range((int)aV1.y, (int)aV2.y);
        return newVec;
    }

    public static bool isEven(int aNum)
    {
        return (aNum % 2 == 0);
    }


    public static int addZeros(double aNum, uint aLength)
    {
        string numString = aNum.ToString();
        double numZeros = aLength - numString.Length;
        string result = "";
        for (int i = 0; i < numZeros; i++)
        {
            result += "0";
        }
        return Convert.ToInt32(result + numString);
    }

    public static double deg2rad(double aDeg)
    {
        return aDeg * Math.PI / 180;
    }

    public static double rad2deg(double aRad)
    {
        return aRad * 180 / Math.PI;
    }

    public static double getSign(double aNum)
    {
        return aNum / Math.Abs(aNum);
    }
    public static double clampDeg(double aDeg)
    {
        aDeg = aDeg % 360;
        if (aDeg < 0)
            aDeg += 360;
        return aDeg;
    }

    public static double clampCustom(double aDeg, float aMuchClamp)
    {
        aDeg = aDeg % aMuchClamp;
        if (aDeg < 0)
            aDeg += aMuchClamp;
        return aDeg;
    }

    public static double clampRad(double aRad)
    {
        aRad = aRad % (2 * Math.PI);
        if (aRad < 0)
            aRad += 2 * Math.PI;
        return aRad;
    }


    public static List<double> addToAngle(double angle, double addition, bool untilEnd)
    {
        if (360 % addition != 0)
        {
            throw new Exception("360 degrees must be divisible by the addition factor having a module of 0");
        }
        List<double> angles = new List<double>();
        if (!untilEnd)
        {
            angles.Add(clampDeg(angle + addition));
        }
        else
        {
            double lastAngle = -1;
            while (lastAngle != angle)
            {
                if (lastAngle != -1)
                {
                    lastAngle = clampDeg(lastAngle + addition);
                }
                else
                {
                    lastAngle = clampDeg(angle + addition);
                }
                angles.Add(lastAngle);
            }
        }
        return angles;
    }

    public static double percentage(double current, double maximum)
    {
        return (current * 100 / maximum);
    }


    public static double distanceBetweenPoints(CVector2D pointA, CVector2D pointB)
    {
        double deltaX = pointA.x - pointB.x;
        double deltaY = pointA.y - pointB.y;
        return Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
    }


    public static double distanceBetweenPoints3D(CVector3D pointA, CVector3D pointB)
    {
        return Math.Sqrt((pointB.x - pointA.x) * (pointB.x - pointA.x) + (pointB.y - pointA.y) * (pointB.y - pointA.y) + (pointB.z - pointA.z) * (pointB.z - pointA.z));
    }

    public static double rotateAngle(double aDeg)
    {
        aDeg += 180;
        return CMath.clampDeg(aDeg);
    }

    public static bool circleCircleCollision(CCircle aCircle1, CCircle aCircle2)
    {

        double radius = aCircle1.mRadious + aCircle2.mRadious;
        double deltaX = aCircle1.mCenter.x - aCircle2.mCenter.x;
        double deltaY = aCircle1.mCenter.y - aCircle2.mCenter.y;
        return deltaX * deltaX + deltaY * deltaY <= radius * radius;

    }

    public static bool rectCircCollision(CCircle circle, CRect rectangle)
    {
        CVector2D rectangleCenter = new CVector2D((rectangle.x + rectangle.width * 0.5), (rectangle.y + rectangle.height * 0.5));

        double w = rectangle.width * 0.5;
        double h = rectangle.height * 0.5;

        double dx = Math.Abs(circle.mCenter.x - rectangleCenter.x);
        double dy = Math.Abs(circle.mCenter.y - rectangleCenter.y);

        if (dx > (circle.mRadious + w) || dy > (circle.mRadious + h))
        {
            return false;
        }

        CVector2D circleDistance = new CVector2D((double)Math.Abs(circle.mCenter.x - rectangle.x - w), (double)Math.Abs(circle.mCenter.y - rectangle.y - h));

        if (circleDistance.x <= (w))
        {
            return true;
        }

        if (circleDistance.y <= (h))
        {
            return true;
        }

        double cornerDistanceSq = Math.Pow(circleDistance.x - w, 2) + Math.Pow(circleDistance.y - h, 2);

        return (cornerDistanceSq <= (Math.Pow(circle.mRadious, 2)));
    }


    public static void rotateAroundCenter(GameObject aGraphic, float angle)
    {
        var pivot = aGraphic.renderer.bounds.center;
        aGraphic.transform.RotateAround(pivot, Vector3.left, angle);
        //var matrix:Matrix = mc.transform.matrix;

        //var rect:Rectangle = mc.getBounds(mc.parent);

        //matrix.translate(-(rect.left + (rect.width / 2)), -(rect.top + (rect.height / 2)));

        //matrix.rotate((angleDegrees / 180) * Math.PI);

        //matrix.translate(rect.left + (rect.width / 2), rect.top + (rect.height / 2));

        //mc.transform.matrix = matrix;
    }

    public static int RGB(int aR, int aG, int aB)
    {
        return aR << 16 | aG << 8 | aB;
    }

    public static CVector3D wander(CVector3D aPos, double aRadius)
    {
        int tAngle = randInt(0, 359);
        CVector3D tVector = new CVector3D(aRadius);
        tVector.rotateVect(tAngle);
        tVector.addVec(aPos);
        return tVector;
    }

    public static float randFloat()
    {
        System.Random random = new System.Random();
        return (float)random.NextDouble();
    }

    public static bool shapeAndCircleCollision(CShape aShape, CRect aBehaviourArea)
    {
        if (aShape is CCircle)
        {
            return rectCircCollision((CCircle)aShape, aBehaviourArea);
        }
        else if (aShape is CRect)
        {
            return rectRectCollision(aBehaviourArea, (CRect)aShape);
        }
        else
        {
            throw new Exception("You specified an invalid shape");
        }
    }



    public static bool isPointInPolygon(CShape aShape, CVector2D aPoint)
    {
        List<CVector2D> aPolygon = new List<CVector2D>();
        if (aShape is CRect)
        {
            aPolygon = ((CRect)aShape).toPolygon();
        }

        bool isInside = false;
        for (int i = 0, j = aPolygon.Count - 1; i < aPolygon.Count; j = i++)
        {
            if (((aPolygon[i].y > aPoint.y) != (aPolygon[j].y > aPoint.y)) &&
            (aPoint.x < (aPolygon[j].x - aPolygon[i].x) * (aPoint.y - aPolygon[i].y) / (aPolygon[j].y - aPolygon[i].y) + aPolygon[i].x))
            {
                isInside = !isInside;
            }
        }
        return isInside;
    }


    public static CVector2D getPointInCircleWithAngleAndCenter(CVector3D aCenter, int aAngle, int aRadious)
    {
        var holder = new CVector2D();
        holder.x = aCenter.x + aRadious * Math.Cos(CMath.deg2rad(aAngle));
        holder.y = aCenter.y + aRadious * Math.Sin(CMath.deg2rad(aAngle));

        return holder;
    }

    public static CVector2D getVectorBetween(CVector2D v1, CVector3D v2)
    {
        var x = Math.Abs(v1.x - v2.x);
        var y = Math.Abs(v1.y - v2.y);
        return new CVector2D(x, y);
    }

    public static double getTopOfPolygon(CPolygon cPolygon)
    {
        var copy = cPolygon.mPoints.ToList();
        //sort in ascending order
        copy.Sort(delegate(CVector2D A, CVector2D B)
         {
             return (int)(A.y - B.y);
         });

        return copy[0].y;
    }
}









