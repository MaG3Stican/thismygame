﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


class CTriangle
{
    public static List<CTriangle> holder = new List<CTriangle>();
    public Vector3 p1 = new Vector3();
    public Vector3 p2 = new Vector3();
    public Vector3 p3 = new Vector3();

    public CTriangle(Vector3 ap1, Vector3 ap2, Vector3 ap3)
    {
        p1 = ap1;
        p2 = ap2;
        p3 = ap3;
        holder.Add(this);
    }

}