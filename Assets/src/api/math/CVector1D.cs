﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//1 dimensional vectors are meant to hold 1 dimension reference so that it can be modified later for example
public class CVector1D
{
    public float mDimension = 0;

    public CVector1D()
    {

    }
    public CVector1D moveDimension(float value)
    {
        mDimension += value;

        return this;
    }
}

