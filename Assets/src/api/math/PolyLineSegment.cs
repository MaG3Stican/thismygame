﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class PolyLineSegment
{
    bool mIsStroked = false;
    public List<CVector2D> Lines {  get; set; }

    public PolyLineSegment()
    {
        Lines = new List<CVector2D>();
    }

    public PolyLineSegment(List<CVector2D> aLines, bool aStroked = false)
    {
        Lines = aLines;
        mIsStroked = aStroked;
    }



    
}
