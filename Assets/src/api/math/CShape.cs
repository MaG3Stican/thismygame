﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class CShape
{
    public abstract List<CVector2D> toPolygon();
    public abstract CShape copy();

    public static GameObject DrawShape(bool aFilled, int aRadious, EShapes aShape, CVector3D aPosition, Color aColor, CLayer aLayer, List<CVector2D> aPoints = null)
    {

        if (aShape == EShapes.POLYGON && aPoints == null)
            throw new Exception("You are trying to draw a polygon shape but you did not specify the points to draw that polygon");

        var cameraPosition = aLayer.Z;
        //only way to draw so far is with a prefab..
        var obj = CHelper.getMeshed2DReadyObject("circle", "Sprites/prefabs/meshedObject");
        //points in circunference
        float thetaScale = 0.09f;
        //points used for circunference
        float points = (float)(3 * Math.PI) / thetaScale;
        var numSegments = 0;
        int radiousSize = aRadious;

        CMesh mesh = new CMesh(obj.GetComponent<MeshFilter>(), aColor);

        if (aShape != EShapes.POLYGON)
        {

            if (aShape == EShapes.CIRCLE)
            {
                numSegments = 80;
            }
            else if (aShape == EShapes.RECT)
            {
                numSegments = 4;
                //this does not work for some reason
                var quaternion = obj.transform.rotation;
                var angles = quaternion.eulerAngles;
                angles.z += -90;
                quaternion.eulerAngles = angles;
                obj.transform.rotation = quaternion;

            }
            else if (aShape == EShapes.TRIANGLE)
            {
                numSegments = 3;
            }

            float deltaTheta = (float)((2.0 * Mathf.PI) / numSegments);
            float theta = 0;

            CLine line = null;
            if (!aFilled)
            {
                line = new CLine(obj.GetComponent<LineRenderer>(), (int)points, aColor, numSegments);
            }
            CVector3D center = new CVector3D();

            center.z = cameraPosition;

            for (int i = 0; i < numSegments; i++)
            {
                float x = radiousSize * Mathf.Cos(theta);
                float y = radiousSize * Mathf.Sin(theta);
                CVector3D pos = new CVector3D(x, y, cameraPosition);

                if (!aFilled)
                    line.addPoint(i, pos);

                //get future point
                var thetaHolder = theta + deltaTheta;
                float xx = radiousSize * Mathf.Cos(thetaHolder);
                float yy = radiousSize * Mathf.Sin(thetaHolder);
                CVector3D pospos = new CVector3D(xx, yy, cameraPosition);
                //triangle
                mesh.vertices.Add(pos.toVec3());
                mesh.vertices.Add(center.toVec3());
                mesh.vertices.Add(pospos.toVec3());

                theta += deltaTheta;
            }
            //positionate
            aPosition = aPosition.opposite();
            obj.transform.position = new Vector3((float)aPosition.x, (float)aPosition.y, cameraPosition);

            mesh.completeMeshWithVerticesData();

            mesh.internalMesh.RecalculateNormals();
            mesh.internalMesh.RecalculateBounds();
            mesh.internalMesh.Optimize();
        }
        else
        {
            //polygon drawing
            for (int i = 0; i < aPoints.Count; i++)
            {
                mesh.vertices.Add(aPoints[i].toVec3d().toVec3());
            }
            mesh.completeMeshWithVerticesData();

            mesh.internalMesh.RecalculateNormals();
            mesh.internalMesh.RecalculateBounds();
            mesh.internalMesh.Optimize();
        }

        return obj;
    }


}
