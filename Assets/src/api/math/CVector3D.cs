
using System;
using UnityEngine;
public class CVector3D
{
    private double EPSILON = 0.000001;

    public double x;
    public double y;
    public double z;

    public double mAngle = 0;

    public CVector3D(double aX, double aY, double aZ)
    {
        x = aX;
        y = aY;
        z = aZ;
    }

    public CVector3D opposite()
    {
        var xx = x * -1;
        var yy = y * -1;
        return new CVector3D(xx, yy);
    }

    public CVector3D(double aX, double aY)
    {
        x = aX;
        y = aY;
    }

    public CVector3D(double aX)
    {
        x = aX;
    }

    public CVector3D()
    {
    }

    public CVector3D inverse()
    {
        var xx = x * -1;
        var yy = y * -1;
        return new CVector3D(xx, yy, z);
    }

    public CVector3D copy()
    {
        CVector3D vect = new CVector3D();
        vect.x = x;
        vect.y = y;
        vect.z = z;
        return vect;
    }

    public void setX(double aX)
    {
        x = aX;
    }
    public void setY(double aY)
    {
        y = aY;
    }
    public void setZ(double aZ)
    {
        z = aZ;
    }
    public void setXYZ(double aX, double aY, double aZ = 0)
    {
        x = aX;
        y = aY;
        z = aZ;
    }

    public double norm()
    {
        return Math.Sqrt(x * x + y * y + z * z);
    }

    public void normalize()
    {
        var m = magnitude();
        // Check division by zero.
        if (m > EPSILON)
        {
            x = x / m;
            y = y / m;
        }
    }

    public CVector3D normalized
    {
        get
        {
            CVector3D v = new CVector3D();
            var m = magnitude();
            // Check division by zero.
            if (m > EPSILON)
            {
                v.x = x / m;
                v.y = y / m;
                v.z = z / m;
            }
            return v;
        }
    }

    public CVector3D addNewVec(CVector3D aVect)
    {
        CVector3D vect = new CVector3D();
        vect.x = x + aVect.x;
        vect.y = y + aVect.y;
        vect.z = z + aVect.z;
        return vect;
    }

    public CVector2D addNewVec(CVector2D aVect)
    {
        CVector2D vect = new CVector2D();
        vect.x = x + aVect.x;
        vect.y = y + aVect.y;
        return vect;
    }

    public void addVec(CVector3D aVect)
    {
        x += aVect.x;
        y += aVect.y;
        z += aVect.z;
    }
    public void addVec(CVector2D aVect)
    {
        x += aVect.x;
        y += aVect.y;
    }

    public CVector3D substractReturn(CVector3D aVect)
    {
        CVector3D vect = new CVector3D();
        vect.x = x - aVect.x;
        vect.y = y - aVect.y;
        vect.z = z - aVect.z;
        return vect;
    }

    public void substractVect(CVector3D aVect)
    {
        x -= aVect.x;
        y -= aVect.y;
        z -= aVect.z;
    }

    public CVector3D timesScalar(double aNum)
    {
        CVector3D vect = new CVector3D();
        vect.x = x * aNum;
        vect.y = y * aNum;
        vect.z = z * aNum;
        return vect;
    }

    public void mult(double aNum)
    {
        x = x * aNum;
        y = y * aNum;
        z = z * aNum;
    }

    public double magnitude()
    {
        return Math.Sqrt(x * x + y * y);

    }

    public static double dotProduct(CVector3D aV1, CVector3D aV2)
    {
        //TODO MAKE LANGUAGE INDEPENDENT CODE HERE
        return Vector3.Dot(new Vector3((float)aV1.x, (float)aV1.y, (float)aV1.z), new Vector3((float)aV2.x, (float)aV2.y, (float)aV2.z));
    }

    public static CVector3D vec3ToVec3D(Vector3 aV1)
    {
        return new CVector3D((float)aV1.x, (float)aV1.y, (float)aV1.z);
    }

    public Vector3 toVec3()
    {
        return new Vector3((float)x, (float)y, (float)z);
    }

    //this is prefered over magnitude because is faster
    public double sqrMagnitude()
    {
        return x * x + y * y + z * z;
    }


    public void truncate(double aLength)
    {
        if (magnitude() > aLength)
        {
            normalize();
            mult(aLength);
        }
    }

    public CVector3D plusScalar(double aNum)
    {
        CVector3D vect = new CVector3D();
        vect.x = x + aNum;
        vect.y = y + aNum;
        vect.z = z + aNum;
        return vect;
    }
    public void sum(double aNum)
    {
        x += aNum;
        y += aNum;
        z += aNum;
    }

    public void sum(CVector3D aV)
    {
        x = x + aV.x;
        y = y + aV.y;
        z = z + aV.z;
    }

    public CVector3D sumReturn(CVector3D aV)
    {
        CVector3D vect = new CVector3D();
        vect.x = x + aV.x;
        vect.y = y + aV.y;
        vect.z = z + aV.z;
        return vect;
    }

    public CVector3D sumReturn(int mag)
    {
        CVector3D vect = new CVector3D();
        vect.x = x + mag;
        vect.y = y + mag;
        vect.z = z + mag;
        return vect;
    }

    public double scalarProduct(CVector3D aVect)
    {
        return x * aVect.x + y * aVect.y + z * aVect.z;
    }

    public CVector3D vectorProduct(CVector3D aVect)
    {
        CVector3D vect = new CVector3D();
        vect.x = y * aVect.z - z * aVect.y;
        vect.y = z * aVect.x - x * aVect.z;
        vect.z = x * aVect.y - y * aVect.x;
        return vect;
    }

    public double getAngle()
    {
        return CMath.vectorToAngle(this);
    }

    public CVector3D rotate(double mAngle)
    {
        CVector3D vect = new CVector3D();
        vect.x = x * Math.Cos(mAngle) - y * Math.Sin(mAngle);
        vect.y = x * Math.Sin(mAngle) + y * Math.Cos(mAngle);
        return vect;
    }

    public void rotateVect(double mAngle)
    {
        x = x * Math.Cos(mAngle) - y * Math.Sin(mAngle);
        y = x * Math.Sin(mAngle) + y * Math.Cos(mAngle);
    }

    public void setMag(double aMag)
    {
        double angle = getAngle();
        x = aMag * Math.Cos(angle);
        y = aMag * Math.Sin(angle);
    }

    public void setAngle(double aAngle)
    {
        double m = magnitude();
        x = Math.Cos(CMath.deg2rad(aAngle)) * m;
        y = Math.Sin(CMath.deg2rad(aAngle)) * m;
        mAngle = aAngle;
    }

    public void setAngleMag(double aAngle, double aMagnitude)
    {
        double m = aMagnitude;
        x = Math.Cos(CMath.deg2rad(aAngle)) * m;
        y = Math.Sin(CMath.deg2rad(aAngle)) * m;
        mAngle = aAngle;

    }

    public double getNormalAngle()
    {
        return Math.Atan2(y, x);
    }



    public static CVector3D operator -(CVector3D aV1, CVector3D aV2)
    {
        //possible error, do we create a new vector or modify the existing one?
        return aV1.substractReturn(aV2);
    }

    public static CVector3D operator +(CVector3D aV1, CVector3D aV2)
    {
        //possible error, do we create a new vector or modify the existing one?
        return aV1.sumReturn(aV2);
    }

    public static CVector3D operator +(CVector3D aV1, int mag)
    {
        //possible error, do we create a new vector or modify the existing one?
        return aV1.sumReturn(mag);
    }


    public static CVector3D operator *(CVector3D aV1, CVector3D aV2)
    {
        return aV1.vectorProduct(aV2);
    }

    public static CVector3D operator *(CVector3D aV1, double aMag)
    {
        return aV1.timesScalar(aMag);
    }
    public static CVector3D operator *(double aMag, CVector3D aV1)
    {
        return aV1.timesScalar(aMag);
    }



    public bool pointsRight
    {
        get
        {
            if (x > 0)
                return true;
            else if (x < 0)
                return false;
            else
                return false;
        }
    }

    public bool pointsLeft
    {
        get
        {
            if (x < 0)
                return true;
            else if (x > 0)
                return false;
            else
                return false;
        }
    }

    public bool pointsDown
    {
        get
        {
            if (y > 0)
                return true;
            else if (y < 0)
                return false;
            else
                return false;
        }
    }
    public bool pointsUp
    {
        get
        {
            if (y < 0)
                return true;
            else if (y > 0)
                return false;
            else
                return false;
        }
    }

    public void flip()
    {
        x *= -1;
        y *= -1;
    }

    public CVector3D moveX(int aX)
    {
        var vec = new CVector3D(x, y);
        vec.x += aX;

        return vec;
    }

    internal CVector3D moveY(int aY)
    {
        var vec = new CVector3D(x, y);

        vec.y += aY;

        return vec;
    }

    public bool pointsDownLeft
    {
        get
        {
            if (pointsDown && pointsLeft)
                return true;
            else
                return false;
        }
    }

    public bool pointsDownRight
    {
        get
        {
            if (pointsDown && pointsRight)
                return true;
            else
                return false;
        }
    }

    public bool pointsUpLeft
    {
        get
        {
            if (pointsUp && pointsLeft)
                return true;
            else
                return false;
        }
    }

    public bool pointsUpRight
    {
        get
        {
            if (pointsUp && pointsRight)
                return true;
            else
                return false;
        }
    }

    public CVector2D toVec2d()
    {
        return new CVector2D(x, y);
    }

}