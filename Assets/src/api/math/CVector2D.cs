
/**
 * ...
 * @author Magestican
 */
using System;
using UnityEngine;
public class CVector2D
{
    public double x = 0;
    public double y = 0;


    private const double EPSILON = 0.000001;
    // TODO: Implementar.
    private double mAngle = EPSILON;
    //private var mMag:Number; 

    public CVector2D(double aX = 0, double aY = 0)
    {
        x = aX;
        y = aY;
    }
    //magnitude
    public double mMg = 0;


    public CVector2D opposite()
    {
        var xx = x * -1;
        var yy = y * -1;
        return new CVector2D(xx, yy);
    }


    public void negative()
    {
        if (x > 0)
        {
            x *= -1;
        }
        if (y > 0)
        {
            y *= -1;
        }
    }

    public void positive()
    {
        if (x < 0)
        {
            x *= -1;
        }
        if (y < 0)
        {
            y *= -1;
        }
    }

    public void negativeX()
    {
        if (x > 0)
        {
            x *= -1;
        }
    }

    public void positiveX()
    {
        if (x < 0)
        {
            x *= -1;
        }
    }

    public void setXY(double aX, double aY)
    {
        x = aX;
        y = aY;
    }

    public void sum(CVector2D aVec)
    {
        x += aVec.x;
        y += aVec.y;
    }

    public void subs(CVector2D aVec)
    {
        x -= aVec.x;
        y -= aVec.y;
    }

    public CVector2D copy()
    {
        CVector2D aVec = new CVector2D();
        aVec.x = x;
        aVec.y = y;
        return aVec;
    }

    public static CVector2D fromAngle(double angle, double magnitude)
    {
        return new CVector2D(magnitude * Math.Cos(angle), magnitude * Math.Sin(angle));
    }


    public CVector2D sumInNewVect(double aHowMuch)
    {
        return new CVector2D(this.x + aHowMuch, this.y + aHowMuch);
    }

    public void mul(double aScale)
    {
        x *= aScale;
        y *= aScale;
    }

    public double mag()
    {
        return Math.Sqrt(x * x + y * y);
    }

    public void truncate(int aLength)
    {
        if (mag() > aLength)
        {
            normalize();
            mul(aLength);
        }
    }

    public void normalize()
    {
        double m = mag();

        // Check division by zero.
        if (m > EPSILON)
        {
            x = x / m;
            y = y / m;
        }
    }

    // Returns angle in radians
    public double getAngle()
    {
        if (mAngle != EPSILON)
        {
            return mAngle;
        }
        else
        {
            mAngle = Math.Atan2(y, x);
            return mAngle;
        }
    }
    // Receives the angle in degrees
    // Changing the angle changes the x and y but retains the same magnitude.
    public void setAngle(double aAngle)
    {
        double m = mag();
        x = Math.Cos(CMath.deg2rad(aAngle)) * m;
        y = Math.Sin(CMath.deg2rad(aAngle)) * m;
        mAngle = aAngle;
    }

    public void setMag(double aMag)
    {
        double angle = getAngle();
        x = aMag * Math.Cos(angle);
        y = aMag * Math.Sin(angle);
        mMg = aMag;
    }

    public void setAngleMag(double aAngle, double aMag)
    {
        setMag(aMag);
        setAngle(aAngle);
    }

    public bool pointsRight
    {
        get
        {
            if (x > 0)
                return true;
            else if (x < 0)
                return false;
            else
                return false;
        }
    }

    public bool pointsLeft
    {
        get
        {
            if (x < 0)
                return true;
            else if (x > 0)
                return false;
            else
                return false;
        }
    }

    public bool pointsDown
    {
        get
        {
            if (y > 0)
                return true;
            else if (y < 0)
                return false;
            else
                return false;
        }
    }
    public bool pointsUp
    {
        get
        {
            if (y < 0)
                return true;
            else if (y > 0)
                return false;
            else
                return false;
        }
    }

    public void flip()
    {
        x *= -1;
        y *= -1;
    }

    internal CVector3D toVec3d()
    {
        return new CVector3D(x, y, 0);
    }

    internal Vector2 toVec2()
    {
        return new Vector2((float)x, (float)y);
    }

    public CVector2D moveX(int aX)
    {
        x += aX;

        return this;
    }

    internal CVector2D moveY(int aY)
    {
        y += aY;

        return this;
    }



    public bool equal(CVector2D aV2)
    {
        return x == aV2.x && y == aV2.y;

    }


}