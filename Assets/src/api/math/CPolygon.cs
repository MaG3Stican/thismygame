﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class CPolygon : CShape
{
    public List<CVector2D> mPoints = new List<CVector2D>();
    public CPolygon()
    {

    }

    public CPolygon(List<CVector2D> aPoints)
    {
        mPoints = aPoints;
    }

    public override List<CVector2D> toPolygon()
    {
        return mPoints;
    }


    public override CShape copy()
    {
        var copy = new List<CVector2D>();
        mPoints.ForEach(c => copy.Add(new CVector2D(c.x, c.y)));
        return new CPolygon(copy);
    }

}
