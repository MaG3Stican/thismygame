﻿
using UnityEngine;
using System.Collections.Generic;

/**
    Class for representing a Bezier path, and methods for getting suitable points to 
    draw the curve with line segments.
*/
public class CBezier
{
    private const int SEGMENTS_PER_CURVE = 10;
    private const double MINIMUM_SQR_DISTANCE = 0.01d;

    // This corresponds to about 172 degrees, 8 degrees from a traight line
    private const double DIVISION_THRESHOLD = -0.99d;

    private List<CVector3D> controlPoints;

    private int curveCount; //how many bezier curves in this path?

    /**
        Constructs a new empty Bezier curve. Use one of these methods
        to add points: SetControlPoints, Interpolate, SamplePoints.
    */
    public CBezier()
    {
        controlPoints = new List<CVector3D>();
    }

    /**
        Sets the control points of this Bezier path.
        Points 0-3 forms the first Bezier curve, points 
        3-6 forms the second curve, etc.
    */
    public void SetControlPoints(List<CVector3D> newControlPoints)
    {
        controlPoints.Clear();
        controlPoints.AddRange(newControlPoints);
        curveCount = (controlPoints.Count - 1) / 3;
    }

    /**
        Returns the control points for this Bezier curve.
    */
    public List<CVector3D> GetControlPoints()
    {
        return controlPoints;
    }

    /**
        Calculates a Bezier interpolated path for the given points.
    */
    public void Interpolate(List<CVector3D> segmentPoints, double scale)
    {
        controlPoints.Clear();

        if (segmentPoints.Count < 2)
        {
            return;
        }

        for (int i = 0; i < segmentPoints.Count; i++)
        {
            if (i == 0) // is first
            {
                CVector3D p1 = segmentPoints[i];
                CVector3D p2 = segmentPoints[i + 1];

                CVector3D tangent = (p2 - p1);
                CVector3D q1 = p1 + scale * tangent;

                controlPoints.Add(p1);
                controlPoints.Add(q1);
            }
            else if (i == segmentPoints.Count - 1) //last
            {
                CVector3D p0 = segmentPoints[i - 1];
                CVector3D p1 = segmentPoints[i];
                CVector3D tangent = (p1 - p0);
                CVector3D q0 = p1 - scale * tangent;

                controlPoints.Add(q0);
                controlPoints.Add(p1);
            }
            else
            {
                CVector3D p0 = segmentPoints[i - 1];
                CVector3D p1 = segmentPoints[i];
                CVector3D p2 = segmentPoints[i + 1];
                CVector3D tangent = (p2 - p0).normalized;
                CVector3D q0 = p1 - scale * tangent * (p1 - p0).magnitude();
                CVector3D q1 = p1 + scale * tangent * (p2 - p1).magnitude();

                controlPoints.Add(q0);
                controlPoints.Add(p1);
                controlPoints.Add(q1);
            }
        }

        curveCount = (controlPoints.Count - 1) / 3;
    }

    /**
        Sample the given points as a Bezier path.
    */
    public void SamplePoints(List<CVector3D> sourcePoints, double minSqrDistance, double maxSqrDistance, double scale)
    {
        if (sourcePoints.Count < 2)
        {
            return;
        }

        Stack<CVector3D> samplePoints = new Stack<CVector3D>();

        samplePoints.Push(sourcePoints[0]);

        CVector3D potentialSamplePoint = sourcePoints[1];

        int i = 2;

        for (i = 2; i < sourcePoints.Count; i++)
        {
            if (
                ((potentialSamplePoint - sourcePoints[i]).sqrMagnitude() > minSqrDistance) &&
                ((samplePoints.Peek() - sourcePoints[i]).sqrMagnitude() > maxSqrDistance))
            {
                samplePoints.Push(potentialSamplePoint);
            }

            potentialSamplePoint = sourcePoints[i];
        }

        //now handle last bit of curve
        CVector3D p1 = samplePoints.Pop(); //last sample point
        CVector3D p0 = samplePoints.Peek(); //second last sample point
        CVector3D tangent = (p0 - potentialSamplePoint).normalized;
        double d2 = (potentialSamplePoint - p1).magnitude();
        double d1 = (p1 - p0).magnitude();
        p1 = p1 + tangent * ((d1 - d2) / 2);

        samplePoints.Push(p1);
        samplePoints.Push(potentialSamplePoint);


        Interpolate(new List<CVector3D>(samplePoints), scale);
    }

    /**
        Caluclates a point on the path.
        
        @param curveIndex The index of the curve that the point is on. For example, 
        the second curve (index 1) is the curve with controlpoints 3, 4, 5, and 6.
        
        @param t The paramater indicating where on the curve the point is. 0 corresponds 
        to the "left" point, 1 corresponds to the "right" end point.
    */
    public CVector3D CalculateBezierPoint(int curveIndex, float t)
    {
        int nodeIndex = curveIndex * 3;

        CVector3D p0 = controlPoints[nodeIndex];
        CVector3D p1 = controlPoints[nodeIndex + 1];
        CVector3D p2 = controlPoints[nodeIndex + 2];
        CVector3D p3 = controlPoints[nodeIndex + 3];

        return CalculateBezierPoint(t, p0, p1, p2, p3);
    }

    /**
        Gets the drawing points. This implementation simply calculates a certain number
        of points per curve.
    */
    public List<CVector3D> GetDrawingPoints0()
    {
        List<CVector3D> drawingPoints = new List<CVector3D>();

        for (int curveIndex = 0; curveIndex < curveCount; curveIndex++)
        {
            if (curveIndex == 0) //Only do this for the first end point. 
            //When i != 0, this coincides with the 
            //end point of the previous segment,
            {
                drawingPoints.Add(CalculateBezierPoint(curveIndex, 0));
            }

            for (int j = 1; j <= SEGMENTS_PER_CURVE; j++)
            {
                float t = j / (float)SEGMENTS_PER_CURVE;
                drawingPoints.Add(CalculateBezierPoint(curveIndex, t));
            }
        }

        return drawingPoints;
    }

    /**
        Gets the drawing points. This implementation simply calculates a certain number
        of points per curve.

        This is a lsightly different inplementation from the one above.
    */
    public List<CVector3D> GetDrawingPoints1()
    {
        List<CVector3D> drawingPoints = new List<CVector3D>();

        for (int i = 0; i < controlPoints.Count - 3; i += 3)
        {
            CVector3D p0 = controlPoints[i];
            CVector3D p1 = controlPoints[i + 1];
            CVector3D p2 = controlPoints[i + 2];
            CVector3D p3 = controlPoints[i + 3];

            if (i == 0) //only do this for the first end point. When i != 0, this coincides with the end point of the previous segment,
            {
                drawingPoints.Add(CalculateBezierPoint(0, p0, p1, p2, p3));
            }

            for (int j = 1; j <= SEGMENTS_PER_CURVE; j++)
            {
                float t = j / (float)SEGMENTS_PER_CURVE;
                drawingPoints.Add(CalculateBezierPoint(t, p0, p1, p2, p3));
            }
        }

        return drawingPoints;
    }

    /**
        This gets the drawing points of a bezier curve, using recursive division,
        which results in less points for the same accuracy as the above implementation.
    */
    public List<CVector3D> GetDrawingPoints2()
    {
        List<CVector3D> drawingPoints = new List<CVector3D>();

        for (int curveIndex = 0; curveIndex < curveCount; curveIndex++)
        {
            List<CVector3D> bezierCurveDrawingPoints = FindDrawingPoints(curveIndex);

            if (curveIndex != 0)
            {
                //remove the fist point, as it coincides with the last point of the previous Bezier curve.
                bezierCurveDrawingPoints.RemoveAt(0);
            }

            drawingPoints.AddRange(bezierCurveDrawingPoints);
        }

        return drawingPoints;
    }

    List<CVector3D> FindDrawingPoints(int curveIndex)
    {
        List<CVector3D> pointList = new List<CVector3D>();

        CVector3D left = CalculateBezierPoint(curveIndex, 0);
        CVector3D right = CalculateBezierPoint(curveIndex, 1);

        pointList.Add(left);
        pointList.Add(right);

        FindDrawingPoints(curveIndex, 0, 1, pointList, 1);

        return pointList;
    }


    /**
        @returns the number of points added.
    */
    int FindDrawingPoints(int curveIndex, float t0, float t1,
        List<CVector3D> pointList, int insertionIndex)
    {
        CVector3D left = CalculateBezierPoint(curveIndex, t0);
        CVector3D right = CalculateBezierPoint(curveIndex, t1);

        if ((left - right).sqrMagnitude() < MINIMUM_SQR_DISTANCE)
        {
            return 0;
        }

        float tMid = (t0 + t1) / 2;
        CVector3D mid = CalculateBezierPoint(curveIndex, tMid);

        CVector3D leftDirection = (left - mid).normalized;
        CVector3D rightDirection = (right - mid).normalized;

        if (CVector3D.dotProduct(leftDirection, rightDirection) > DIVISION_THRESHOLD || Mathf.Abs(tMid - 0.5f) < 0.0001f)
        {
            int pointsAddedCount = 0;

            pointsAddedCount += FindDrawingPoints(curveIndex, t0, tMid, pointList, insertionIndex);
            pointList.Insert(insertionIndex + pointsAddedCount, mid);
            pointsAddedCount++;
            pointsAddedCount += FindDrawingPoints(curveIndex, tMid, t1, pointList, insertionIndex + pointsAddedCount);

            return pointsAddedCount;
        }

        return 0;
    }



    /**
        Caluclates a point on the Bezier curve represented with the four controlpoints given.
    */
    private CVector3D CalculateBezierPoint(double t, CVector3D p0, CVector3D p1, CVector3D p2, CVector3D p3)
    {
        double u = 1 - t;
        double tt = t * t;
        double uu = u * u;
        double uuu = uu * u;
        double ttt = tt * t;

        CVector3D p = uuu * p0; //first term

        p += 3 * uu * t * p1; //second term
        p += 3 * u * tt * p2; //third term
        p += ttt * p3; //fourth term

        return p;

    }
}
