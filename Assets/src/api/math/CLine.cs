﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CLine
{
    LineRenderer renderer;

    public CVector2D a = new CVector2D();
    public CVector2D b = new CVector2D();

    public CLine(LineRenderer aRenderer, int aPoints, Color aColor, int aSegments)
    {
        renderer = aRenderer;

        var red = CMath.randFloat();

        renderer.SetVertexCount((int)aPoints);
        renderer.material = new Material(Shader.Find("Particles/Additive"));
        renderer.SetColors(aColor, aColor);
        renderer.SetWidth(0.05f, 0.05f);
        renderer.SetVertexCount(aSegments + 1);
    }

    public CLine(CVector2D aA, CVector2D aB)
    {
        a = aA;
        b = aB;
    }

    public CLine(int aX, int aY, int bX, int bY)
    {
        a.x = aX;
        a.y = aY;
        b.x = bX;
        b.y = bY;
    }

    internal void addPoint(int aIndex, CVector3D aPos)
    {
        renderer.SetPosition(aIndex, aPos.toVec3());
    }
}