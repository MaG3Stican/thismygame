
using System;
using System.Collections.Generic;
using UnityEngine;
public class CCircle : CShape
{
    public uint mRadious;
    public CVector3D mCenter;

    public CCircle(uint aRadious = 0, CVector3D aCenter = null)
    {
        if (aCenter == null)
        {
            mCenter = new CVector3D();
        }
        else
        {
            mCenter = aCenter;
        }
        mRadious = aRadious;
    }




    public static GameObject DrawLine(CVector3D aPosition, CVector3D aDestination)
    {
        aPosition.y = aPosition.y * -1;
        aDestination.y = aDestination.y * -1;
        var obj = CHelper.getMeshed2DReadyObject("circle", "Sprites/prefabs/meshedObject");

        var pos = obj.transform.position;
        pos.z = -8;
        obj.transform.position = pos;

        var numSegments = 3;
        LineRenderer lineRenderer = obj.GetComponent<LineRenderer>();


        Color c1 = new Color(0.5f, 0.5f, 0.5f, 1f);

        lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
        lineRenderer.SetWidth(20, 20);
        lineRenderer.SetColors(c1, c1);
        lineRenderer.SetVertexCount(numSegments);

        aPosition.z = -8;
        aDestination.z = -8;
        lineRenderer.SetPosition(0, aPosition.toVec3());
        lineRenderer.SetPosition(1, aDestination.toVec3());
        lineRenderer.SetPosition(2, aDestination.toVec3());



        return obj;
    }


    public override CShape copy()
    {
        return new CCircle(mRadious, new CVector3D(mCenter.x, mCenter.y));
    }

    public override List<CVector2D> toPolygon()
    {
        var polygon = new List<CVector2D>();
        CVector2D pos;
        //get at least 8 points from the circunference
        var initialPos = mCenter.x + mRadious;
        int angle = 0;
        for (int i = 0; i < 8; i++)
        {
            angle = 360 / i;
            pos = CMath.getPointInCircleWithAngleAndCenter(mCenter, angle, (int)mRadious);
            polygon.Add(pos);
        }

        return polygon;
    }


}