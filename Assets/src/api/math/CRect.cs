
using System.Collections.Generic;
using UnityEngine;

public class CRect : CShape
{

    public double x = 0;
    public double y = 0;
    public uint height = 0;
    public uint width = 0;

    public CRect(uint aWidth, uint aHeight, double aX = 0, double aY = 0)
    {
        x = aX;
        y = aY;
        width = aWidth;
        height = aHeight;
    }

    public CRect(uint aWidth, uint aHeight, CVector3D aVector)
    {
        x = aVector.x;
        y = aVector.y;
        width = aWidth;
        height = aHeight;
    }

    public CRect()
    {
    }

    public override List<CVector2D> toPolygon()
    {
        return new List<CVector2D>() { new CVector2D(x, y), new CVector2D(x + width, y), new CVector2D(x + width, y + height), new CVector2D(x, y + height) };
    }

    public override CShape copy()
    {
        return new CRect(width, height, new CVector3D(x, y));
    }


}
