
using System;
using UnityEngine;
public class CSoundAndMusic
{

    private AudioSource mSound = new AudioSource();
    private bool mLoop = false;
    private float mVolume = 0f;
    public CSoundAndMusic(AudioClip aSound = null, bool aLoop = false)
    {
        if (aSound != null)
        {
            mSound.clip = aSound;
        }
        mLoop = aLoop;
    }

    public void playSound(AudioClip aSound, bool aLoop, float aVolume = 1f, double timeLoop = 1)
    {
        mLoop = aLoop;
        mVolume = aVolume;
        mSound.clip = aSound;
        if (mSound != null)
        {
            mSound.loop = mLoop;

            mSound.volume = mVolume;
            mSound.Play();

        }
        else
        {
            throw new Exception("SOUND PARAMETER COULD NOT BE CONVERTED TO SOUND!");
        }
    }

    public void play()
    {
        mSound.Play();
    }

    public void resume()
    {
        mSound.Play();
    }

    public void pause()
    {
        mSound.Stop();
    }
    public bool isPlaying()
    {
        return mSound.isPlaying;
    }

    public void stop()
    {
        mSound.Stop();
    }

    public void destroy()
    {
        mSound.Stop();
        mSound = null;
    }

}