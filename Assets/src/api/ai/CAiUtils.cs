﻿using System;
using System.Collections.Generic;
using System.Text;


class CAiUtils
{
    public static List<CVector2D> backtrace(CGenericTile node)
    {
        var path = new List<CVector2D>();
        path.Add(new CVector2D(node.getX(), node.getY()));
        while (node.mPathData.parent != null)
        {
            node = node.mPathData.parent;
            path.Add(new CVector2D(node.getX(), node.getY()));
        }
        path.Reverse();

        return path;
    }
}

