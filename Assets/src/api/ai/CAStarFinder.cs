﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CAStarFinder
{
    bool mAllowDiagonal = false;
    bool mDontCrossCorners = false;
    EHeuristic mHeuristic = EHeuristic.Manhattan;
    int mWeight = 1;
    EDiagonalMovement mDiagonalMovement = EDiagonalMovement.Never;

    public struct SStarFinder
    {

        public bool allowDiagonal;
        public bool dontCrossCorners;
        public EHeuristic heuristic;
        public int weight;
        public EDiagonalMovement diagonalMovement;
    }
    public CAStarFinder(SStarFinder aOptions)
    {
        mAllowDiagonal = aOptions.allowDiagonal;
        mDontCrossCorners = aOptions.dontCrossCorners;
        mHeuristic = aOptions.heuristic;
        mWeight = 1;
        mDiagonalMovement = aOptions.diagonalMovement;

        if (!aOptions.allowDiagonal)
        {
            mDiagonalMovement = EDiagonalMovement.Never;
        }
        else
        {
            if (aOptions.dontCrossCorners)
            {
                mDiagonalMovement = EDiagonalMovement.OnlyWhenNoObstacles;
            }
            else
            {
                mDiagonalMovement = EDiagonalMovement.IfAtMostOneObstacle;
            }
        }

        //When diagonal movement is allowed the manhattan heuristic is not admissible
        //It should be octile instead
        if (aOptions.diagonalMovement == EDiagonalMovement.Never)
        {
            mHeuristic = aOptions.heuristic;
        }
        else
        {
            mHeuristic = EHeuristic.Octile;
        }
    }


    /**
     * Find and return the the path.
     * @return {Array.<[number, number]>} The path, including both start and
     *     end positions.
     */
    public List<CVector2D> findPath(int startX, int startY, int endX, int endY, CVector2D aMin, CVector2D aMax)
    {

        //THE TILES ARE MARKED AS VISITED BECAUSE OF THE REFERENCE, WE HAVE TO CHANGE THIS! WE NEED A NEW SET OF TILES FOR EACH ITERATION!

        var openList = new CHeap();
        //THESE TWO CAN BE NULL, HAVE TO SET THEIR POSITION JUST IN CASE
        var startNode = CMap.inst().getTileInSectorWithPixels(startX, startY);
        var endNode = CMap.inst().getTileInSectorWithPixels(endX, endY);


        //endNode.debug(Color.blue);

        CGenericTile node;
        CGenericTile neighbor;
        List<CGenericTile> neighbors;
        double l, x, y, ng;

        // set the `g` and `f` value of the start node to be 0
        startNode.mPathData.g = 0;
        startNode.mPathData.f = 0;

        // push the start node into the open list
        openList.insert(startNode);
        startNode.mPathData.opened = true;

        // while the open list is not empty
        while (!openList.empty())
        {
            // pop the position of node which has the minimum `f` value.
            node = openList.pop();

            node.mPathData.closed = true;

            // if reached the end position, construct the path and return it
            if (node.getX() == endNode.getX() && node.getY() == endNode.getY())
            {
                var trace = CAiUtils.backtrace(node);
                CGenericTile pathTile = new CGenericTile();
                var count = CMap.inst().mTilePositions.Count;
                for (int i = 0; i < count; i++)
                {
                    CMap.inst().isWalkableAt(CMap.inst().mTilePositions[i].x, CMap.inst().mTilePositions[i].y, ref pathTile);
                    pathTile.mPathData.closed = false;
                    pathTile.mPathData.opened = false;
                    pathTile.mPathData.parent = null;
                    pathTile.mPathData.f = 0;
                    pathTile.mPathData.g = 0;
                    pathTile.mPathData.h = 0;
                }
                CMap.inst().resetPositions();
                neighbors = new List<CGenericTile>();
                openList = new CHeap(EComparison.Default);
                return trace;
            }

            // get neigbours of the current node
            neighbors = CMap.inst().getNeighborsWalkables(node, mDiagonalMovement, aMin, aMax);
            l = neighbors.Count;
            for (int i = 0; i < l; ++i)
            {
                neighbor = neighbors[i];

                if (neighbor.mPathData.closed)
                {
                    continue;
                }

                x = neighbor.getX();
                y = neighbor.getY();

                // get the distance between current node and the neighbor
                // and calculate the next g score
                ng = node.mPathData.g + ((x - node.getX() == 0 || y - node.getY() == 0) ? 1 : Math.Sqrt(2));



                // check if the neighbor has not been inspected yet, or
                // can be reached with smaller cost from the current node
                if (!neighbor.mPathData.opened || ng < neighbor.mPathData.g)
                {
                    neighbor.mPathData.g = ng;
                    int heuristicValue = 0;
                    if (mHeuristic == EHeuristic.Manhattan)
                    {
                        heuristicValue = CHeuristic.manhattan((int)Math.Abs(x - endX), (int)Math.Abs(y - endY));
                    }
                    neighbor.mPathData.h = neighbor.mPathData.h != 0 ? neighbor.mPathData.h : mWeight * heuristicValue;
                    neighbor.mPathData.f = neighbor.mPathData.g + neighbor.mPathData.h;
                    neighbor.mPathData.parent = node;


                    if (!neighbor.mPathData.opened)
                    {
                        openList.insert(neighbor);
                        neighbor.mPathData.opened = true;
                    }
                    else
                    {
                        // the neighbor can be reached with smaller cost.
                        // Since its f value has been updated, we have to
                        // update its position in the open list
                        openList.updateItem(neighbor);
                    }
                }

            } // end for each neighbor
        } // end while not open list empty


        //reset just in case
        CGenericTile xpathTile = new CGenericTile();
        var indexx = CMap.inst().mTilePositions.Count;
        for (int i = 0; i < indexx; i++)
        {
            CMap.inst().isWalkableAt(CMap.inst().mTilePositions[i].x, CMap.inst().mTilePositions[i].y, ref xpathTile);
            //xpathTile.debug(Color.grey);
            xpathTile.mPathData.closed = false;
            xpathTile.mPathData.opened = false;
            /* if (xpathTile.mPathData.parent != null)
                 xpathTile.mPathData.parent.debug(Color.grey);*/
            xpathTile.mPathData.parent = null;

            xpathTile.mPathData.f = 0;
            xpathTile.mPathData.g = 0;
            xpathTile.mPathData.h = 0;
        }
        CMap.inst().resetPositions();
        neighbors = new List<CGenericTile>();
        openList = new CHeap(EComparison.Default);
        // fail to find the path
        return new List<CVector2D>();
    }
}

