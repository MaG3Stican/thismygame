﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class CHeuristic
{

    /**
* @namespace PF.Heuristic
* @description A collection of heuristic functions.
*/
    public CHeuristic()
    {

    }
    /**
     * Manhattan distance.
     * @param {number} dx - Difference in x.
     * @param {number} dy - Difference in y.
     * @return {number} dx + dy
     */
    public static int manhattan(int aDx, int aDy)
    {
        return aDx + aDy;
    }

    /**
     * Euclidean distance.
     * @param {number} dx - Difference in x.
     * @param {number} dy - Difference in y.
     * @return {number} sqrt(dx * dx + dy * dy)
     */
    public static int euclidean(int aDx, int aDy)
    {
        return (int)Math.Sqrt(aDx * aDx + aDy * aDy);
    }

    /**
     * Octile distance.
     * @param {number} dx - Difference in x.
     * @param {number} dy - Difference in y.
     * @return {number} sqrt(dx * dx + dy * dy) for grids
     */
    public static int octile(int aDx, int aDy)
    {
        var F = Math.Sqrt(2) - 1;
        return (int)((aDx < aDy) ? F * aDx + aDy : F * aDy + aDx);
    }

    /**
     * Chebyshev distance.
     * @param {number} dx - Difference in x.
     * @param {number} dy - Difference in y.
     * @return {number} max(dx, dy)
     */
    public static int chebyshev(int aDx, int aDy)
    {
        return Math.Max(aDx, aDy);
    }

};




