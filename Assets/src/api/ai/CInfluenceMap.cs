
using System;
using System.Collections.Generic;
public class CInfluenceMap
{
    private List<List<int>> mMap = new List<List<int>>();
    private List<List<int>> mStaticMap;
    private int mWidth = 0;
    private int mHeight = 0;

    public CInfluenceMap(int aWidth, int aHeight)
    {
        mWidth = aWidth;
        mHeight = aHeight;

        mMap = new List<List<int>>();
        for (int i = 0; i < mHeight; i++)
        {
            List<int> tRow = new List<int>();
            for (int j = 0; j < mWidth; j++)
            {
                tRow.Add(0);
            }
            mMap.Add(tRow);
        }
    }

    public void setStaticMap(List<List<int>> aStaticMap)
    {
        mStaticMap = aStaticMap;
        mMap = mStaticMap;
    }

    public List<List<int>> getMap()
    {
        return mMap;
    }
    public void setMap(List<List<int>> aMap)
    {
        mMap = aMap;
    }

    public int getValue(int aX, int aY)
    {
        if (aX < 0 || aY < 0 || aX >= mWidth || aY >= mHeight)
            return -9999;
        else
            return mMap[aY][aX];
    }

    public void addValue(int aCol, int aRow, float aValue)
    {
        if (aRow >= 0 && aRow < mHeight && aCol >= 0 && aCol < mWidth)
        {
            mMap[aRow][aCol] = (int)aValue;

            int tStartX = (int)Math.Max(0, aCol - Math.Sqrt(aValue - 0.5));
            int tEndX = (int)Math.Min(mMap[0].Count, aCol + Math.Sqrt(aValue - 0.5));
            int tStartY = (int)Math.Max(0, aRow - Math.Sqrt(aValue - 0.5));
            int tEndY = (int)Math.Min(mMap.Count, aRow + Math.Sqrt(aValue - 0.5));

            for (int i = tStartY; i < tEndY; i++)
            {
                for (int j = tStartX; j < tEndX; j++)
                {
                    float d = (i - aRow) * (i - aRow) + (j - aCol) * (j - aCol);
                    int tNewValue;
                    if (d > 0)
                        tNewValue = (int)Math.Floor(aValue / (d + 0.5));
                    else
                        tNewValue = (int)aValue;
                    if (mMap[i][j] < tNewValue && mStaticMap[i][j] == 0)
                        mMap[i][j] = tNewValue;
                }
            }
        }
    }

    public void addStatic(int aCol, int aRow, int aValue)
    {
        mMap[aRow][aCol] = aValue;
    }

    public void update()
    {

    }
}

