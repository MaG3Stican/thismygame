﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class CPatrolAndFollow : CGenericBehaviour
{
    public enum EActionsType
    {
        Close,
        Ranged,
        Mixed
    }

    private enum EStates
    {
        Patrolling,
        Chasing,
        IsAtAttackLocation,
        ReachedTarget
    }

    private EStates mState;

    private EActionsType mActionType;



    private CVector3D mLastObjectSighting;          // Reference to the last global sighting of the player.
    private float mChaseTimer;                               // A timer for the chaseWaitTime.
    private float mPatrolTimer;                              // A timer for the patrolWaitTime.


    public CPatrolAndFollow(CGenericGraphic aThisIsI, CBehaviourProperties aProperties, CGenericGraphic aTarget, EActionsType aActionType = EActionsType.Ranged)
    {
        base.mTarget = aTarget;
        base.aObjectImplementingBehaviour = aThisIsI;

        mActionType = aActionType;

        mVisionRange = aProperties.aVisionRange;
        mChaseSpeed = aProperties.aChaseSpeed;
        mPatrolSpeed = aProperties.aPatrolSpeed;
        mChaseWaitTime = aProperties.aChaseWaitTime;
        mPatrolWaitTime = aProperties.aPatrolWaitTime;
        mRangeOfAttack = aProperties.aRangeOfAttack;

        var sector = CMap.inst().getSector(aThisIsI.getPos());

        CVector3D aPos = sector.getRandomPointInside(base.aObjectImplementingBehaviour.getHeight(), base.aObjectImplementingBehaviour.getWidth());

        planRoute(aPos);
    }




    public override void render(CGenericGraphic graphic)
    {
        base.render(graphic);
    }

    public override void update()
    {
        manageStates();
    }

    public void manageStates()
    {
        if (mState == EStates.Patrolling)
        {



            //if (isTargetOnRangeOfSight() && isTargetAlive())
            //{
            //    setState(EStates.Chasing);
            //    return;
            //}
            //else
            //{
            //execute patrol behaviour
            if (aObjectImplementingBehaviour.reachedDestination())
            {
                if (mPatrolWayPoints.Count > mCurrentPatrolWaypoint)
                {
                    var waypoint = mPatrolWayPoints[mCurrentPatrolWaypoint];
                    aObjectImplementingBehaviour.goToAndStop(waypoint, mPatrolSpeed);
                    mCurrentPatrolWaypoint++;
                }
                else
                {
                    mCurrentPatrolWaypoint = 0;
                    mPatrolWayPoints.Reverse();
                }
            }
            //}
        }
        if (mState == EStates.Chasing)
        {
            GoToLocationOrStop();
            return;
        }
        if (mState == EStates.ReachedTarget)
        {
            setState(EStates.IsAtAttackLocation);
            return;
        }
        if (mState == EStates.IsAtAttackLocation)
        {
            if (isTargetInsideOurBehaviourArea())
            {
                if (isTargetOnRangeOfSight() && isTargetAlive())
                {
                    aObjectImplementingBehaviour.attack();
                    return;
                }
                else if (!isTargetOnRangeOfSight() && isTargetAlive())
                {
                    aObjectImplementingBehaviour.stopAttacking();
                    setState(EStates.Chasing);
                    return;
                }
                else if (!isTargetOnRangeOfSight() && !isTargetAlive())
                {
                    setState(EStates.Patrolling);
                    return;
                }
            }
            //we reset back to normal state
            else
            {
                setState(EStates.Patrolling);
                return;
            }
        }
    }

    void GoToLocationOrStop()
    {
        //we will go out of chasing after we reach the target
        if (mActionType == EActionsType.Ranged)
        {
            if (nothingIsBlockingVision())
            {
                if (isTargetOnRangeOfAttack())
                {
                    aObjectImplementingBehaviour.stopMove();
                    setState(EStates.ReachedTarget);
                    return;
                }
                else
                {
                    aObjectImplementingBehaviour.goToAndStop(getTarget().getPos());
                }
            }
            else
            {
                aObjectImplementingBehaviour.goToAndStop(getAlternativeRoute());
            }
        }
        else if (mActionType == EActionsType.Close)
        {
            if (nothingIsBlockingVision())
            {
                if (aObjectImplementingBehaviour.reachedDestination())
                {
                    aObjectImplementingBehaviour.stopMove();
                    setState(EStates.ReachedTarget);
                    return;
                }
                else
                {
                    aObjectImplementingBehaviour.goToAndStop(getTarget().getPos());
                }
            }
            else
            {
                aObjectImplementingBehaviour.goToAndStop(getAlternativeRoute());
            }
        }
    }

    private void setState(EStates aState)
    {
        mState = aState;

        if (mState == EStates.Chasing)
        {
            mChaseTimer = 0;
            aObjectImplementingBehaviour.setVel((int)mChaseSpeed);
        }
        if (mState == EStates.Patrolling)
        {
            aObjectImplementingBehaviour.setVel((int)mPatrolSpeed);
        }
    }

    void Chasing()
    {

        //IMPORTANT - TO BE IMPLEMENTED
        //// If near the last personal sighting...
        //if (nav.remainingDistance < nav.stoppingDistance)
        //{
        //    // ... increment the timer.
        //    chaseTimer += Time.deltaTime;

        //    // If the timer exceeds the wait time...
        //    if (chaseTimer >= chaseWaitTime)
        //    {
        //        // ... reset last global sighting, the last personal sighting and the timer.
        //        lastPlayerSighting.position = lastPlayerSighting.resetPosition;
        //        enemySight.personalLastSighting = lastPlayerSighting.resetPosition;
        //        chaseTimer = 0f;
        //    }
        //}
        //else
        //    // If not near the last sighting personal sighting of the player, reset the timer.
        //    chaseTimer = 0f;
    }


    void Patrolling()
    {
        //I HAVE TO CREATE TWO VECTORS, CALCULATE THE ANGLE BETWEEN, AND CHECK IF THE VECTOR BETWEEN ME AND THE TARGET IS WITHIN THAT OTHER ANGLE

        //if ( lookAtGetAngle(target) < getWhereIAmLooking() + (getPatrolAngle() / 2)  lookAtGetAngle(target) < getWhereIAmLooking() - (getPatrolAngle() / 2)






        //    // Set an appropriate speed for the NavMeshAgent.
        //    nav.speed = patrolSpeed;

        //    // If near the next waypoint or there is no destination...
        //    if (nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)
        //    {
        //        // ... increment the timer.
        //        patrolTimer += Time.deltaTime;

        //        // If the timer exceeds the wait time...
        //        if (patrolTimer >= patrolWaitTime)
        //        {
        //            // ... increment the wayPointIndex.
        //            if (wayPointIndex == patrolWayPoints.Length - 1)
        //                wayPointIndex = 0;
        //            else
        //                wayPointIndex++;

        //            // Reset the timer.
        //            patrolTimer = 0;
        //        }
        //    }
        //    else
        //        // If not near a destination, reset the timer.
        //        patrolTimer = 0;

        //    // Set the destination to the patrolWayPoint.
        //    nav.destination = patrolWayPoints[wayPointIndex].position;
    }
}

