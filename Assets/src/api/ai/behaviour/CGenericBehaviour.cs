
using System;
using System.Collections.Generic;

public class CGenericBehaviour
{



    public struct CBehaviourProperties
    {
        public uint aVisionRange;
        public float aPatrolSpeed;
        public float aChaseSpeed;
        public float aChaseWaitTime;
        public float aPatrolWaitTime;
        public uint aRangeOfAttack;
        public CRect aBehaviourArea;
    }

    internal List<CVector2D> mPatrolWayPoints = new List<CVector2D>();                 // An array of transforms for the patrol route.
    internal int mCurrentPatrolWaypoint = 0;

    internal uint mVisionRange;
    internal float mPatrolSpeed = 0f;                          // speed when patrolling.
    internal float mChaseSpeed = 0f;                           // speed when chasing.
    internal float mChaseWaitTime = 0f;                        // The amount of time to wait when the last sighting is reached.
    internal float mPatrolWaitTime = 0f;                       // The amount of time to wait when the patrol way point is reached.
    internal uint mRangeOfAttack = 0;
    internal CRect mBehaviourArea = new CRect();

    int mWalkingPathIndex = 0;
    internal CGameObject mTarget;

    internal CGenericGraphic aObjectImplementingBehaviour;
    private CGameObject mObjectToTrack;
    private CVector3D mAlternativeRoute;


    public CGenericBehaviour()
    {

    }

    public void setTarget(CGameObject aTarget)
    {
        mTarget = aTarget;
    }
    public CGameObject getTarget()
    {
        return mTarget;
    }

    public virtual void update()
    {

    }

    public virtual void render(CGenericGraphic graphic)
    {
    }

    public void destroy()
    {
    }

    public bool isTargetOnRangeOfSight()
    {
        CVector3D sightingDistance = aObjectImplementingBehaviour.getPos() - getTarget().getPos();
        //squaring the magnitude is faster
        if (Math.Pow(mVisionRange, 2) < sightingDistance.sqrMagnitude())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool isTargetInsideOurBehaviourArea()
    {
        if (CMath.shapeAndCircleCollision(aObjectImplementingBehaviour.getShape(), mBehaviourArea))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    public bool isTargetAlive()
    {
        return !mTarget.isDead();
    }

    public bool nothingIsBlockingVision()
    {
        var result = CVars.inst().getMap().isWallOrBuildingBlocking(aObjectImplementingBehaviour.getPos(), mTarget.getPos());
        if (result.NothingBlocks)
        {
            return true;
        }
        else
        {

            return false;
        }
    }


    public CVector3D getAlternativeRoute()
    {
        return mAlternativeRoute;
    }


    internal bool isTargetOnRangeOfAttack()
    {
        CVector3D sightingDeltaPos = aObjectImplementingBehaviour.getPos() - getTarget().getPos();
        return sightingDeltaPos.sqrMagnitude() > Math.Pow(mRangeOfAttack, 2);
    }


    internal void planRoute(CVector3D aTarget)
    {
        CAStarFinder.SStarFinder finder = new CAStarFinder.SStarFinder();
        finder.allowDiagonal = true;
        finder.diagonalMovement = EDiagonalMovement.Never;
        finder.dontCrossCorners = true;
        finder.heuristic = EHeuristic.Manhattan;
        finder.weight = 1;
        CAStarFinder aFinder = new CAStarFinder(finder);

        //HAVE TO MOVE UP
        mPatrolWayPoints = aFinder.findPath((int)(aObjectImplementingBehaviour.getPos().x),
                                            (int)(aObjectImplementingBehaviour.getPos().y),
                                            (int)aTarget.x, (int)aTarget.y,
                                            aObjectImplementingBehaviour.getMinBoounds(), aObjectImplementingBehaviour.getMaxBounds());
    }
}
























