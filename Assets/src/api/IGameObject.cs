
public interface IGameObject
{

    void update();
    void render();
    void destroy();

}