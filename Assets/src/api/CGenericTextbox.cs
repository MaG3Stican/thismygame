﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;



public enum EWordState
{
    None = 0,
    FinishedProcessing = 1,
    ReadyForProcessing = 2,
    MutatingColors = 3,
    Processing = 4,
    Idle = 5,
    FinishedProcessingIdleTime
}

public class CGenericTextbox : CGenericGraphic
{

    //most important property
    protected List<CGenericLetter> mLetters = new List<CGenericLetter>();
    protected CVector3D mDefaultPos = new CVector3D(600, 150);
    protected int mLetterLifeTime = 16;
    protected int mCurrentLetterLifeTime = 0;
    protected TextMeshProUGUI text;
    protected bool mFinishedWithCurrentText = false;
    protected EWordState mState = EWordState.None;
    protected bool mAllLettersFinishedProcessing = false;
    protected int mFinishedProcessingIdleTime = 0;

    protected int currentCharactersProcessed = 0;


    public CGenericTextbox(CLayer aLayer, int aLetterLifeTime = 0)
    {
        mCollides = false;
        setLayer(aLayer);
        if (aLetterLifeTime > 0)
        {
            mLetterLifeTime = aLetterLifeTime;
        }
    }



    //u can only init after setting the text property
    protected void init(string aText = null)
    {
        if (text != null)
        {
            if (aText != null)
            {
                text.text = aText;
            }
            text.ForceMeshUpdate();
            var characterCount = text.textInfo.characterCount;

            for (int i = 0; i < characterCount; i++)
            {
                mCurrentLetterLifeTime += mLetterLifeTime;

                var charInfo = text.textInfo.characterInfo[i];
                if (!charInfo.isVisible)
                    continue;
                var vertexIndex = charInfo.vertexIndex;

                var letter = new CGenericLetter(getLayer(), mCurrentLetterLifeTime, i, ELetterDyingReason.None, new CVector2D(charInfo.bottomRight.x - charInfo.bottomLeft.x, charInfo.bottomRight.y - charInfo.topRight.y));

                letter.vertices.Add(vertexIndex + 0);
                letter.vertices.Add(vertexIndex + 1);
                letter.vertices.Add(vertexIndex + 2);
                letter.vertices.Add(vertexIndex + 3);
                //set velocity so that the letter starts moving
                letter.setPos(mDefaultPos);
                letter.setVelY(0.1);
                mLetters.Add(letter);
            }
        }
        else
        {
            throw new Exception("You are trying to init the letters system but you have not set the text property yet");
        }
    }


    public override void deactivate()
    {
        text.gameObject.SetActive(false);
        base.deactivate();
    }
    public override void activate()
    {
        text.gameObject.SetActive(true);
        base.activate();
    }


    protected void makeLettersDrop()
    {
        //these variables are outside to improve performance
        var uiRenderer = text.canvasRenderer;
        var index = 0;
        List<int> vertices;
        CGenericLetter letter;
        mFinishedWithCurrentText = true;
        currentCharactersProcessed = 0;
        while (index < mLetters.Count)
        {
            letter = mLetters[index];
            vertices = letter.vertices;

            if (letter.getState() == ELetterState.ReadyForProcessing)
            {
                for (var i = 0; vertices.Count > i; i++)
                {
                    text.textInfo.meshInfo.uiVertices[vertices[i]].position -= letter.getVel().toVec3();
                }
                letter.setVelY(letter.getVelY() + 0.1);
                mFinishedWithCurrentText = false;
                currentCharactersProcessed++;
            }

            mAllLettersFinishedProcessing = letter.finishedProcessing();

            letter.update();
            mLetters[index] = letter;
            index++;
        }
        uiRenderer.SetVertices(text.textInfo.meshInfo.uiVertices, text.textInfo.meshInfo.uiVertices.Length);
    }

    protected void setLettersFinishingReason(ELetterDyingReason aDyingReason)
    {
        var index = 0;
        while (index < mLetters.Count)
        {
            mLetters[index].mDyingReason = aDyingReason;
            index++;
        }
    }

    protected void setLettersState(ELetterState aState)
    {
        var index = 0;
        while (index < mLetters.Count)
        {
            mLetters[index].setState(aState);
            index++;
        }
    }


    protected virtual void setState(EWordState mCurrentState)
    {
        mState = mCurrentState;
        setTimeState(0);
    }

    protected virtual void manageStates()
    {

    }


    internal void makeLettersTransparent()
    {
        //these variables are outside to improve performance
        var uiRenderer = text.canvasRenderer;
        var index = 0;
        List<int> vertices;
        CGenericLetter letter;
        while (index < mLetters.Count)
        {
            letter = mLetters[index];
            //checking so that we dont execute the method more than once
            if (letter.getState() == ELetterState.None)
            {
                vertices = letter.vertices;
                //hide letters 
                for (var i = 0; letter.vertices.Count > i; i++)
                {
                    text.textInfo.meshInfo.uiVertices[vertices[i]].color.a = 0;
                }
            }
            mLetters[index] = letter;
            letter.update();
            index++;
        }
        uiRenderer.SetVertices(text.textInfo.meshInfo.uiVertices, text.textInfo.meshInfo.uiVertices.Length);
    }

    internal void makeLettersAppearOneByOne()
    {
        //these variables are outside to improve performance
        var uiRenderer = text.canvasRenderer;
        var index = 0;
        List<int> vertices;
        CGenericLetter letter;
        mFinishedWithCurrentText = true;
        while (index < mLetters.Count)
        {
            letter = mLetters[index];
            if (letter.getState() == ELetterState.ReadyForProcessing)
            {
                vertices = letter.vertices;
                //show letters 
                for (var i = 0; letter.vertices.Count > i; i++)
                {
                    if (text.textInfo.meshInfo.uiVertices[vertices[i]].color.a < 99)
                    {
                        text.textInfo.meshInfo.uiVertices[vertices[i]].color.a += 1;
                        mFinishedWithCurrentText = false;
                    }
                }
            }
            else if (letter.getState() == ELetterState.WaitingForProcessingTime)
            {
                mFinishedWithCurrentText = false;
            }
            mLetters[index] = letter;
            letter.update();
            index++;
        }
        uiRenderer.SetVertices(text.textInfo.meshInfo.uiVertices, text.textInfo.meshInfo.uiVertices.Length);
    }


    public EWordState getState()
    {
        return mState;
    }

    public int mLayDownFor { get; set; }
}
