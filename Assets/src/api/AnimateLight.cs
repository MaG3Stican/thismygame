using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class AnimateLight : MonoBehaviour
{

		Light aLight = new Light ();
		Vector3 initialPosition = new Vector3 ();
		int capturedFrame = 0;
		bool changeLightUp = false;
		bool changeLightDown = false;
		// Use this for initialization
		void Start ()
		{
				capturedFrame = Time.frameCount;
				aLight = GetComponent<Light> ();	
				initialPosition = aLight.transform.position;
				changeLightDown = true;
		}
	
		// Update is called once per frame
		void Update ()
		{
	    
				int i = Time.frameCount - capturedFrame;
				if (i > 3) {
						aLight.transform.position = new Vector3 (aLight.transform.position.x - 0.1f, aLight.transform.position.y + 0.1f, aLight.transform.position.z);
						
						if (aLight.intensity <= 7.75f && changeLightUp) {
								aLight.intensity += 0.25f;
								//IF LIGHT HITS 8 THEN TURN IT DOWN
								if (aLight.intensity == 8) {
										changeLightDown = true;
										changeLightUp = false;
								}

						}
						if (aLight.intensity != 3 && changeLightDown) {
								aLight.intensity -= 0.25f;
								//IF LIGHT HITS 4 THEN TURN IT UP
								if (aLight.intensity <= 4) {
										changeLightDown = false;
										changeLightUp = true;
								}
						}
						capturedFrame = Time.frameCount;
						return;
				}
				if (i > 1) {
						aLight.transform.position = initialPosition;
						
				}
				


		}
}
