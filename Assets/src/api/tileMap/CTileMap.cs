﻿
using System.Collections.Generic;
public class CTileMap
{

    private CTile mEmptyTile;


    private int mMapWidth;
    private int mMapHeight;
    private int mTileWidth;
    private int mTileHeight;

    //private List<CMapLayer> mLayers;
    private int mMainLayer = 0;
    private CTileSet mTileSet;

    private Dictionary<int, int> mAtributes = new Dictionary<int, int>();

    public CTileMap()
    {
        mEmptyTile = new CTile(0, 0, 0, 0, false, true);
        //mLayers = new List<CMapLayer>();
    }

    public CTile getEmptyTile()
    {
        return mEmptyTile;
    }

    public void loadTileMap(Dictionary<string, string> aMap, List<string> aAtributes)
    {
        //    mTileMap = new CXMLMapLoader(aMap);
        //    mTileWidth = mTileMap.getTileWidth();
        //    mTileHeight = mTileMap.getTileHeight();
        //    mMapWidth = mTileMap.getMapWidth();
        //    mMapHeight = mTileMap.getMapHeight();

        //    for (var i:uint; i < aAtributes.length; i++)
        //    {
        //        mAtributes[aAtributes[i]] = mTileMap.getAttribute(aAtributes[i]);
        //        mEmptyTile.setDynamicProperty(aAtributes[i], false);
        //    }
        //    mEmptyTile.setDynamicProperty(CTerrains.WALKABLE, true);
    }

    public void loadMap(string aLayerName)
    {
        //    var tMapLayer:CMapLayer = new CMapLayer(aLayerName);
        //    var tTilePositions:Vector.<Vector.<int>> = mTileMap.getMapLayer(aLayerName);
        //    var tMap:Vector.<Vector.<CTile>> = new Vector.<Vector.<CTile>>();
        //    var tLayer:CLayer = CLayer.addLayer(aLayerName);

        //    tMapLayer.setMap(tTilePositions);
        //    tMapLayer.setLayer(tLayer);

        //    for (var i:uint = 0; i < tTilePositions.length; i++)
        //    {
        //        var tRow:Vector.<CTile> = new Vector.<CTile>();
        //        for (var j:uint = 0; j < tTilePositions[i].length; j++)
        //        {
        //            if (tTilePositions[i][j] != 0)
        //            {
        //                var tTile:CTile = new CTile(mTileWidth * j, mTileHeight * i, mTileWidth, mTileHeight, tLayer);
        //                tTile.setTileIndex(tTilePositions[i][j]);
        //                for (var k:String in mAtributes)
        //                {
        //                    var tTerrains:Array = mAtributes[k];
        //                    var tTerrainName:String = k;
        //                    tTile.setDynamicProperty(tTerrainName, tTerrains[tTilePositions[i][j] - 1]);
        //                }
        //                tRow.push(tTile);
        //            }
        //            else
        //                tRow.push(mEmptyTile);
        //        }
        //        tMap[i] = tRow;
        //    }

        //    tMapLayer.setTileMap(tMap);
        //    mLayers.push(tMapLayer);
    }


    //public void convertRawMapToTileMap(aName:String, aArray:Vector.<Vector.<int>>):
    //{
    //    var tMapLayer:CMapLayer = new CMapLayer(aName);
    //    var tMap:Vector.<Vector.<CTile>> = new Vector.<Vector.<CTile>>();
    //    var tLayer:CLayer = CLayer.addLayer(aName);
    //    tMapLayer.setMap(aArray);
    //    tMapLayer.setLayer(tLayer);

    //    for (var i:uint = 0; i < aArray.length; i++)
    //    {
    //        var tRow:Vector.<CTile> = new Vector.<CTile>();
    //        for (var j:uint = 0; j < aArray[i].length; j++)
    //        {
    //            if (aArray[i][j] != 0)
    //            {
    //                var tTile:CTile = new CTile(mTileWidth * j, mTileHeight * i, mTileWidth, mTileHeight, tLayer);
    //                tTile.setTileIndex(aArray[i][j]);
    //                //tTile.setDynamicProperty(CTerrains.WALKABLE, mAtributes[CTerrains.WALKABLE][aArray[i][j] - 1]);
    //                //tTile.setDynamicProperty(CTerrains.PLATFORM, mAtributes[CTerrains.PLATFORM][aArray[i][j] - 1]);
    //                //tTile.setDynamicProperty(CTerrains.ENEMY_PATH, mAtributes[CTerrains.ENEMY_PATH][aArray[i][j] - 1]);
    //                tRow.push(tTile);
    //            }
    //            else
    //                tRow.push(mEmptyTile);
    //        }
    //        tMap.push(tRow);
    //    }
    //    tMapLayer.setTileMap(tMap);
    //    mLayers.push(tMapLayer);
    //}

    public object getLayerByName(string aName)
    {
        //int i = 0;
        //while (i < mLayers.Count)
        //{
        //    if (mLayers[i].getName() == aName)
        //        return mLayers[i];
        //    i++;
        //}
        return null;
    }

    public void setMainLayer(string aLayerName)
    {
        int i = 0;
        //while (i < mLayers.Count)
        //{
            //    if (mLayers[i].getName() == aLayerName)
            //    {
            //        mMainLayer = i;
            //        return;
            //    }
            //    i++;
        //}
    }

    public CTile getTile(uint x, uint y)
    {
        return new CTile();
        //return mLayers[mMainLayer].getTile(x, y);
    }

    public void flushMaps()
    {
        //for (uint k = 0; k < mLayers.Count; k++)
        //{
        //    if (mLayers[k])
        //    {
        //        mLayers[k] = null;
        //    }
        //}
    }

    public void setTileSet(CTileSet aTileSet)
    {
        mTileSet = aTileSet;
    }
    public CTileSet getTileSet()
    {
        return mTileSet;
    }

    //public function getLoader():CXMLMapLoader
    //{
    //    return mTileMap;
    //}

    public void setMapWidth(int aMapWidth)
    {
        mMapWidth = aMapWidth;
    }
    public int getMapWidth()
    {
        return mMapWidth;
    }

    public void setMapHeight(int aMapHeight)
    {
        mMapHeight = aMapHeight;
    }
    public int getMapHeight()
    {
        return mMapHeight;
    }

    public void setTileWidth(int aTileWidth)
    {
        mTileWidth = aTileWidth;
    }
    public int getTileWidth()
    {
        return mTileWidth;
    }

    public void setTileHeight(int aTileHeight)
    {
        mTileHeight = aTileHeight;
    }
    public int getTileHeight()
    {
        return mTileHeight;
    }

    public int getWorldWidth()
    {
        return mMapWidth * mTileWidth;
    }

    public int getWorldHeight()
    {
        return mMapHeight * mTileHeight;
    }

    public void update()
    {
        //for (uint k = 0; k < mLayers.Count; k++)
        //{
        //    mLayers[k].update();
        //}
    }

    public void render()
    {
        //for (var k:uint = 0; k < mLayers.length; k++)
        //{
        //    mLayers[k].render();
        //}
    }

    public void destroy()
    {
        //for (uint k = 0; k < mLayers.Count; k++)
        //{
        //    mLayers[k].destroy();
        //    mLayers[k] = null;
        //}
        //mLayers = null;

        mEmptyTile.destroy();
        mEmptyTile = null;
    }
}