﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class CSector
{
    public List<CGenericTile> mSector;
    private CRect mRect;
    //these are behaviours enemies can take in this section of the map
    private List<CGenericBehaviour> mEnemyBehaviours;
    private string mName;
    private int mIndex;
    private int offset = 20;

    public CLayer mLayer;
    public CSector(List<CGenericTile> aSector, uint aWidth, uint aHeight, CVector2D aLeftTopPos, string aName, int aIndex, CLayer aLayer)
    {
        mLayer = aLayer;
        mSector = aSector;
        mRect = new CRect(aWidth, aHeight, aLeftTopPos.x, aLeftTopPos.y);
        mEnemyBehaviours = new List<CGenericBehaviour>();
        mName = aName;
        mIndex = aIndex;
    }
    //this constructor is only for placeholding
    public CSector(string aName, int aIndex)
    {
        mName = aName;
        mIndex = aIndex;
    }

    public CRect getRect()
    {
        return mRect;
    }

    public void addBehaviour(CGenericBehaviour aBehaviour)
    {
        mEnemyBehaviours.Add(aBehaviour);
    }

    public CGenericBehaviour getSomeBehaviour()
    {
        var length = mEnemyBehaviours.Count;
        var behaviourIndex = CMath.randInt(0, length);
        return mEnemyBehaviours[behaviourIndex];
    }

    public string getName()
    {
        return mName;
    }
    public int getIndex()
    {
        return mIndex;
    }


    public CVector3D getRandomPointInside(uint width, uint height)
    {
        if (CMap.inst().mMapTiles.Count == 0)
        {
            CMap.inst().mergeSections();
        }
        return CMap.inst().getTileInSectorWithPixels(
                                    (double)CMath.randInt((int)mRect.x, ((int)mRect.width + (int)mRect.x) - width),
                                    (double)CMath.randInt((int)mRect.y, ((int)mRect.height + (int)mRect.y) - height)).getPos().copy();
    }

}