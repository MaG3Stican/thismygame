﻿
using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class CGenericTile : CGenericGraphic
{
    public static int TileSize = 0;
    public const int NONE = -1;
    public const int GROW_X = 0;
    public const int GROW_Y = 1;
    public static int mBhevaiour = NONE;
    public static int mLastX = 0;
    public static int mLastY = -TileSize;
    private static bool firstTime = true;

    private PolygonCollider2D mPolygonCollider;

    public struct SPathFindingData
    {
        //A* data 
        /*G denotes the distance from the starting point to the current point.
          H denotes the *estimated* distance from current point to the target point.
          F denotes the sum of G and H.*/
        public double g, f, h;
        public bool opened, closed;
        public CGenericTile parent;

    }

    public SPathFindingData mPathData;

    internal Dictionary<string, string> mDynamicProperties = new Dictionary<string, string>();

    private uint mTileIndex;
    private bool mPlatform;

    private bool mIsUseful = false;
    private bool mUsefulCheckDone = false;

    private CLight mLight = null;

    public CGenericTile()
    {

    }

    public CGenericTile(CLayer layer, GameObject aGameObject)
    {
        setLayer(layer);

        setShape(EShapes.RECT);


        if (mBhevaiour != NONE)
        {
            //this is for auto generated maps
            if (mBhevaiour == GROW_X)
            {
                mLastX += TileSize;
            }
            else
            {
                mBhevaiour = GROW_X;
                mLastX = 0;
                mLastY += TileSize;
            }
            setX(mLastX);
            setY(mLastY);

            if (firstTime == true)
            {
                mLastX = 0;
                mLastY = 0;
                setX(mLastX);
                setY(mLastY);
                firstTime = false;
            }
        }
        else
        {
            setGraphic(aGameObject);
            //set position here
            CVector3D pos = new CVector3D();
            pos.x = aGameObject.transform.position.x * -1;
            pos.y = aGameObject.transform.position.y * -1;
            pos.z = getZ();

            setPos(pos);
        }

        setWidth(CGameConstants.TILE_WIDTH);
        setHeight(CGameConstants.TILE_HEIGHT);

        //THIS IS A FIX SO THAT TILES ARE IN FRONT OF EVERYTHING ELSE
        setFeet(new CRect(0, 0, 0, CGameConstants.TILE_HEIGHT));
    }


    override public void update()
    {
        if (mUsefulCheckDone == false)
        {
            mIsUseful = isLightSource() || !isWalkable();
            if (mIsUseful)
            {
                mPolygonCollider = getGraphic().AddComponent<PolygonCollider2D>();
                getGraphic().name += " *HAS POLYGON COLLIDER*";
            }
            mUsefulCheckDone = true;

        }
        if (mIsUseful)
        {
            if (mLight == null && isLightSource())
            {
                mLight = new CLight(getPos(), getLightRange());
            }
            if (mLight != null)
            {
                mLight.update();
            }


            base.update();
        }


    }

    public override void deactivate()
    {
        if (mLight != null)
            mLight.deactivate();


        base.deactivate();
    }

    public override void activate()
    {
        if (mLight != null)
            mLight.activate();


        base.activate();
    }

    override public void render()
    {
        if (mLight != null)
        {
            mLight.render();
        }

        base.render();
    }

    public bool amIUseful()
    {
        if (isLightSource())
            return true;
        if (!isWalkable())
            return true;
        if (canOverlap())
            return true;

        return false;
    }



    public void debug(Color aColor)
    {
        if (aColor == null)
        {
            aColor = Color.green;
        }
        var copy = getPos().copy();
        copy.y = copy.y * -1;
        var rect = CCircle.DrawShape(true, (int)CGameConstants.TILE_HEIGHT, EShapes.RECT, copy, aColor, CLayer.getLayer(ELayer.Hud));
        Vector3 c = copy.toVec3();
        rect.transform.position = c;
    }


    public uint getTileIndex()
    {
        return mTileIndex;
    }

    public void setTileIndex(uint aTileIndex)
    {
        mTileIndex = aTileIndex;
    }

    public bool isLightSource()
    {
        if (getDynamicProperty("mLightSource") == "true")
            return true;
        else if (getDynamicProperty("mLightSource") == "false")
            return false;
        else
            return false;
    }
    public uint getLightRange()
    {
        if (getDynamicProperty("mLightSourceRange") != "")
            return Convert.ToUInt32(getDynamicProperty("mLightSourceRange"));
        else if (getDynamicProperty("mLightSourceRange") == "false")
            return 1;
        else
            return 1;
    }



    public bool isWalkable()
    {
        if (getDynamicProperty("mWalkable") == "")
            return true;
        else if (getDynamicProperty("mWalkable") == "false")
            return false;
        else
            return true;
    }

    public bool canOverlap()
    {
        if (getDynamicProperty("mOverlaps") == "true")
            return true;
        else if (getDynamicProperty("mOverlaps") == "false")
            return false;
        else
            return false;
    }


    public void setPlatform(bool aPlatform)
    {
        mPlatform = aPlatform;
    }

    public bool isPlatform()
    {
        return mPlatform;
    }



    public bool isFloor()
    {
        Debug.Break();
        return mDynamicProperties[ETerrains.WALKABLE.ToString()] != null || mDynamicProperties[(string)ETerrains.PLATFORM.ToString()] != null;
    }

    public void setDynamicProperty(string index, string value)
    {
        mDynamicProperties[index] = value;
    }

    public string getDynamicProperty(string index)
    {
        if (mDynamicProperties.ContainsKey(index))
            return mDynamicProperties[index];
        else
        {
            return "";
        }
    }


    internal bool isWalkable(CVector2D aMin, CVector2D aMax)
    {
        if (getPos().x <= aMin.x && getPos().x >= aMax.x)
            return false;

        if (getPos().y <= aMin.y && getPos().y >= aMax.y)
            return false;

        if (getDynamicProperty("mWalkable") == "")
            return true;
        else if (getDynamicProperty("mWalkable") == "false")
            return false;
        else
            return true;
    }

    public List<CGenericTile> getUp(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();
        var yy = getY() - CGameConstants.TILE_HEIGHT;
        var xx = getX();
        // ↑
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }
    public List<CGenericTile> getRight(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();
        var yy = getY();
        var xx = getX() + CGameConstants.TILE_WIDTH;
        // →
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }

    public List<CGenericTile> getDown(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();

        var yy = getY() + CGameConstants.TILE_HEIGHT;
        var xx = getX();
        // ↓
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }
    public List<CGenericTile> getLeft(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();

        var yy = getY();
        var xx = getX() - CGameConstants.TILE_WIDTH;
        // ←
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }

    public List<CGenericTile> getUpLeft(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();

        var yy = getY() - CGameConstants.TILE_HEIGHT;
        var xx = getX() - CGameConstants.TILE_WIDTH;
        // ↖
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }
    public List<CGenericTile> getUpRight(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();

        var yy = getY() - CGameConstants.TILE_HEIGHT;
        var xx = getX() + CGameConstants.TILE_WIDTH;
        // ↗
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }
    public List<CGenericTile> getDownRight(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();

        var yy = getY() + CGameConstants.TILE_HEIGHT;
        var xx = getX() + CGameConstants.TILE_WIDTH;
        // ↘
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }
    public List<CGenericTile> getDownLeft(CVector2D aPivot = null)
    {
        if (aPivot == null)
            aPivot = new CVector2D();

        var yy = getY() + CGameConstants.TILE_HEIGHT;
        var xx = getX() - CGameConstants.TILE_WIDTH;
        // ↙
        return CMap.inst().getTileGlobalWithPixels(xx + aPivot.x, yy + aPivot.y);
    }



    //HAVE TO TEST THIS WORKS
    internal CPolygon getCollisionableShape()
    {
        CPolygon pol = new CPolygon();
        var path = mPolygonCollider.GetPath(0);
        var pos = getGraphic().transform.position;
        path.ToList().ForEach(c => pol.mPoints.Add(new CVector2D(c.x + pos.x, c.y + pos.y)));
        return pol;
    }
}