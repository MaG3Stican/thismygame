﻿
using System;
using System.Collections.Generic;
using UnityEngine;


public class CTile : CGenericTile
{
    private uint mTileIndex;
    private bool mEmpty;
    private bool mVisited = false;
    private bool mPlatform;
    private bool mHazard;
    private bool mDebug;
    //private CRectPrefab debugRectCRect = new CRectPrefab(CGameConstants.TILE_WIDTH, CGameConstants.TILE_HEIGHT, Color.yellow);

    public CTile(CLayer aLayer, GameObject aGraphic)
        : base (aLayer,aGraphic)
    {
        CGraphicsManager.inst().addGraphic(this, EGraphics.Tiles);
    }


    public CTile(uint aX = 0, uint aY = 0, uint aWidth = 0, uint aHeight = 0, bool aLayer = false, bool aEmpty = false)
    {
        setShape(EShapes.RECT);
        setWidth(aWidth);
        setHeight(aHeight);

        mEmpty = aEmpty;
        if (!mEmpty)
        {
            if (aLayer == null)
            {
                setWidth(aWidth);
            }
            mTileIndex = 0;
            activate();
        }

        setWidth(aWidth);
        setHeight(aHeight);

        setPosXYZ(aX, aY);

        render();
    }



    public bool isHazard()
    {
        return mHazard;
    }

    public bool isEmpty()
    {
        return mEmpty;
    }


    override public void destroy()
    {
        base.destroy();
    }
    override public void render()
    {
        base.render();
    }


    public void setDebug(bool aDebug)
    {
        mDebug = aDebug;
    }

    public void setVisited(bool aVisited)
    {
        mVisited = aVisited;
    }

    public bool visited()
    {
        return mVisited;
    }

}