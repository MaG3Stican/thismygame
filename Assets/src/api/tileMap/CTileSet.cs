
using System.Collections.Generic;
using UnityEngine;
public class CTileSet
{
    // Constants
    private const uint MOVIECLIP = 0;
    private uint mType;
    private List<Texture2D> mTiles;

    public CTileSet()
    {

    }

    public void setTileSetMC(Texture2D aTileSet)
    {
        mTiles.Add(aTileSet);
        mType = MOVIECLIP;
    }
    public Texture2D getTileByID(int aID)
    {
        if (mType == MOVIECLIP)
        {
            return mTiles[aID];
        }
        else
            return null;
    }
}
