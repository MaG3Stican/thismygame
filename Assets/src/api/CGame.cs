
using System;
using UnityEngine;


public class CGame
{
    //DIFFICULTY
    public const uint EASY = 0;
    public const uint MEDIUM = 1;
    public const uint HARD = 2;
    private int mGameTime = 0;
    static private CGame mInstance;

    private CGameState mState;
    private static uint DIFFICULTY;
    private uint prevtime = 0;
    private int mFPS;
    private CCamera mCamera;
    private float timeLeft;
    private float time;
    private float fps;

    private CHud mHud;


    // FPS Counter.
    private string mFPStxt = "";
    private int count;

    public CGame()
    {
        if (mInstance != null)
        {
            throw new Exception("Error in api.CGame(). You are not allowed to instantiate it more than once.");
        }

        mInstance = this;
        init();
    }

    public void init()
    {
    }

    static public CGame inst()
    {
        return mInstance;
    }

    private void loop(Event e)
    {
        count++;
        //timeLeft -= Time.deltaTime;
        //time = getTimer();
        //fps=1000 / (time - prevtime);
        //showfps.text="FPS " + fps;
        //prevtime=getTimer();
    }

    private void showFPS()
    {
        //mFPStxt.text = "" + count;
        count = -1;
    }

    public void update()
    {
        mState.update();
    }

    private void enterFrameHandler(Event eevvent)
    {
        try
        {
            update();
            render();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    public void render()
    {
        mState.render();
    }

    public void destroy()
    {
        mState.destroy();
        mState = null;
        mInstance = null;
    }

    public void setState(CGameState aState)
    {
        Console.WriteLine("executed setstate");
        if (mState != null)
        {
            mState.destroy();
            mState = null;
            //CHelper.removeAllChildrenDisplayObject(mContainer);
        }

        mState = aState;
        mState.init();
    }


    public CGameState getState()
    {
        return mState;
    }


    public int getScreenWidth()
    {
        return Screen.width;
    }

    public int getStageHeight()
    {
        return Screen.height;
    }


    public void setDifficulty(uint pDifficulty)
    {
        DIFFICULTY = pDifficulty;
    }

    public uint getDifficulty()
    {
        return DIFFICULTY;
    }

    public void setFPS(int aFPS)
    {
        mFPS = aFPS;
    }

    public int getFPS()
    {
        return mFPS;
    }

    public void setGameTime(int aGameTime)
    {
        mGameTime = aGameTime;
    }
    public int getGameTime()
    {
        return mGameTime;
    }

    public void setHud(CHud aHud)
    {
        Console.WriteLine("executed sethud");
        mHud = aHud;
    }

    public CHud getHud()
    {
        return mHud;
    }
}
