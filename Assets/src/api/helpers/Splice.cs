﻿
using System.Collections.Generic;
using System.Linq;
public static class SpliceExtension
{
    public static List<T> Splice<T>(this List<T> list, int offset, int count)
    {
        return list.Skip(offset).Take(count).ToList();
    }
    public static IEnumerable<T> Splice<T>(this IEnumerable<T> list, int offset, int count)
    {
        return list.Skip(offset).Take(count);
    }
}