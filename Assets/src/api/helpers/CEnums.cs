﻿public enum EShapes
{
    NONE = -1,
    RECT = 0,
    CIRCLE = 1,
    TRIANGLE = 2,
    POLYGON = 3
}

public enum EShapePerfection
{
    LOW = 0,
    NORMAL = 1,
    HIGH = 2
}

public enum ETileCondition
{
    None = -1,
    WalkableOnTop = 0
}


public enum EComparison
{
    Default = 0
}

public enum EHeuristic
{
    Manhattan = 0,
    Octile = 1
}

public enum EDiagonalMovement
{
    Never = 0,
    OnlyWhenNoObstacles = 1,
    IfAtMostOneObstacle = 2,
    Always = 3
}


public enum EDirection
{
    NONE = -1,
    UP = 0,
    DOWN = 1,
    LEFT = 2,
    RIGHT = 3,


}


public enum EBoundBehaviour
{
    NONE = 0,// Ignores borders
    WRAP = 1, // Goes from one side to the opposite
    BOUNCE = 2, // bounces
    STOP = 3, // stays in place
    DIE = 4 // destroys the object
}


public static class ETerrainLayers
{
    public const string EnvUp = "environment_upper";
    public const string Above = "above";
    public const string Paths = "paths";
    public const string Objects = "objects";
    public const string Accents = "accents";
    public const string Floor = "floor";
}


public enum EMovement
{
    NONE = 0,
    X = 1,
    Y = 2,
    XY = 3

}

public enum EAnimationSpeed
{
    FAST_ANIMATION = 0,
    NORMAL_ANIMATION = 1,
    SLOW_ANIMATION = 2,
    VERY_SLOW_ANIMATION = 3
}



