﻿using UnityEngine;
using System.Collections;

public class ZoomInZoomOut : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	int zoom = 20;
	int normal = 60;
	float smooth = 5f;
	bool isZoomed = false;
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown ("z")) {
			isZoomed = !isZoomed; 
		}
			
		if (isZoomed == true) {
			camera.fieldOfView = Mathf.Lerp (camera.fieldOfView, zoom, Time.deltaTime * smooth);
		} else {
			camera.fieldOfView = Mathf.Lerp (camera.fieldOfView, normal, Time.deltaTime * smooth);
		}

	}
}
