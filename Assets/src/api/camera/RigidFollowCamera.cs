using UnityEngine;
using System.Collections;

public class RigidFollowCamera : MonoBehaviour {
	public GameObject target;
	
	public float cameraDistance = 20f;
	public float maxCameraDistance = 30f;
	public float minCameraDistance = 10f;
	public float zoomSensibility = 3f;
	
	private Vector3 cameraDirection;
	
	private Vector3 targetPosition {
		get{
			return target.transform.position;
		}
	}

	void Start() {
		if(target != null)
			cameraDirection = Vector3.Normalize(transform.position - targetPosition);
		else
			cameraDirection = Vector3.zero;
	}
	
	void LateUpdate() {
		// distance
		var mouseY = Input.GetAxis("Mouse Y");
		if(Input.GetMouseButton(1) && mouseY != 0f){
			cameraDistance -= mouseY * zoomSensibility;
			if(cameraDistance > maxCameraDistance) cameraDistance = maxCameraDistance;
			if(cameraDistance < minCameraDistance) cameraDistance = minCameraDistance;
		}
		
		// looking position
		var rotation = 0f;
		if(Input.GetKeyDown(KeyCode.Q))
			rotation = -90f;
		else if(Input.GetKeyDown(KeyCode.E))
			rotation = +90f;
		
		if(rotation != 0f){
			transform.RotateAround(targetPosition, Vector3.up, rotation);
			cameraDirection = Vector3.Normalize(transform.position - targetPosition);
		}

		transform.position = targetPosition + cameraDirection * cameraDistance;
		transform.LookAt(target.transform);
	}
}
