
using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;
using System.Linq;

public class CAnimation
{
    private int mTimeState = 0;
    private EAnimationSpeed mAnimationSpeed = EAnimationSpeed.NORMAL_ANIMATION;


    public Dictionary<string, List<Sprite>> mAnimations = new Dictionary<string, List<Sprite>>();
    private SpriteRenderer renderer;
    private string mCurrentAnimation = "";
    private int mCurrentFrame;
    private bool mPlaying = false;
    private bool mLoops = false;
    private bool mReverse = false;

    private int mSpeed = 0;

    public CAnimation()
    {

    }

    public void loadAnimation(SpriteRenderer aRenderer, string aLocation, EAnimationSpeed aAnimationSpeed = EAnimationSpeed.NORMAL_ANIMATION)
    {

        mAnimations.Add(aLocation.parsedName(), CHelper.loadSequenceSprites(aLocation));
        mCurrentAnimation = aLocation.parsedName();
        mPlaying = true;
        renderer = aRenderer;

        changeSpeed(aAnimationSpeed);

    }


    public void play(string aAnimationName, bool aLoops, bool aReverse = false)
    {
        var exists = mAnimations.ContainsKey(aAnimationName);
        if (exists)
        {
            ////keep the current rotation, because unity hides all these variables we have more extra work
            //var quaternion = mCurrentAnimation.transform.rotation;
            //var angles = quaternion.eulerAngles;
            //var rotation = angles.y;
            //mCurrentAnimation = anim;
            //quaternion = mCurrentAnimation.transform.rotation;
            //angles = quaternion.eulerAngles;
            //angles.y = rotation;
            //quaternion.eulerAngles = angles;
            //mCurrentAnimation.transform.rotation = quaternion;
            //var copy = aPosition.copy();
            //copy.y = copy.y * -1;
            //anim.transform.position = copy.toVec3();
            mCurrentAnimation = aAnimationName;
            mLoops = aLoops;
            mReverse = aReverse;

            calculateCurrentFrame(true);
        }
        else
            throw new Exception("The animation specified does not exist");

    }
    public void stop()
    {
        mPlaying = false;
    }

    public void resume()
    {
        mPlaying = true;
    }

    public void update()
    {
        //check if we are in the last frame, and if the animation is supposed to loop
        if (mTimeState % mSpeed == 0)
            if (isEnded() && mLoops)
                calculateCurrentFrame(true);
            else if (!isEnded())
                calculateCurrentFrame();

        mTimeState++;
    }

    public void render()
    {
        if (mTimeState % mSpeed == 0)
            renderer.sprite = mAnimations[mCurrentAnimation][mCurrentFrame];
    }

    private void calculateCurrentFrame(bool reset = false)
    {
        if (reset)
            if (mReverse)
                mCurrentFrame = mAnimations[mCurrentAnimation].Count - 1;
            else
                mCurrentFrame = 0;
        else
            if (mReverse)
                mCurrentFrame--;
            else
                mCurrentFrame++;
    }


    public bool isEnded()
    {
        return mAnimations[mCurrentAnimation].Count - 1 == mCurrentFrame;
    }

    public void changeSpeed(EAnimationSpeed aSpeed)
    {
        if (aSpeed == EAnimationSpeed.VERY_SLOW_ANIMATION)
            mSpeed = 25;
        else if (aSpeed == EAnimationSpeed.SLOW_ANIMATION)
            mSpeed = 20;
        else if (aSpeed == EAnimationSpeed.NORMAL_ANIMATION)
            mSpeed = 10;
        else if (aSpeed == EAnimationSpeed.FAST_ANIMATION)
            mSpeed = 5;

    }
}

