﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

public class CMap
{
    public const int MAXIMUM_SECTION_LAYERS = 4;
    public const int MAXIMUM_BACKGROUND_LAYERS = 1;
    public static float mCachedDivisionFactor = 0;

    public Dictionary<string, Dictionary<string, string>> mTileProperties = new Dictionary<string, Dictionary<string, string>>();
    public List<CGenericTile> mTileSet = new List<CGenericTile>();
    //tiles can be over each other
    public List<List<List<CGenericTile>>> mMapTiles = new List<List<List<CGenericTile>>>();
    private Dictionary<string, List<CSector>> mSectors = new Dictionary<string, List<CSector>>();
    //this is generally the borders of the map, we dont want to place things over them
    CVector2D mBorderOffset = new CVector2D();
    public List<CVector2D> mTilePositions = new List<CVector2D>();
    CLayer mLayer = new CLayer();

    string mPartialTilesetPath;
    XDocument mXml = new XDocument();
    //in tiles
    public static CVector2D mMapSize = new CVector2D();
    public static CVector2D mPixelMapSize = new CVector2D();
    public static CVector2D mLastSectionPosition = new CVector2D();
    //this is to improve performance, instead of using division, we will use multiplication by this factor
    public static int mTileSize;

    public CVector2D mTileSetSize = new CVector2D();

    public struct SBlockageResult
    {
        public bool NothingBlocks;
        public CVector3D NearestPlaceToGo;
    }
    private static CMap mInst;
    public static CMap inst()
    {
        return mInst;
    }

    public CMap(CLayer aMapLayer, string aXmlMapPath, string aPartialTilesetPath)
    {
        mLayer = aMapLayer;
        mPartialTilesetPath = aPartialTilesetPath;

        TextAsset textAsset = (TextAsset)Resources.Load(aXmlMapPath);

        mXml = XDocument.Parse(textAsset.text);
        mMapSize.x = (int)mXml.Root.Attribute("width");
        mMapSize.y = (int)mXml.Root.Attribute("height");
        mTileSize = (int)mXml.Root.Attribute("tilewidth");
        mPixelMapSize.x = (int)mXml.Root.Attribute("width") * mTileSize;
        mPixelMapSize.y = (int)mXml.Root.Attribute("height") * mTileSize;
        mBorderOffset = new CVector2D(mTileSize, mTileSize);
        mCachedDivisionFactor = 1f / (float)mTileSize;
        //load all properties from tiles
        var tilesWithProperties = mXml.Element("map").Element("tileset").Elements("tile");
        for (int tileIndex = 0; tileIndex < tilesWithProperties.Count(); tileIndex++)
        {
            XElement midway = tilesWithProperties.FirstOrDefault();
            IEnumerable<XElement> properties = new List<XElement>();

            midway = tilesWithProperties.ElementAt(tileIndex).Element("properties");

            if (midway != null)
            {
                properties = midway.Elements("property");

                Dictionary<string, string> dictionaryWithProperties = new Dictionary<string, string>();
                for (int propIndex = 0; propIndex < properties.Count(); propIndex++)
                {

                    var key = properties.ElementAt(propIndex).Attribute("name").Value;
                    var value = properties.ElementAt(propIndex).Attribute("value").Value;
                    dictionaryWithProperties.Add(key, value);
                }
                mTileProperties.Add((Convert.ToInt32(tilesWithProperties.ElementAt(tileIndex).Attribute("id").Value) + 1).ToString(), dictionaryWithProperties);
            }
        }


        mTileSetSize.x = Convert.ToInt32(mXml.Element("map").Element("tileset").Element("image").Attribute("width").Value) * mCachedDivisionFactor;
        mTileSetSize.y = Convert.ToInt32(mXml.Element("map").Element("tileset").Element("image").Attribute("height").Value) * mCachedDivisionFactor;

        mInst = this;

    }

    public void init(CMapStrategy aMapStrategy)
    {
        aMapStrategy.init();
    }

    public List<CGenericTile> loadMapSector(string aSectionName, CLayer aLayer, bool aInitMethod = false)
    {
        List<CGenericTile> tileSection = new List<CGenericTile>();

        if (!aInitMethod && mTileSet.Count() == 0)
        {
            throw new Exception("You must call loadInitialSector before this method");
        }
        var elements = mXml.Element("map").Elements("layer");
        List<XElement> sectors = new List<XElement>();
        foreach (XElement element in elements)
        {
            if (element.Attribute("name").Value.Contains(aSectionName))
                sectors.Add(element);
        }
        if (sectors.Count == 0)
            throw new Exception("Sector does not exist");


        for (int _i = 0; _i < sectors.Count; _i++)
        {
            //get children
            var holder = sectors[_i].Element("data");

            var tiles = holder.Elements().ToList();
            CVector3D lastPos = new CVector3D();
            lastPos.z = aLayer.Z;
            for (int i = 0; i < tiles.Count(); i++)
            {
                var tile = tiles[i];
                //emtpy tiles have 0 by default in tiled
                if (tile.Attribute("gid").Value != "0")
                {
                    GameObject graphic = new GameObject();
                    CTile mTile = new CTile(mLayer, graphic);

                    //debugging
                    var value = tile.Attribute("gid").Value;

                    mTile.setGraphic(CHelper.loadSpriteToGameObject(mTile.getGraphic(), mPartialTilesetPath, Convert.ToInt32(tile.Attribute("gid").Value) - 1, true));

                    if (mTileProperties.ContainsKey(value))
                    {
                        for (int index = 0; index < mTileProperties[value].Count; index++)
                        {
                            var item = mTileProperties[value].ElementAt(index);
                            mTile.setDynamicProperty(item.Key, item.Value);
                        }
                    }
                    mTile.setPos(lastPos);
                    mTile.setY(mTile.getY());
                    tileSection.Add(mTile);
                }
                lastPos.x = ((i + 1) % mMapSize.x) * mTileSize;
                if ((i + 1) % mMapSize.x == 0)
                {
                    lastPos.y += mTileSize;
                }
            }
        }

        return tileSection;
    }


    public void loadInitialSector(string aSectionName, CLayer aLayer)
    {

        mTileSet = loadMapSector(aSectionName, aLayer, true);

        placeSection(mTileSet, new CVector2D(mMapSize.x * mTileSize, mMapSize.y * mTileSize), aSectionName, aLayer, true);
    }


    public void initRandomMap(uint tileSize, int tileAmmounts, GameObject aMapGrahic)
    {
        mTileSize = (int)tileSize;
        mMapSize.x = tileAmmounts;
        CGenericTile.TileSize = (int)tileSize;
        CGenericTile tile;

        List<List<CGenericTile>> tilesOnX = new List<List<CGenericTile>>();

        GameObject graphicCopy = null;

        for (int y = 0; y < tileAmmounts; y++)
        {
            tilesOnX = new List<List<CGenericTile>>();
            for (int x = 0; x < tileAmmounts; x++)
            {
                //int tileNumber = CMath.randInt(1, 5);

                //tile.render();
                if (graphicCopy != null)
                {
                    graphicCopy = (GameObject)CHelper.Instantiate(graphicCopy);
                    graphicCopy.name = string.Concat("Tile with x : ", x, " and y : ", y);
                }
                else
                {
                    graphicCopy = (GameObject)CHelper.Instantiate(aMapGrahic);
                    aMapGrahic.SetActive(false);
                }
                tile = new CGenericTile(CLayer.getLayer(ELayer.Ground), graphicCopy);

                List<CGenericTile> tilesHere = new List<CGenericTile>();
                tilesHere.Add(tile);
                tilesOnX.Add(tilesHere);
                tile.activate();

                CGraphicsManager.inst().addGraphic(tile, EGraphics.Tiles);
            }
            //once we finish with x , we switch back to Y
            switch (CGenericTile.mBhevaiour)
            {
                case CGenericTile.GROW_X:
                    CGenericTile.mBhevaiour = CGenericTile.GROW_Y;
                    break;
                case CGenericTile.GROW_Y:
                    CGenericTile.mBhevaiour = CGenericTile.GROW_X;
                    break;
            }
            mMapTiles.Add(tilesOnX);
        }


    }

    public void initUnityMap(uint tileSize, GameObject aMap, CMapStrategy aMapStrategy)
    {
        CGenericTile.mBhevaiour = CGenericTile.NONE;
        var tiles = aMap.GetComponentsInChildren(typeof(Transform));
        List<Transform> transforms = new List<Transform>();
        CGenericTile tile;
        foreach (Transform kidlette in tiles)
        {
            transforms.Add(kidlette);

            tile = new CGenericTile(CLayer.getLayer(ELayer.Ground), kidlette.gameObject);
            CGraphicsManager.inst().addGraphic(tile, EGraphics.Tiles);

        }

        var sameX = transforms
    .GroupBy(i => i.localPosition.x)
    .Where(g => g.Count() > 1)
    .Select(g => g.Key);

        var sameY = transforms
    .GroupBy(i => i.localPosition.y)
    .Where(g => g.Count() > 1)
    .Select(g => g.Key);

        mMapSize.x = sameX.Count();
        mMapSize.y = sameY.Count();

        mTileSize = (int)tileSize;

        CVars.inst().setMap(this);

        aMapStrategy.init();
    }

    public void update()
    {
        //update tiles if required
    }

    public CGenericTile getCenterTile()
    {
        var index = (int)(mMapSize.x * 0.5);
        return (mMapTiles[index][index])[0] as CGenericTile;
    }

    //public CGenericTile getTileWithIndex(int x, int y)
    //{
    //    if (x < 0 || y < 0 || x >= mMapSize.x || y >= mMapSize.y)
    //    {
    //        Console.WriteLine("Coordenates are out of range: x=" + x + " y=" + y);
    //        return mMapTiles[0][0];
    //    }
    //    else
    //    {
    //        return (mMapTiles[y][x]);
    //    }
    //}

    public CGenericTile getTileInSectorWithPixels(double x, double y)
    {
        CSector sector = getSector(x, y);

        if (x < 0 || y < 0 || x >= mMapSize.x * mTileSize || y >= mMapSize.y * mTileSize)
        {
            Console.WriteLine("Coordenates are out of range: x=" + x + " y=" + y);
            return mSectors[sector.getName()][sector.getIndex()].mSector.FirstOrDefault(c => c.getX() == 0 && c.getY() == 0);
        }
        else
        {
            x = (int)Math.Floor((double)(x * mCachedDivisionFactor));
            y = (int)Math.Floor((double)(y * mCachedDivisionFactor));

            var result = mSectors[sector.getName()][sector.getIndex()].mSector.FirstOrDefault(c => c.getX() == (x * getTileWidth()) && c.getY() == (y * getTileHeight()));
            //tile sections have empty areas, if an area is empty it means is walkable, so lets create a tile and return it
            if (result == null)
            {
                result = new CGenericTile(mSectors[sector.getName()][sector.getIndex()].mLayer, new GameObject());
                result.setX(x * CGameConstants.TILE_WIDTH);
                result.setY(y * CGameConstants.TILE_HEIGHT);

                mMapTiles[(int)y][(int)x].Add(result);
                mSectors[sector.getName()][sector.getIndex()].mSector.Add(result);
            }
            return result;
        }
    }

    private CGenericTile getNonWalkableTiles(List<CGenericTile> aTiles)
    {
        for (var i = 0; i < aTiles.Count; i++)
        {
            if (!aTiles[i].isWalkable())
            {
                return aTiles[i];
            }
        }
        return aTiles[0];
    }

    public List<CGenericTile> getTileGlobalWithPixels(double x, double y)
    {
        try
        {
            if (x < 0 || y < 0 || x >= mMapSize.x * mTileSize || y >= mMapSize.y * mTileSize)
            {
                Console.WriteLine("Coordenates are out of range: x=" + x + " y=" + y);
                return mMapTiles[0][0];
            }
            else
            {
                x = (int)Math.Floor((double)(x * mCachedDivisionFactor));
                y = (int)Math.Floor((double)(y * mCachedDivisionFactor));

                var result = mMapTiles[(int)(y)][(int)(x)];

                return result;
            }
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public CGenericTile getTileGlobalNonWalkableWithPixels(double x, double y)
    {

        if (x < 0 || y < 0 || x >= mMapSize.x * mTileSize || y >= mMapSize.y * mTileSize)
        {
            Console.WriteLine("Coordenates are out of range: x=" + x + " y=" + y);
            return mMapTiles[0][0][0];
        }
        else
        {
            x = (int)Math.Floor((double)(x * mCachedDivisionFactor));
            y = (int)Math.Floor((double)(y * mCachedDivisionFactor));

            var result = mMapTiles[(int)(y)][(int)(x)];
            CGenericTile tile;
            if (result.Count == 0)
            {
                tile = new CGenericTile(CLayer.getLayer(ELayer.None), new GameObject());
                tile.setX(x * CGameConstants.TILE_WIDTH);
                tile.setY(y * CGameConstants.TILE_HEIGHT);

                mMapTiles[(int)y][(int)(x)].Add(tile);
            }
            else
            {
                return getNonWalkableTiles(result);
            }
            return tile;
        }
    }
    public bool isWalkableAt(double x, double y, ref CGenericTile aTile)
    {
        if (x < 0 || y < 0 || x >= getWorldWidthInPixels() || y >= getWorldHeightInPixels())
        {
            Console.WriteLine("Coordenates are out of range: x=" + x + " y=" + y);
            return false;
        }
        else
        {
            x = (int)Math.Floor((double)(x * mCachedDivisionFactor));
            y = (int)Math.Floor((double)(y * mCachedDivisionFactor));

            var result = mMapTiles[(int)(y)][(int)(x)];
            if (result.Count == 0)
            {
                aTile = new CGenericTile(CLayer.getLayer(ELayer.None), new GameObject());
                aTile.setX(x * CGameConstants.TILE_WIDTH);
                aTile.setY(y * CGameConstants.TILE_HEIGHT);


                mMapTiles[(int)y][(int)(x)].Add(aTile);
            }
            else
            {
                aTile = getNonWalkableTiles(result);
                return getNonWalkableTiles(result).isWalkable();
            }
            return aTile.isWalkable();
        }
    }

    public bool isWalkableAtForObject(double x, double y, ref CGenericTile aTile, CVector2D aMin, CVector2D aMax)
    {
        if (x < aMin.x || y < aMin.y || x + CGameConstants.TILE_WIDTH >= aMax.x || y + CGameConstants.TILE_HEIGHT >= aMax.y)
        {
            //Console.WriteLine("Coordenates are out of range: x=" + x + " y=" + y);
            return false;
        }
        else
        {
            x = (int)Math.Floor((double)(x * mCachedDivisionFactor));
            y = (int)Math.Floor((double)(y * mCachedDivisionFactor));


            try
            {
                var result = mMapTiles[(int)(y)][(int)(x)];
                if (result.Count == 0)
                {
                    aTile = new CGenericTile(CLayer.getLayer(ELayer.None), new GameObject());
                    aTile.setX(x * CGameConstants.TILE_WIDTH);
                    aTile.setY(y * CGameConstants.TILE_HEIGHT);

                    mMapTiles[(int)y][(int)(x)].Add(aTile);
                }
                else
                {
                    aTile = getNonWalkableTiles(result);
                    return getNonWalkableTiles(result).isWalkable(aMin, aMax);
                }
                return aTile.isWalkable(aMin, aMax);
            }

            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }


    public int getTileHeight()
    {
        return mTileSize;
    }
    public int getTileWidth()
    {
        return mTileSize;
    }

    public uint getWorldWidth()
    {
        return (uint)(mTileSize * mMapSize.x);
    }
    public uint getWorldHeight()
    {
        return (uint)(mTileSize * mMapSize.y);
    }

    public uint getWorldWidthInPixels()
    {
        return (uint)(mPixelMapSize.x);
    }
    public uint getWorldHeightInPixels()
    {
        return (uint)(mPixelMapSize.y);
    }


    public void placeSection(List<CGenericTile> aTiles, CVector2D aMapSizeInPixels, string aSectorName, CLayer aLayer, bool aSpecial = false)
    {
        //i have to move all tiles in x until there is no more x space (AKA WIDTH) then i have to move in y and then x again
        bool mAdded = false;
        //try to move on X first
        //pixels

        if (!aSpecial)
        {
            while (!mAdded)
            {
                //check if we can actually add sections
                if (mLastSectionPosition.x + aMapSizeInPixels.x < getWorldWidthInPixels())
                {

                    //move everything this much distance, but first check if it will overlap
                    while (checkIfOverlap(new CRect((uint)aMapSizeInPixels.x, (uint)aMapSizeInPixels.y, Math.Abs(mLastSectionPosition.x + mBorderOffset.x), Math.Abs(mLastSectionPosition.y + mBorderOffset.y))))
                    {
                        //first lets move on the x axis
                        if (mLastSectionPosition.x + aMapSizeInPixels.x + mBorderOffset.x < getWorldWidthInPixels())
                        {
                            //move one tile at a time
                            mLastSectionPosition.x += mTileSize;
                        }
                        //if we cannot, lets start again from down below
                        //TODO CHECK THE Y AXIS IF WE CANNOT MOVE FURTHER
                        else
                        {
                            mLastSectionPosition.y += mTileSize;
                            //restart X
                            mLastSectionPosition.x = 0;
                        }
                        //if this happens, we cannot add more sections
                        if (mLastSectionPosition.y > getWorldHeightInPixels())
                        {
                            throw new Exception("You cannot add more sections, or section to be added is too big and there is no space");
                        }

                    }
                    //given that we can, lets start were we left off, in mLastSectionPosition and move only from there
                    var leftMostTile = aTiles.OrderBy(z => z.getX()).FirstOrDefault();
                    var topMostTile = aTiles.OrderBy(z => z.getY()).FirstOrDefault();
                    //this is the distance between were the section currently is, and where it should be 


                    double distanceX = 0;
                    double distanceY = 0;

                    distanceX = mLastSectionPosition.x - leftMostTile.getX();
                    distanceY = mLastSectionPosition.y - topMostTile.getY();
                    //this is so that no matter what we move in the right direction
                    distanceX *= -1;
                    distanceY *= -1;

                    for (int i = 0; i < aTiles.Count; i++)
                    {
                        aTiles[i].setX(Math.Abs(aTiles[i].getX() - distanceX + mBorderOffset.x));
                        aTiles[i].setY(Math.Abs(aTiles[i].getY() - distanceY + mBorderOffset.y));
                    }
                    mLastSectionPosition.x += mBorderOffset.x;

                    mAdded = true;


                    if (mSectors.ContainsKey(aSectorName))
                    {
                        mSectors[aSectorName].Add(new CSector(aTiles, (uint)aMapSizeInPixels.x, (uint)aMapSizeInPixels.y, new CVector2D(leftMostTile.getX(), topMostTile.getY()), aSectorName, mSectors[aSectorName].Count, aLayer));
                    }
                    else
                    {
                        var list = new List<CSector>();
                        list.Add(new CSector(aTiles, (uint)aMapSizeInPixels.x, (uint)aMapSizeInPixels.y, new CVector2D(leftMostTile.getX(), topMostTile.getY()), aSectorName, 0, aLayer));
                        mSectors.Add(aSectorName, list);
                    }

                }
                else if (mLastSectionPosition.y + aMapSizeInPixels.y < getWorldHeightInPixels())
                {
                    //restart x
                    mLastSectionPosition.x = 0;
                    //go down
                    mLastSectionPosition.y += mSectors.Values.First()[0].getRect().height + mBorderOffset.y;
                }
                else
                {
                    throw new Exception("Cannot add more sections, map is too small");
                }
                //if not possible to move on X move on Y first then on X
            }
        }
        else
        {
            var list = new List<CSector>();
            list.Add(new CSector(aTiles, (uint)aMapSizeInPixels.x, (uint)aMapSizeInPixels.y, new CVector2D(0, 0), aSectorName, 0, aLayer));
            mSectors.Add(aSectorName, list);
        }

    }

    public bool checkIfOverlap(CRect aRect2)
    {
        for (int index = 0; index < mSectors.Count; index++)
        {
            //make two squares
            var item = mSectors.ElementAt(index);
            //SKIP SECTION ONE! SECTION ONE IS THE BASIS!!
            for (int i = 0; i < item.Value.Count; i++)
            {
                if (item.Value[0].getName() != EMapLayers.MapLayout)
                {
                    CRect rect1 = item.Value[i].getRect();
                    var theyCollide = CMath.rectRectCollision(rect1, aRect2);

                    var theyContain = CMath.contains(rect1, aRect2);

                    if (theyCollide || theyContain)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public SBlockageResult isWallOrBuildingBlocking(CVector3D aObjectMoving, CVector3D aWhereToGo)
    {
        //we create a rectangle 
        if (aObjectMoving.x == aWhereToGo.x)
        {
            //get all tiles in the same x position with direction towards target
        }
        else if (aObjectMoving.y == aWhereToGo.y)
        {
            //get all tiles in the same y position with direction towards target
        }
        //create rectangle, get all tiles inside, find intersections of the line between the two vectors to get the tiles
        else
        {
            //distance from the object to were it wants to go
            var distance = (aWhereToGo - aObjectMoving).normalized;

            //**GET TOP LEFT OF THE RECTANGLE !!**//
            CRect aRectangle = new CRect();
            //is pointing right and down
            var angle = distance.getAngle();
            if (angle > 0 && distance.getAngle() < 90)
            {
                aRectangle.x = aObjectMoving.x;
                aRectangle.y = aObjectMoving.y;
            }
            //is pointing left and down
            else if (angle > 90 && distance.getAngle() < 180)
            {
                aRectangle.x = aWhereToGo.x;
                aRectangle.y = aObjectMoving.y;
            }
            //is pointing left and up
            else if (angle > 180 && distance.getAngle() < 270)
            {
                aRectangle.x = aWhereToGo.x;
                aRectangle.y = aWhereToGo.y;
            }
            //is pointing right and up
            else if (angle > 270 && distance.getAngle() < 360)
            {
                aRectangle.x = aObjectMoving.x;
                aRectangle.y = aWhereToGo.y;
            }

            aRectangle.width = (uint)Math.Abs(aObjectMoving.x - aWhereToGo.x);
            aRectangle.height = (uint)Math.Abs(aObjectMoving.y - aWhereToGo.y);

            //RECTANGLE TOP LEFT
            var xPos = (int)aRectangle.x;
            var yPos = (int)aRectangle.y;

            var horizontalLines = new List<CLine>();
            while (yPos < aRectangle.y + aRectangle.height)
            {
                yPos += getTileHeight();
                horizontalLines.Add(new CLine(xPos, yPos, xPos + (int)aRectangle.width, yPos));
                xPos = (int)aRectangle.x;
            }
            var verticalLines = new List<CLine>();
            while (yPos < aRectangle.y + aRectangle.height)
            {
                yPos += getTileHeight();

                horizontalLines.Add(new CLine(xPos, yPos, xPos, yPos + (int)aRectangle.height));
                xPos = (int)aRectangle.x;
            }
            //NOW IFIND INTERSECTION POINTS AND GET TILES

            //verticalLines.Add(getTileInSectorWithPixels(xPos, yPos));
            //verticalLines.Add(getTileInSectorWithPixels(xPos + (int)aRectangle.width, yPos));
        }

        return new SBlockageResult();
    }

    public CSector getSector(CVector3D aPos)
    {
        List<CSector> sectors = new List<CSector>();
        for (int index = 0; index < mSectors.Count && sectors.Count < 2; index++)
        {
            var item = mSectors.ElementAt(index);
            var itemKey = item.Key;
            foreach (var c in item.Value)
            {
                if (c.getRect().x <= aPos.x && aPos.x <= c.getRect().x + c.getRect().width && c.getRect().y <= aPos.y && aPos.y <= c.getRect().y + c.getRect().height)
                {
                    sectors.Add(c);
                }
            }
        }
        if (sectors.Count > 1)
        {
            //return first found sector other than default
            return sectors[1];
        }
        else
        {
            //return default sector if no other sector matched
            return sectors[0];
        }

    }
    public CSector getSector(double x, double y)
    {
        List<CSector> sectors = new List<CSector>();
        for (int index = 0; index < mSectors.Count && sectors.Count < 2; index++)
        {
            var item = mSectors.ElementAt(index);
            var itemKey = item.Key;
            foreach (var c in item.Value)
            {
                if (c.getRect().x <= x && x <= c.getRect().x + c.getRect().width && c.getRect().y <= y && y <= c.getRect().y + c.getRect().height)
                {
                    sectors.Add(c);
                }
            }
        }
        if (sectors.Count > 1)
        {
            //return first found sector other than default
            return sectors[1];
        }
        else
        {
            //return default sector if no other sector matched
            return mSectors[EMapLayers.MapLayout][0];
        }
    }

    public void mergeSections()
    {
        if (mMapTiles.Count == 0)
        {
            var tiles = new List<CGenericTile>();

            foreach (var key in mSectors)
            {
                foreach (var sector in key.Value)
                {
                    tiles.AddRange(sector.mSector);
                }
            }
            var howManyX = mMapSize.x * getTileHeight();
            var howManyY = mMapSize.y * getTileWidth();
            CVector2D currentLocation = new CVector2D(0, 0);
            var tilesOnThisX = new List<List<CGenericTile>>();
            for (int y = 0; y < howManyY; y += getTileHeight())
            {
                tilesOnThisX = new List<List<CGenericTile>>();
                for (int x = 0; x < howManyX; x += getTileWidth())
                {
                    List<CGenericTile> result = new List<CGenericTile>();

                    for (var index = 0; index < tiles.Count; index++)
                    {
                        if (tiles[index].getX() == x && tiles[index].getY() == y)
                        {
                            result.Add(tiles[index]);
                        }
                    }
                    tilesOnThisX.Add(result);
                }
                mMapTiles.Add(tilesOnThisX);
            }
        }
    }

    public List<CGenericTile> getNeighborsWalkables(CGenericTile node, EDiagonalMovement diagonalMovement, CVector2D aMin, CVector2D aMax)
    {
        double x = node.getX(),
            y = node.getY();
        var neighbors = new List<CGenericTile>();
        bool s0 = false, d0 = false,
        s1 = false, d1 = false,
        s2 = false, d2 = false,
        s3 = false, d3 = false;
        //make sure we merged tiles before continuing 
        if (mMapTiles.Count == 0)
        {
            mergeSections();
        }
        CGenericTile aRefTile = new CGenericTile();

        double xx = 0;
        double yy = 0;


        yy = y - getTileHeight();
        xx = x;
        // ↑
        if (isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
            s0 = true;
        }
        yy = y;
        xx = x + getTileWidth();
        // →
        if (isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
            s1 = true;
        }

        yy = y + getTileHeight();
        xx = x;
        // ↓
        if (isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
            s2 = true;
        }

        yy = y;
        xx = x - getTileWidth();
        // ←
        if (isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
            s3 = true;
        }


        if (diagonalMovement == EDiagonalMovement.Never)
        {
            return neighbors;
        }

        if (diagonalMovement == EDiagonalMovement.OnlyWhenNoObstacles)
        {
            d0 = s3 && s0;
            d1 = s0 && s1;
            d2 = s1 && s2;
            d3 = s2 && s3;
        }
        else if (diagonalMovement == EDiagonalMovement.IfAtMostOneObstacle)
        {
            d0 = s3 || s0;
            d1 = s0 || s1;
            d2 = s1 || s2;
            d3 = s2 || s3;
        }
        else if (diagonalMovement == EDiagonalMovement.Always)
        {
            d0 = true;
            d1 = true;
            d2 = true;
            d3 = true;
        }
        else
        {
            throw new Exception("Incorrect value of diagonalMovement");
        }


        yy = y - getTileHeight();
        xx = x - getTileWidth();
        // ↖
        if (d0 && this.isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
        }
        yy = y - getTileHeight();
        xx = x + getTileWidth();
        // ↗
        if (d1 && this.isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
        }
        yy = y + getTileHeight();
        xx = x + getTileWidth();
        // ↘
        if (d2 && this.isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
        }
        yy = y + getTileHeight();
        xx = x - getTileWidth();
        // ↙
        if (d3 && this.isWalkableAtForObject(xx, yy, ref aRefTile, aMin, aMax))
        {
            mTilePositions.Add(new CVector2D(xx, yy));
            neighbors.Add(aRefTile);
        }

        return neighbors;
    }
    public struct SNeighbours
    {
        public List<CGenericTile> right;
        public List<CGenericTile> bottom;
        public List<CGenericTile> left;
        public List<CGenericTile> top;
    }
    public SNeighbours getNeighbors(CGenericTile node, EDiagonalMovement diagonalMovement)
    {

        double x = node.getX(),
               y = node.getY();

        double xx = 0;
        double yy = 0;


        yy = y - getTileHeight();
        xx = x;

        // ↑
        var top = getTileGlobalWithPixels(xx, yy);
        yy = y;
        xx = x + getTileWidth();

        // →
        var right = getTileGlobalWithPixels(xx, yy);
        yy = y + getTileHeight();
        xx = x;

        // ↓
        var bottom = getTileGlobalWithPixels(xx, yy);
        yy = y;
        xx = x - getTileWidth();
        // ←
        var left = getTileGlobalWithPixels(xx, yy);

        if (top.Count > 1)
        {
            if (top[1].getName() == "New Game Object")
                top = null;
        }
        if (right.Count > 1)
        {
            if (right[1].getName() == "New Game Object")
                right = null;
        }
        if (bottom.Count > 1)
        {
            if (bottom[1].getName() == "New Game Object")
                bottom = null;
        }
        if (left.Count > 1)
        {
            if (left[1].getName() == "New Game Object")
                left = null;
        }


        return new SNeighbours() { left = left, right = right, top = top, bottom = bottom };
    }

    public void resetPositions()
    {
        mTilePositions = new List<CVector2D>();
    }

    public double randomXInMap()
    {
        return CMath.randInt(0, getWorldWidth());
    }

    public double randomYInMap()
    {

        return CMath.randInt(0, getWorldHeight());
    }


}