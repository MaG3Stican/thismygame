using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class OptionsMenu : MonoBehaviour {	
	private const int height = 430;
	private const int width = 750;
	
	public OptionsBag Options;
	
	#region Behaviour
	
	private void BackSettings(){
    	Debug.Log("Back Settings");
		GoToMainMenu();
	}
	
	private void ApplySettings(){
		Debug.Log("Apply Settings");
		Options.Apply();
		GoToMainMenu();
	}

	void ChangeVSync ()
	{
		Debug.Log("Change VSync");
		Options.VSync = !Options.VSync;
	}

	void ChangeFullScreen ()
	{
		Debug.Log("Switch Fullscreen");
		Options.FullScreen = !Options.FullScreen;
	}
	
	private void GoToMainMenu(){
		var script = (Behaviour) this.GetComponent("MainMenu");
		script.enabled = true;
		this.enabled = false;
	}

	void ChangeResolution ()
	{
		Debug.Log("Change resolution");
		Options.SwitchScreenResolution();
	}

	void ChangeGraphicsQuality ()
	{
		Debug.Log("Change Quality Settings");
		Options.SwitchGraphicsQuality();
	}

	void ChangeAA ()
	{
		Debug.Log("Change AA");
		Options.SwitchAA();
	}

	void ChangeShadowQuality ()
	{
		Debug.Log("Change Shadow Quality");
		Options.SwitchShadowQuality();
	}

	void ChangeAnisotropicTextures ()
	{
		Debug.Log("Change Anisotropic Textures");
		Options.SwitchAnisotropicTextures();
	}

	void ChangeParticlesDetail ()
	{
		Debug.Log("Change Particles Level");
		Options.SwitchParticlesDetail();
	}

	void ChangeLoDBias ()
	{
		Debug.Log("Change LoD Bias");
		Options.SwitchLoDBias();
	}

	void ChangeBlendWeightsLevel ()
	{
		Debug.Log("Change Blend Weights");
		Options.SwitchBlendWeightsLevel();
	}
	
	#endregion
	
	#region Layout Rendering
	
	private void RenderLeftBox(){
		GUI.BeginGroup(new Rect(20, 50, width/2-40, height-100));
		
		
		var screenResolutionTooltip = "Monitor screen resolution. Lower it for better performance.";
		GUI.Label(
			new Rect(0, 5, 130, 32),
			new GUIContent("Screen Resolution", screenResolutionTooltip)
			);
		if(GUI.Button(
			new Rect(130, 0, 170, 32),
			new GUIContent(Options.ScreenResolutionStr, screenResolutionTooltip))){
			ChangeResolution();
		}
		
		var fullScreenTooltip = "Switch off to play in a window.";
		GUI.Label(
			new Rect(0, 32+5+10, 130, 32),
			new GUIContent("Switch Fullscreen", fullScreenTooltip)
			);		
		if(GUI.Button(
			new Rect(130, 32+10, 170, 32),
			new GUIContent(Options.FullScreenStr, fullScreenTooltip)
			)){
			ChangeFullScreen();
		}
		
		var vSyncTooltip = "Reduces texture tearing. Deactivate it to increase performance.";
		GUI.Label(
			new Rect(0, 32*2+5+10*2, 130, 32),
			new GUIContent("Vertical Sync", vSyncTooltip)
			);
		if(GUI.Button(
			new Rect(130, 32*2+10*2, 170, 32),
			new GUIContent(Options.VSyncStr, vSyncTooltip))){
			ChangeVSync();
		}
		
		GUI.Label(
			new Rect(0, 32*3+5+10*3, 130, 32),
			new GUIContent("Brightness", "Screen Brightness.")
			);
		Options.Brightness = GUI.HorizontalSlider(
			new Rect(130, 32*3+10*3+10, 170, 32),
			Options.Brightness,
			OptionsBag.MinBrightness,
			OptionsBag.MaxBrightness
			);
		
		GUI.EndGroup();
	}

	void RenderRightBox ()
	{
		GUI.BeginGroup(new Rect(width/2+20, 50, width/2-40, height-100));
		
		var labelWidth = 130;
		var buttonWidth = 170;
		var buttonHeight = 32;
		var marginTopLabel = 5;
		
		var qualitySettingsTooltip = "Set the overall graphics quality";
		GUI.Label(
			new Rect(0, marginTopLabel, labelWidth, buttonHeight),
			new GUIContent("Graphics Quality", qualitySettingsTooltip)
			);
		if(GUI.Button(
			new Rect(labelWidth, 0, buttonWidth, buttonHeight),
			new GUIContent(Options.GraphicsQuality, qualitySettingsTooltip))){
			ChangeGraphicsQuality();
		}
		
		var aaTooltip = "Set Global antialiasing Quality";
		GUI.Label(
			new Rect(0, buttonHeight+50+marginTopLabel, labelWidth, buttonHeight),
			new GUIContent("Antialiasing", aaTooltip)
			);
		if(GUI.Button(
			new Rect(labelWidth, buttonHeight+50, buttonWidth, buttonHeight),
			new GUIContent(Options.AA, aaTooltip))){
			ChangeAA();
		}
		
		var shadowQualityTooltip = "Set Shadow Quality";
		GUI.Label(
			new Rect(0, 50+2*(buttonHeight+marginTopLabel), labelWidth, buttonHeight),
			new GUIContent("Shadow Quality", shadowQualityTooltip)
			);
		if(GUI.Button(
			new Rect(130, 50+2*(buttonHeight+marginTopLabel), buttonWidth, buttonHeight),
			new GUIContent(Options.ShadowQuality, shadowQualityTooltip))){
			ChangeShadowQuality();
		}
		
		var anisotropicTexturesTooltip = "Toggle Anisotropic Textures Quality";
		GUI.Label(
			new Rect(0, 50+3*(buttonHeight+marginTopLabel), labelWidth, buttonHeight),
			new GUIContent("Anisotropic Textures", anisotropicTexturesTooltip)
			);
		if(GUI.Button(
			new Rect(130, 50+3*(buttonHeight+marginTopLabel), buttonWidth, buttonHeight),
			new GUIContent(Options.AnisotropicTextures, anisotropicTexturesTooltip))){
			ChangeAnisotropicTextures();
		}
		
		var softParticlesTooltip = "Change Particles Level";
		GUI.Label(
			new Rect(0, 50+4*(buttonHeight+marginTopLabel), labelWidth, buttonHeight),
			new GUIContent("Particles Detail", softParticlesTooltip)
			);
		if(GUI.Button(
			new Rect(130, 50+4*(buttonHeight+marginTopLabel), buttonWidth, buttonHeight),
			new GUIContent(Options.ParticlesDetail, softParticlesTooltip))){
			ChangeParticlesDetail();
		}
		
		var lodTooltip = "Change Level of Detail";
		GUI.Label(
			new Rect(0, 50+5*(buttonHeight+marginTopLabel), labelWidth, buttonHeight),
			new GUIContent("LoD Bias", lodTooltip)
			);
		if(GUI.Button(
			new Rect(130, 50+5*(buttonHeight+marginTopLabel), buttonWidth, buttonHeight),
			new GUIContent(Options.LoDBias, lodTooltip))){
			ChangeLoDBias();
		}
		
		var blendWeightsTooltip = "Change Blend Weights";
		GUI.Label(
			new Rect(0, 50+6*(buttonHeight+marginTopLabel), labelWidth, buttonHeight),
			new GUIContent("Blend Weights", blendWeightsTooltip)
			);
		if(GUI.Button(
			new Rect(130, 50+6*(buttonHeight+marginTopLabel), buttonWidth, buttonHeight),
			new GUIContent(Options.BlendWeightsLevel, blendWeightsTooltip))){
			ChangeBlendWeightsLevel();
		}
		
		GUI.EndGroup();
	}
	
	private void RenderBottomBox(){
		const int buttonHeight = 32;
		const int marginHorizontalButton = 10;
		const int marginBottomButton = 10;
		
		Rect backgroundBox = new Rect(0, 0, width,height);
		//the menu background box
	    GUI.Box(backgroundBox, "");
		
		Rect backRect = new Rect(
			width-60-marginHorizontalButton,
			height-buttonHeight-marginBottomButton,
			60,
			buttonHeight); 
		if(GUI.Button(backRect, new GUIContent("Back", "Go back to Main Menu")))
			BackSettings();
	
		Rect applyRect = new Rect(
			width-60-120-2*marginHorizontalButton,
			height-buttonHeight-marginBottomButton,
			120,
			buttonHeight);
		if(GUI.Button(applyRect, new GUIContent("Apply Settings", "Apply and Save Current Settings")))
	    	ApplySettings();
		
		Rect tooltipRect = new Rect(marginHorizontalButton*2,
			height-26-marginBottomButton,
			applyRect.x, 26);
		GUI.Label(tooltipRect, GUI.tooltip);
	}

	void Render () {
		Rect menuGroup;
		menuGroup = new Rect((Screen.width-width) / 2, (Screen.height-height)/ 2, width, height);
		
	    GUI.BeginGroup(menuGroup);
			RenderLeftBox();
			RenderRightBox();
			RenderBottomBox();
	    GUI.EndGroup();
	}
	
	#endregion
	
	void OnGUI() {
		if(Options == null)
			Options = new OptionsBag(this.camera);
    	Render();
	}
}
