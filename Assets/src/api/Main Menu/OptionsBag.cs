using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class OptionsBag
	{
		/// <summary>
		/// The camera which render the screen. It must have a brightnessPostEffect element
		/// </summary>
		private Camera target;
		
		private BrightnessPostEffect brightnessPostEffect{
			get {
				return target.GetComponent(typeof(BrightnessPostEffect))
					as BrightnessPostEffect;
			}
		}
		
		#region Screen Settings
		
		public Resolution ScreenResolution {
			get {
				if(currentResolutionIdx >= 0 && currentResolutionIdx < Screen.resolutions.Length)
					return Screen.resolutions[currentResolutionIdx];
				else if(Screen.fullScreen)
					return Screen.currentResolution;
				else{
					Resolution res = new Resolution() {
						width= Screen.width,
						height= Screen.height,
						refreshRate= Screen.currentResolution.refreshRate
					};
				
					return res;
				}
			}
		}
		public string ScreenResolutionStr {
			get {
				var res = ScreenResolution.width + "x" + ScreenResolution.height;
				if(ScreenResolution.refreshRate > 0)
					res+= " " + ScreenResolution.refreshRate + "Hz";
				return res;
			}
		}
		
		public bool ScreenResolutionChanged {
			get {
				return currentResolutionIdx != originalScreenResolutionIdx;
			}
		}
		
		private int currentResolutionIdx = -1;
		private int originalScreenResolutionIdx = -1;
		
		public Resolution SwitchScreenResolution(){
			currentResolutionIdx = (currentResolutionIdx + 1) % Screen.resolutions.Length;
			return Screen.resolutions[currentResolutionIdx];
		}
		
		public bool VSync { get; set; }
		public string VSyncStr {
			get {
				return VSync ? "On" : "Off";
			}
		}
		public bool FullScreen { get; set; }
		public string FullScreenStr {
			get {
				return FullScreen ? "On": "Off";
			}
		}
		public readonly bool OriginalFullScreenVal;
		
		public const float MinBrightness = -0.6f;
		public const float MaxBrightness = +0.6f;
		public float Brightness { 
			get {
				return brightness;
			}
			set {
				if(value != brightness){
					brightness = value;
					SetBrightness(value);
				}
			}
		}
		private float brightness;
		public readonly float OriginalBrightness = 0.0f;
		
		#endregion
		
		#region Graphics Settings
		
		public static readonly string[] GraphicsQualityNames = new string[] {
			"Lowest",
			"Low",
			"Medium",
			"High"
		};
		public string GraphicsQuality {
			get {
				return GraphicsQualityNames[graphicsQualityIdx];
			}
		}
		private int graphicsQualityIdx = 3;
		public void SwitchGraphicsQuality(){
			graphicsQualityIdx = 
				(graphicsQualityIdx+1) % GraphicsQualityNames.Length;
			
			switch(graphicsQualityIdx){
			case 0:
				AAIdx = 0;
				ShadowQualityIdx = 0;
				AnisotropicTexturesIdx = 0;
				ParticlesDetailIdx = 0;
				LoDBiasIdx = 0;
				BlendWeightsLevelIdx = 0;
				break;
			case 1:
				AAIdx = 0;
				ShadowQualityIdx = 1;
				AnisotropicTexturesIdx = 0;
				ParticlesDetailIdx = 0;
				LoDBiasIdx = 0;
				BlendWeightsLevelIdx = 0;
				break;
			case 2:
				AAIdx = 1;
				ShadowQualityIdx = 2;
				AnisotropicTexturesIdx = 1;
				ParticlesDetailIdx = 1;
				LoDBiasIdx = 1;
				BlendWeightsLevelIdx = 1;
				break;
			case 3:
				AAIdx = 2;
				ShadowQualityIdx = 3;
				AnisotropicTexturesIdx = 1;
				ParticlesDetailIdx = 2;
				LoDBiasIdx = 2;
				BlendWeightsLevelIdx = 2;
				break;
			default:
				break;
			}
		}
		public void ApplyQualityLevel ()
		{
			switch(graphicsQualityIdx){
			case 0:
				QualitySettings.SetQualityLevel(2);
				break;
			case 1:
				QualitySettings.SetQualityLevel(3);
				break;
			case 2:
				QualitySettings.SetQualityLevel(4);
				break;
			case 3:
				QualitySettings.SetQualityLevel(5);
				break;
			default:
				break;
			}
		}

		public void LoadCurrentGraphicsSettings ()
		{
			int graphicsQualityOriginal = QualitySettings.GetQualityLevel();
			if(graphicsQualityOriginal <= 2)
				graphicsQualityIdx = 0;
			else if(graphicsQualityOriginal <= 3)
				graphicsQualityIdx = 1;
			else if(graphicsQualityOriginal <= 4)
				graphicsQualityIdx = 2;
			else
				graphicsQualityIdx = 3;
		}
		
		public static readonly string[] AANames = new string[] {
			"Disabled",
			"2x",
			"4x",
			"8x"
		};
		private static readonly int[] AAValues = new int[]{
			2,4,8
		};
		public string AA {
			get {
				return AANames[AAIdx];
			}
		}
		private int AAIdx = 2;
		public void SwitchAA(){
			AAIdx = 
				(AAIdx+1) % AANames.Length;
		}

		public void LoadCurrentAA ()
		{
			switch(QualitySettings.antiAliasing){
			case 0:
				AAIdx = 0;
				break;
			case 2:
				AAIdx = 1;
				break;
			case 4:
				AAIdx = 2;
				break;
			case 8:
				AAIdx = 3;
				break;
			default:
				break;
			}
		}

		public void ApplyAAValue ()
		{
			QualitySettings.antiAliasing = AAValues[AAIdx];
		}
		
		public static readonly string[] ShadowQualityNames = new string[] {
			"Off",
			"Low",
			"Medium",
			"High"
		};
		public string ShadowQuality {
			get {
				return ShadowQualityNames[ShadowQualityIdx];
			}
		}
		private int ShadowQualityIdx = 3;
		public void SwitchShadowQuality(){
			ShadowQualityIdx = 
				(ShadowQualityIdx+1) % ShadowQualityNames.Length;
		}
		public void ApplyShadowQuality ()
		{
			switch(ShadowQualityIdx){
			case 0:
				QualitySettings.shadowDistance = 0;
				QualitySettings.shadowCascades = 0;
				break;
			case 1:
				QualitySettings.shadowDistance = 20;
				QualitySettings.shadowCascades = 1;
				break;
			case 2:
				QualitySettings.shadowDistance = 40;
				QualitySettings.shadowCascades = 2;
				break;
			case 3:
				QualitySettings.shadowDistance = 150;
				QualitySettings.shadowCascades = 4;
				break;
			default:
				break;
			}
		}

		public void LoadCurrentShadowQuality ()
		{
			if(QualitySettings.shadowCascades <= 0)
				ShadowQualityIdx = 0;
			else if(QualitySettings.shadowCascades <= 1)
				ShadowQualityIdx = 1;
			else if(QualitySettings.shadowCascades <= 2)
				ShadowQualityIdx = 2;
			else if(QualitySettings.shadowCascades <= 4)
				ShadowQualityIdx = 3;
		}
		
		public static readonly string[] AnisotropicTexturesNames = new string[] {
			"Off",
			"On"
		};
		public string AnisotropicTextures {
			get {
				return AnisotropicTexturesNames[AnisotropicTexturesIdx];
			}
		}
		private int AnisotropicTexturesIdx = 1;
		public void SwitchAnisotropicTextures(){
			AnisotropicTexturesIdx = 
				(AnisotropicTexturesIdx+1) % AnisotropicTexturesNames.Length;
		}

		public void ApplyAnisotropic ()
		{
			if(AnisotropicTexturesIdx == 0)
				QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
			else 
				QualitySettings.anisotropicFiltering = AnisotropicFiltering.ForceEnable;
		}

		public void LoadCurrentAnisotropic ()
		{
			if(QualitySettings.anisotropicFiltering == AnisotropicFiltering.Disable)
				AnisotropicTexturesIdx = 0;
			else
				AnisotropicTexturesIdx = 1;
		}
		
		public static readonly string[] ParticlesDetailNames = new string[] {
			"Low",
			"Medium",
			"High"
		};
		public string ParticlesDetail {
			get {
				return ParticlesDetailNames[ParticlesDetailIdx];
			}
		}
		private int ParticlesDetailIdx = 2;
		public void SwitchParticlesDetail(){
			ParticlesDetailIdx = 
				(ParticlesDetailIdx+1) % ParticlesDetailNames.Length;
		}
		public void LoadCurrentParticleDetail ()
		{
			if(QualitySettings.particleRaycastBudget <= 16)
				ParticlesDetailIdx = 0;
			else if(QualitySettings.particleRaycastBudget <= 256)
				ParticlesDetailIdx = 1;
			else
				ParticlesDetailIdx = 2;
		}
		public void ApplyParticleDetail ()
		{
			switch(ParticlesDetailIdx)
			{
			case 0:
				QualitySettings.particleRaycastBudget = 16;
				break;
			case 1:
				QualitySettings.particleRaycastBudget = 256;
				break;
			case 2:
				QualitySettings.particleRaycastBudget = 4096;
				break;
			default:
				break;
			}	
		}
		
		public static readonly string[] LoDBiasNames = new string[] {
			"Low",
			"Medium",
			"High"
		};
		public string LoDBias {
			get {
				return LoDBiasNames[LoDBiasIdx];
			}
		}
		private int LoDBiasIdx = 2;
		public void SwitchLoDBias(){
			LoDBiasIdx = 
				(LoDBiasIdx+1) % LoDBiasNames.Length;
		}

		public void LoadCurrentLoDBias ()
		{
			if(QualitySettings.lodBias <= 0.4f)
				LoDBiasIdx = 0;
			else if(LoDBiasIdx <= 1.0)
				LoDBiasIdx = 1;
			else
				LoDBiasIdx = 2;
		}

		public void ApplyLoDBias ()
		{
			switch(LoDBiasIdx){
			case 0:
				QualitySettings.lodBias = 0.4f;
				break;
			case 1:
				QualitySettings.lodBias = 1.0f;
				break;
			case 2:
				QualitySettings.lodBias = 2.0f;
				break;
			}
		}
		
		public static readonly string[] BlendWeightsLevelNames = new string[] {
			"Low",
			"Medium",
			"High"
		};
		public string BlendWeightsLevel {
			get {
				return BlendWeightsLevelNames[BlendWeightsLevelIdx];
			}
		}
		private int BlendWeightsLevelIdx = 2;
		public void SwitchBlendWeightsLevel(){
			BlendWeightsLevelIdx = 
				(BlendWeightsLevelIdx+1) % BlendWeightsLevelNames.Length;
		}
		public void LoadCurrentBlendWeights ()
		{
			switch(QualitySettings.blendWeights){
			case BlendWeights.OneBone:
				BlendWeightsLevelIdx = 0;
				break;
			case BlendWeights.TwoBones:
				BlendWeightsLevelIdx = 1;
				break;
			case BlendWeights.FourBones:
				BlendWeightsLevelIdx = 2;
				break;
			}
		}

		public void ApplyBlendWeights ()
		{
			switch(BlendWeightsLevelIdx){
			case 0:
				QualitySettings.blendWeights = BlendWeights.OneBone;
				break;
			case 1:
				QualitySettings.blendWeights = BlendWeights.TwoBones;
				break;
			case 2:
				QualitySettings.blendWeights = BlendWeights.FourBones;
				break;
			}
		}
		
		
		#endregion

		public void Apply ()
		{
			if(VSync)
				QualitySettings.vSyncCount = 0;
			else 
				QualitySettings.vSyncCount = 1;
			
			if(currentResolutionIdx != originalScreenResolutionIdx ||
				OriginalFullScreenVal != FullScreen){
				Screen.SetResolution(
					ScreenResolution.width,
					ScreenResolution.height,
					FullScreen,
					ScreenResolution.refreshRate
					);
			}
			
			ApplyQualityLevel();
			ApplyAAValue();
			ApplyShadowQuality();
			ApplyAnisotropic();
			ApplyParticleDetail();
			ApplyLoDBias();
			ApplyBlendWeights();
		}

		public void SetBrightness (float originalBrightness)
		{
			brightnessPostEffect.Brightness = Brightness;
		}
		
		
		public void Discard(){
			Brightness = OriginalBrightness;
			SetBrightness(OriginalBrightness);
		}
		
		public OptionsBag (Camera target)
		{
			this.target = target;
			
			if(Screen.fullScreen){
				for(var i = 0; i < Screen.resolutions.Length; ++i){
					if(Equals(Screen.resolutions[i], Screen.currentResolution)){
						currentResolutionIdx = i;
						originalScreenResolutionIdx = currentResolutionIdx;
						break;
					}
				}
			}
			
			VSync = QualitySettings.vSyncCount > 0;
			FullScreen = Screen.fullScreen;
			OriginalFullScreenVal = Screen.fullScreen;
			OriginalBrightness = brightnessPostEffect.Brightness;
			Brightness = OriginalBrightness;
			
			LoadCurrentGraphicsSettings();
			LoadCurrentAA();
			LoadCurrentShadowQuality();
			LoadCurrentAnisotropic();
			LoadCurrentParticleDetail();
			LoadCurrentLoDBias();
			LoadCurrentBlendWeights();
		}
		
		private static bool Equals(Resolution res, Resolution res2)
        {
            return res.width == res2.width 
				&& res.height == res2.height
				&& res.refreshRate == res2.refreshRate;
        }
	}
}

