using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class MainMenu : MonoBehaviour {
	public Texture2D LogoTexture;
	
	private const int height = 320;
	private const int width = 550;
	private const int buttonWidth = 180;
	private const int buttonHeight = 40;
	private const int logoHeight = 133;
	private Rect menuGroup;
	private readonly static Rect backgroundBox = new Rect(0, 0, width,height);
	private readonly static Rect logoTextureRect = new Rect(15, 10, width, logoHeight);
	private readonly static Rect startGameRect = new Rect(
		width/2 - buttonWidth / 2,
		height-3*(buttonHeight+10),
		buttonWidth,
		buttonHeight
		);
	private readonly static Rect optionsRect = new Rect(
		width/2 - buttonWidth / 2,
		height-2*(buttonHeight+10),
		buttonWidth,
		buttonHeight
		);
	private readonly static Rect quitGameRect = new Rect(
		width/2 - buttonWidth / 2,
		height-1*(buttonHeight+10),
		buttonWidth,
		buttonHeight); 

	// Use this for initialization
	void render () {
		menuGroup = new Rect((Screen.width-width) / 2, (Screen.height-height)/ 2, width, height);
		
	    //layout start
	    GUI.BeginGroup(menuGroup);
	   
	    //the menu background box
	    GUI.Box(backgroundBox, "");
	   
	    //logo picture
	    GUI.Label(logoTextureRect, LogoTexture);
	   
	    ///////main menu buttons
	    //game start button
	    if(GUI.Button(startGameRect, "Start game")) {
			Debug.Log("Start Game");
	    }
		
		if(GUI.Button(quitGameRect, "Quit")) {
	    	Debug.Log("Quit Game");
			Application.Quit();
	    }
		
	    //quit button
	    if(GUI.Button(optionsRect, "Options")) {
	    	Debug.Log("Options Game");
			var optionsMenu = (OptionsMenu) this.GetComponent("OptionsMenu");
			optionsMenu.enabled = true;
			optionsMenu.Options = new OptionsBag(this.camera);
			this.enabled = false;
	    }
	   
	    //layout end
	    GUI.EndGroup();
	}
	
	void OnGUI() {
    	render();
	}
}
