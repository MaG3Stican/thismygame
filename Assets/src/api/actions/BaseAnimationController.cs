using UnityEngine;
using System.Collections;

public abstract class BaseAnimationController: MonoBehaviour
{
	public float duration = 1f;

	protected float startTime;

	protected float t {
		get {
			return (Time.time - startTime) / duration;
		}
	}

	protected float Eval(AnimationCurve curve){
		return curve.Evaluate(t);
	}

	virtual protected void OnEnd()
	{
	}

	virtual protected void Start()
	{
		startTime = Time.time;
	}

	abstract protected void OnUpdate();

	void Update()
	{
		if(t >= 1f)
			OnEnd();
		OnUpdate();
	}
}


