using UnityEngine;
using System.Collections;
using AssemblyCSharp;

public class PointClickThrowFireball : MonoBehaviour
{
    public Collider groundPlane;
    public GameObject fireballPrefab;
    public GameObject flameCirclePrefab;
    public GameObject fireballCirclePrefab;
    public GameObject flameSpiralPrefab;

    public float initialDistance = 6.5f;
    public float initialHeight = 3.0f;
    public float slope = 0.25f;
    public float fireballCooldown = 0.5f;
    public float fireballDelay = 0.3f;
    private bool inFireballCooldown = false;
    //private ThrowEffect throwEffect; 

    // Use this for initialization
    void Start()
    {
        //throwEffect = new FireballCircleEffect(flameCirclePrefab, fireballCirclePrefab);
        //throwEffect = new FlameSpiralEffect (flameSpiralPrefab);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 target;
        if (MustThrowFireball(out target))
        {
            WaitForCoolDown();
            //throwEffect.run(this);
            // look at thrown point
            transform.LookAt(target + Vector3.up * transform.position.y);
            StartCoroutine(WaitAndThrowFireball(target));
        }
    }

    // return true if player clicked in and can throw a fireball
    private bool MustThrowFireball(out Vector3 target)
    {
        target = Vector3.zero;

        if (inFireballCooldown)
            return false; // in cooldown
        else if (Camera.main == null)
            return false; // no camera for seeing transform direction
        else if (Input.GetMouseButtonDown(1) == false)
            return false; // no movement requested by user
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;
        if (groundPlane.Raycast(ray, out hitInfo, Mathf.Infinity) == false)
            return false; // no point throw fireball
        else
        {
            target = hitInfo.point;
            return true;
        }
    }

    private IEnumerator WaitAndThrowFireball(Vector3 target)
    {
        yield return new WaitForSeconds(fireballDelay);
        throwFireball(target);
    }

    private IEnumerator WaitForCoolDown()
    {
        yield return new WaitForSeconds(fireballCooldown + fireballDelay);
        inFireballCooldown = false;
    }

    void throwFireball(Vector3 target)
    {
        inFireballCooldown = true;
        StartCoroutine(WaitForCoolDown());

        // create forward fireball
        var origin = transform.position;
        origin.y += initialHeight;
        origin = new Ray(origin, target - origin)
    .GetPoint(initialDistance);
        var controlPoint = Vector3.Distance(target, origin) * slope *
                (transform.up + Vector3.Normalize(target - origin));
        CreateFireball(origin, target, controlPoint, 0.0f, 0.0f);

        // create lateral fireballs
        CreateFireball(origin, target, controlPoint, -60.0f, +45.0f);
        CreateFireball(origin, target, controlPoint, +60.0f, -45.0f);
    }

    GameObject CreateFireball(Vector3 origin, Vector3 target, Vector3 controlPoint, float rotateControlPoint, float rotateOrigin)
    {

        controlPoint = Quaternion.AngleAxis(rotateControlPoint, transform.forward) * controlPoint;
        var prefab = Instantiate(fireballPrefab, origin, Quaternion.identity)
    as GameObject;
        prefab.transform.RotateAround(transform.position, transform.up, rotateOrigin);
        //var controller = prefab.GetComponent<FireballController>();
        //controller.destination = target;
        //controller.controlPoint = controlPoint;
        //controller.ResetBezierCurve();
        return prefab;
    }
}
