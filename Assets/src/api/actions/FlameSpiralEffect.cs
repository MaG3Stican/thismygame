//using UnityEngine;
//using System.Collections;

//namespace AssemblyCSharp
//{
//    public class FlameSpiralEffect : ThrowEffect
//    {
//        private readonly GameObject prefab;
//        public readonly int spirals = 5;

//        public FlameSpiralEffect(GameObject flameSpiralPrefab){
//            prefab = flameSpiralPrefab;
//        }

//        public void run (MonoBehaviour script)
//        {
//            var r = Random.Range(0, 360 / spirals);
//            for(var i = 1; i <= spirals; ++i)
//                CreateSpiral(script.transform, i * 360f / spirals + r);
//        }

//        private void CreateSpiral(Transform target, float angle)
//        {
//            var prefab = MonoBehaviour.Instantiate(this.prefab,
//                                                   target.position + target.forward,
//                                                   target.rotation) as GameObject;
//            prefab.transform.RotateAround(target.position, target.up, angle);

//            var controller = prefab.GetComponent<FireballSpiralController>();
//            controller.target = target;
//        }
//    }
//}

