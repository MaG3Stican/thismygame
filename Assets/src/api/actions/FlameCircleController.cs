﻿using UnityEngine;
using System.Collections;

public class FlameCircleController : BaseAnimationController
{
    public AnimationCurve emissionRateCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    private float maxEmissionRate;
    public AnimationCurve particleSpeedCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
    private float maxParticleSpeed;

    public float height = 0.5f;
    public Transform target;

    // Use this for initialization
    //void Start()
    //{
    //    base.Start();
    //    maxEmissionRate = particleSystem.emissionRate;
    //    maxParticleSpeed = particleSystem.startSpeed;
    //}

    //protected override System.Void OnEnd ()
    //{
    //    particleSystem.enableEmission = false;
    //    if(particleSystem.particleCount == 0){
    //        Destroy(gameObject);
    //    }
    //}

    // Update is called once per frame
    protected override void OnUpdate()
    {
        particleSystem.emissionRate = maxEmissionRate * Eval(emissionRateCurve);
        particleSystem.startSpeed = maxParticleSpeed * Eval(particleSpeedCurve);

        transform.position = target.position + target.up * height;
        transform.rotation = target.rotation;
        transform.Rotate(transform.right, -90, Space.World);
    }
}
