using UnityEngine;
using System.Collections;

public class PointClickCharacterController : MonoBehaviour {
	public Collider groundPlane;	

	public float movementSpeed = 4.0f;
	public float rotationSpeed = 4.0f;
	public float minMovementDistance = 4.0f;
	private Vector3 destination;

	void Start(){
		destination = transform.position;
	}
		
	void AdjustTransform(){
		if(Vector3.Distance(transform.position, destination) < minMovementDistance)
			return;
		transform.rotation = Quaternion.Lerp(
			transform.rotation,
			Quaternion.LookRotation(destination - transform.position),
			Time.deltaTime * rotationSpeed);		
		transform.position = Vector3.Lerp(
			transform.position,
			destination,
			movementSpeed * Time.deltaTime / Vector3.Distance(transform.position, destination)
		);
		Debug.DrawLine(transform.position, destination, Color.green);
	}
	
	// Update is called once per frame
	void Update () {
		if(Camera.main != null && Input.GetMouseButton(0)){
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if (groundPlane.Raycast(ray, out hitInfo, Mathf.Infinity)){
				destination = hitInfo.point;
				destination.y = transform.position.y;
			}
		}

		AdjustTransform();
	}
}
