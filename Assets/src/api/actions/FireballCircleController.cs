﻿using UnityEngine;
using System.Collections;

public class FireballCircleController : MonoBehaviour {
	public Transform center;
	public float height = 0f;
	public float radius = 3f;
	public float maxAngularSpeed = 5f;
	public AnimationCurve angularSpeed = AnimationCurve.Linear(0f, 1f, 1f, 1f);
	public float duration = 0.2f;
	public AnimationCurve emissionRateCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);
	public float maxGravity = -5f;
	public AnimationCurve gravityCurve = AnimationCurve.Linear(0f,0f,1f,1f);


	private float startTime;
	private float startEmissionRate;

	public float currentAngle {
		get{
			return Mathf.Rad2Deg * currentAngleRad;
		}
		set {
			currentAngleRad = Mathf.Deg2Rad * value;
		}
	}

	// fireball angle in rad
	private float currentAngleRad = 0f;

	// Use this for initialization
	void Start () {
		startTime = Time.time;
		startEmissionRate = particleSystem.emissionRate;
	}
	
	// Update is called once per frame
	void Update () {
		var t = (Time.time - startTime) / duration;

		if(t > 1.0f && particleSystem.particleCount == 0){
			Destroy(gameObject);
			return;
		} else if(t > 1.0f){
			particleSystem.enableEmission = false;
		}

		particleSystem.gravityModifier = gravityCurve.Evaluate(t) * maxGravity;

		currentAngleRad += angularSpeed.Evaluate(t) * maxAngularSpeed * Time.deltaTime;
		particleSystem.emissionRate = emissionRateCurve.Evaluate(t) * startEmissionRate;

		// move fireball in a circle
		var forward = Vector3.forward;
		var right = Vector3.right;
		var position = forward * radius * Mathf.Sin(currentAngleRad) +
				center.up * height +
				right * radius * Mathf.Cos(currentAngleRad);
		var slope = 
			forward * radius * Mathf.Cos(currentAngleRad) -
			right * radius * Mathf.Sin(currentAngleRad);
		transform.position = position + center.position;
		transform.LookAt(transform.position - slope);
	}
}
