﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public class CField : CGameObject
{
    public CField()
    {

    }

    public CField(CVector2D aPosition, int aMass)
    {
        setX(aPosition.x);
        setY(aPosition.y);
        setMass(aMass);
    }
}
