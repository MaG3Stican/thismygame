﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


public enum EParticleRefreshRate
{
    SLOW,
    MEDIUM,
    NORMAL,
    FAST,
}

public struct SParticleSystem
{
    public List<CEmitter> mEmitters;
    public List<CParticle> mParticles;
    public List<CField> mGravityFields;
    public EParticleRefreshRate mRefreshRate;
    public int mMaxParticles;
}

public class CParticleService
{
    int mTimeState = 0;
    int maxParticles = 20000,
      particleSize = 1,
      emissionRate = 20,
      objectSize = 3;
    bool mDynamicSystem = false;
    //all the emitters affect all the particles 
    Dictionary<EParticleTypes, SParticleSystem> mParticleUnits = new Dictionary<EParticleTypes, SParticleSystem>();



    public CParticleService()
    {

    }


    public SParticleSystem addParticleSystem(EParticleTypes aType, EParticleRefreshRate aRefreshRate, int aMaxParticles)
    {
        if (!mParticleUnits.ContainsKey(aType))
        {
            var system = new SParticleSystem() { mMaxParticles = aMaxParticles, mRefreshRate = aRefreshRate, mEmitters = new List<CEmitter>(), mGravityFields = new List<CField>(), mParticles = new List<CParticle>() };
            mParticleUnits.Add(aType, system);
            return system;
        }
        else
        {
            throw new Exception("You wanted to add the same system twice");
        }
    }


    public void initDummy()
    {

        var system = addParticleSystem(EParticleTypes.Dummy, EParticleRefreshRate.FAST, 40);

        var pink = CHelper.loadSpriteToGameObject(null, CAssets.PINK_PARTICLE, 0, false,false);
        var red = CHelper.loadSpriteToGameObject(null, CAssets.RED_PARTICLE, 0, false, false);
        red.name = "PARTICLE SYSTEM - RED";
        pink.name = "PARTICLE SYSTEM - PINK";
        
        // Add one emitter located at `{ x : 100, y : 230}` from the origin (top left)
        // that emits at a velocity of `2` shooting out from the right (angle `0`)
        system.mEmitters.Add(new CEmitter(new CVector2D(CCamera.inst().getPos().x + 150, CCamera.inst().getPos().y + 186), CVector2D.fromAngle(6, 2), Math.PI, CLayer.getLayer(ELayer.Particles), EParticleTypes.Dummy, red));

        system.mEmitters.Add(new CEmitter(new CVector2D(CCamera.inst().getPos().x + 150, CCamera.inst().getPos().y + 180), CVector2D.fromAngle(6, 2), Math.PI, CLayer.getLayer(ELayer.Particles), EParticleTypes.Dummy, pink));

        // Add one field located at `{ x : 400, y : 230}` (to the right of our emitter)
        // that repels with a force of `140`
        system.mGravityFields.Add(new CField(new CVector2D(CCamera.inst().getPos().x + 300, CCamera.inst().getPos().y + 300), 900));

        system.mGravityFields.Add(new CField(new CVector2D(CCamera.inst().getPos().x + 200, CCamera.inst().getPos().y + 250), -50));
    }


    void plotParticles(EParticleTypes aParticleUnit, double boundsX, double boundsY)
    {
        List<CParticle> updatedParticles = new List<CParticle>();
        var currentParticles = mParticleUnits[aParticleUnit].mParticles;
        var currentFields = mParticleUnits[aParticleUnit].mGravityFields;

        for (var i = 0; i < currentParticles.Count; i++)
        {
            var particle = currentParticles[i];
            var pos = particle.getPos();
            //if the particle is out of the range stop updating it, we will remove the reference and it will be destroyed
            if (pos.x < 0 || pos.x > boundsX || pos.y < 0 || pos.y > boundsY) continue;
            // Update velocities and accelerations to account for the fields
            particle.submitToFields(currentFields);

          
            updatedParticles.Add(particle);
        }

        currentParticles = updatedParticles;
    }



    internal virtual void update()
    {

        for (int index = 0; index < mParticleUnits.Count; index++)
        {
            var item = mParticleUnits.ElementAt(index);

            if (mTimeState % calculateRefresh(item.Value.mRefreshRate) == 0)
            {
                //update emitters
                for (int index2 = 0; index2 < item.Value.mEmitters.Count; index2++)
                {
                    if (item.Value.mMaxParticles > item.Value.mParticles.Count)
                        item.Value.mParticles.Add(item.Value.mEmitters[index2].emitParticle());

                    item.Value.mEmitters[index2].update();
                }
                //update gravity fields
                for (int index2 = 0; index2 < item.Value.mGravityFields.Count; index2++)
                {
                    item.Value.mGravityFields[index2].update();
                }

                //update particles
                for (int index2 = 0; index2 < item.Value.mParticles.Count; index2++)
                {
                    item.Value.mParticles[index2].update();
                }
            }
        }
        mTimeState++;
    }

    internal virtual void render()
    {
        for (int index = 0; index < mParticleUnits.Count; index++)
        {

            var item = mParticleUnits.ElementAt(index);


            if (mTimeState % calculateRefresh(item.Value.mRefreshRate) == 0)
            {
                //render particles
                for (int index2 = 0; index2 < item.Value.mParticles.Count; index2++)
                {

                    plotParticles(item.Key, CMap.inst().getWorldWidthInPixels(), CMap.inst().getWorldHeightInPixels());
                    item.Value.mParticles[index2].render();
                }
                //render emitters
                for (int index2 = 0; index2 < item.Value.mEmitters.Count; index2++)
                {
                    item.Value.mEmitters[index2].render();
                }
                //render gravity fields
                for (int index2 = 0; index2 < item.Value.mGravityFields.Count; index2++)
                {
                    item.Value.mGravityFields[index2].render();
                }
            }
        }

    }

    private int calculateRefresh(EParticleRefreshRate aRate)
    {
        if (aRate == EParticleRefreshRate.SLOW)
            return 30;

        if (aRate == EParticleRefreshRate.MEDIUM)
            return 20;

        if (aRate == EParticleRefreshRate.NORMAL)
            return 5;

        if (aRate == EParticleRefreshRate.FAST)
            return 2;

        return 1;
    }


}