using System;
using System.Collections.Generic;
using System.Linq;

public class CParticleManager : CParticleService
{

    private int mTimeState = 0;
    // Singleton.
    private static CParticleManager mInst = null;


    public CParticleManager()
    {
        registerSingleton();
    }

    public static CParticleManager inst()
    {
        return mInst;
    }


    private void registerSingleton()
    {
        if (mInst == null)
        {
            mInst = this;
        }
        else
        {
            throw new Exception("Cannot create another instance of Singleton CItemManager.");
        }
    }


    internal override void update()
    {
        base.update();
        mTimeState++;
    }




    //public CGameObject collides(CEmitter aEmitter, EParticleTypes aItem)
    //{
    //    return mEmitters[aItem].collides(aEmitter);
    //}


    ////if nothing is specified, removes first object of array
    //private void removeItem(EParticleTypes aItem, CGenericGraphic aObject = null)
    //{
    //    mEmitters[aItem].remove(aObject);

    //}


    internal override void render()
    {
        base.render();
        //for (int index = 0; index < mEmitters.Count; index++)
        //{
        //    var item = mEmitters.ElementAt(index);
        //    item.Value.render();
        //}
        //activateNearbyGraphics();

    }

    public void destroy()
    {
        if (mInst != null)
        {
            //for (int index = 0; index < mEmitters.Count; index++)
            //{
            //    var item = mEmitters.ElementAt(index);
            //    foreach (var particle in item.Value)
            //    {
            //        particle.destroy();
            //    }
            //}
        }
    }


    //public void activateNearbyGraphics()
    //{
    //    for (int index = 0; index < mEmitters.Count; index++)
    //    {
    //        var item = mEmitters.ElementAt(index);
    //        item.Value.checkIfRenderNeedBe();
    //    }
    //}


    //internal void updateRenderOnce(EParticleTypes aParticles)
    //{
    //    var item = mEmitters[aParticles];
    //    item.update();
    //    item.render();
    //    mEmitters[aParticles] = new CVectorExt();
    //}
}

