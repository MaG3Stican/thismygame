
//public class CParticle : CGameObject 
//{
//    //States
//    private const uint NORMAL = 0;
//    private const uint DYING = 1;

//    private int mState;
//    private int mTimeState = 0;

//    private const uint GRAVITY = 1;
//    private CRect mMC;
//    private int mTimeToLive;
//    private int mAlpha = 1;

//    public  CParticle(int aColor,CLayer aLayer,int aMinTime,int aMaxTime) 
//    {
//        mMC = new CRect(4, 4, aColor);
//        mTimeToLive = CMath.randInt(aMinTime, aMaxTime);
//        setVelX(CMath.randInt(5, 10));
//        getVel().setAngle(CMath.deg2rad(CMath.randInt(0, 359)));
//        //setVelZ(CMath.randInt(0, 10));
//        setAccelZ( -GRAVITY);
//        setBounds( -CMath.INFINITY, -CMath.INFINITY, CMath.INFINITY, CMath.INFINITY, 0, CMath.INFINITY);
//        setBoundAction(CGameObject.STOP);
//        setState(DYING);
//    }

//    private function setState(aState:int):void
//    {
//        mTimeState = 0;
//        mState = aState;
//    }

//    override public function update():void 
//    {
//        super.update();
//        if (mState == NORMAL)
//        {
//            if (mTimeState > mTimeToLive)
//            {
//                setState(DYING);
//                return;
//            }
//        }
//        else if (mState == DYING)
//        {
//            mAlpha -= 1 / mTimeToLive;
//            if (mAlpha <= 0)
//                setDead(true);
//        }
//        mTimeState++;
//    }

//    override public function render():void 
//    {
//        mMC.x = getX() - CGame.inst().getCamera().getX();
//        mMC.y = getY() - getZ() - CGame.inst().getCamera().getY();
//        mMC.alpha = mAlpha;
//    }

//    override public function destroy():void 
//    {
//        CHelper.removeDisplayObject(mMC);
//        mMC = null;
//    }
//}