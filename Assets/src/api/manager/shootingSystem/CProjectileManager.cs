
using System;
using System.Collections.Generic;
using System.Linq;

public class CProjectileManager
{
    // Singleton.
    private static CProjectileManager mInst = null;

    // List of Items to be managed.
    private Dictionary<EProjectiles, CVectorExt> mProjectiles = new Dictionary<EProjectiles, CVectorExt>();

    public CProjectileManager()
    {
        registerSingleton();
    }

    public static CProjectileManager inst()
    {
        return mInst;
    }

    private void registerSingleton()
    {
        if (mInst == null)
        {
            mInst = this;
        }
        else
        {
            throw new Exception("Cannot create another instance of Singleton CItemManager.");
        }
    }

    public int getCount(EProjectiles aItem)
    {
        return mProjectiles[aItem].arr.Count;
    }

    public void update()
    {
        for (int index = 0; index < mProjectiles.Count; index++)
        {
            var item = mProjectiles.ElementAt(index);
            item.Value.update();
        }
    }

    public CGameObject collides(CGameObject aGameObject, EProjectiles aItem)
    {
        return mProjectiles[aItem].collides(aGameObject);

    }

    public void flush(EProjectiles aItem)
    {
        mProjectiles[aItem].destroy();
    }

    //if nothing is specified, removes first object of array
    private void removeProjectile(EProjectiles aItem, CGenericGraphic aObject = null)
    {
        mProjectiles[aItem].remove(aObject);

    }

    public void render()
    {
        for (int index = 0; index < mProjectiles.Count; index++)
        {
            var item = mProjectiles.ElementAt(index);
            item.Value.render();
        }
    }

    public void addProjectile(CGenericGraphic aObject, EProjectiles aProjectile)
    {
        if (mProjectiles.ContainsKey(aProjectile))
        {
            mProjectiles[aProjectile].append(aObject);
        }
        else
        {
            mProjectiles.Add(aProjectile, new CVectorExt());
            mProjectiles[aProjectile].append(aObject);
        }
    }

    public void destroy()
    {
        if (mInst != null)
        {
            for (int index = 0; index < mProjectiles.Count; index++)
            {
                var item = mProjectiles.ElementAt(index);
                item.Value.destroy();
            }
        }
    }


    public void activateNearbyGraphics(CGameObject aGameObject, int aRangeBlackHoles, int aRangeRendering)
    {
        //mMinions.checkIfRenderNeedBe(aUser, aRangeRendering);
        //
        //mBuildings.checkIfRenderNeedBe(aUser, aRangeRendering);
    }

}

