
using UnityEngine;
public class CGenericProjectile : CGenericGraphic
{
    public CGenericProjectile(GameObject graphic, CVector3D initPosition, int angle, int magnitude, CLayer aLayer, uint aRadious)
    {

        setShape(EShapes.CIRCLE);
        setRadious(aRadious);
        setPos(initPosition);
        var vel = new CVector3D(1, 1, 1);
        vel.setAngleMag(angle, magnitude);
        vel.z = 0;
        setVel(vel);


        setLayer(aLayer);

        setBounds(CVars.inst().MinBounds(), CVars.inst().MaxBounds());

        setGraphic(graphic);

        CProjectileManager.inst().addProjectile(this, EProjectiles.Bullet);

        setBoundAction(EBoundBehaviour.WRAP);


    }

    override public void update()
    {
        base.update();
    }

    override public void render()
    {
        base.render();
    }

    override public void destroy()
    {
        base.destroy();
    }

}