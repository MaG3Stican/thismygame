﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CEmitter : CGenericGraphic
{
    GameObject mParticleGraphic;
    private double mSpread = 0;
    public CEmitter(CVector2D aPosition, CVector2D aVelocity, double aSpread, CLayer aLayer, EParticleTypes aTypeOfParticle, GameObject aParticleGraphic)
    {
        setX(aPosition.x);
        setY(aPosition.y);
        setVelX(aVelocity.x);
        setVelY(aVelocity.y);
        mSpread = aSpread != 0 ? aSpread : Math.PI / 32; // possible angles = velocity +/- spread
        setLayer(aLayer);

        mParticleGraphic = aParticleGraphic;
    }

    public CParticle emitParticle()
    {
        // Use a randomized angle over the spread so we have more of a "spray"
        var angle = getVel().getAngle() + mSpread - (CMath.randFloat() * mSpread * 2);

        // The magnitude of the emitter's velocity
        var magnitude = getVel().magnitude();

        // The emitter's position
        var position = new CVector2D(getX(), getY());

        // New velocity based off of the calculated angle and magnitude
        var velocity = CVector2D.fromAngle(angle, magnitude);

        // return our new Particle!
        return new CParticle(position, velocity, getLayer(), (GameObject)CHelper.Instantiate(mParticleGraphic), CCamera.inst().getMinBoounds(), CCamera.inst().getMaxBounds());
    }


}
