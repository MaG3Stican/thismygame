﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CNetworkManager
{
    NetworkView mNetworkView;

    private static CNetworkManager mInst = null;

    public CNetworkManager()
    {
        if (mInst == null)
        {
            mInst = this;

            mNetworkView.stateSynchronization = NetworkStateSynchronization.Off;

            mNetworkView.observed = null;

            CVars.inst().setNetworkManager(this);
        }

        CNetworkManager result = CVars.inst().getNetworkManager();
    }

}