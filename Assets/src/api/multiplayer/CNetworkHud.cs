﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;



public class CNetworkHud : CHud
{
    public UnityEngine.UI.Button mStartServerBtn;
    public UnityEngine.UI.Button mRefreshBtn;
    public List<UnityEngine.UI.Button> mJoinHostBtn = new List<UnityEngine.UI.Button>();
    private int mLastCount = 0;

    private static CNetworkHud mInst = null;

    public CNetworkHud()
    {
        if (!Network.isClient && !Network.isServer)
        {
            var client = CVars.inst().getMyNetworkClientData();
            var server = CVars.inst().getMyNetworkServerData();

            mStartServerBtn = CVars.inst().getCanvas().addButton(new CVector3D(100, 100), "startServerButton", "Start server", true);

            mStartServerBtn.onClick.AddListener(delegate
                {
                    server.startServer();
                    //if the user wants to start a server, then remove the refresh/ connect to server button
                });

            mRefreshBtn = CVars.inst().getCanvas().addButton(new CVector3D(100, 150), "refreshButton", "Refresh host list", true);

            mRefreshBtn.onClick.AddListener(delegate
            {
                client.refreshHostList();

            });

            doHudRefresh();

            if (mInst == null)
            {
                mInst = this;
            }
            else
            {
                throw new Exception("There already exists an instance of the network hud");
            }
        }
    }

    public static CNetworkHud inst()
    {
        return mInst;
    }


    public void doHudRefresh()
    {
        if (!Network.isServer)
        {
            var client = CVars.inst().getMyNetworkClientData();


            if (client.hostList != null && mLastCount < client.hostList.Length)
            {
                mLastCount = client.hostList.Length;
                //remove buttons
                for (int i = 0; i < mJoinHostBtn.Count; i++)
                {
                    var btnn = mJoinHostBtn[i];
                    btnn.gameObject.SetActive(false);
                    CHelper.Destroy(btnn.gameObject);
                }
                //add them again
                for (int i = 0; i < client.hostList.Length; i++)
                {
                    var btn = CVars.inst().getCanvas().addButton(new CVector3D(100, 200), "serverButton" + i.ToString(), client.hostList[i].gameName, true);
                    //this variable has to be declared here to be encapsulated, cannot be inside of the delegate
                    var ii = i;
                    btn.onClick.AddListener(delegate
                    {
                        client.connectToServer(client.hostList[ii]);
                    });
                    mJoinHostBtn.Add(btn);

                }
            }
        }
    }

}
