﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


enum state
{
    invalid = 0,
    mainmenu = 1,
    joinmenu = 2,
    hostmenu = 3,
    waitserver = 4,
    waitclient = 5,
    failconnect = 6,
    playing = 7
}


public class CNetworkObject : MonoBehaviour
{

    public HostData[] hostList = null;

    internal float nextNetworkUpdateTime = 0.0F;
    internal float networkUpdateIntervalMax = 0.1F; // maximum of 10 updates per second

    internal int GameState = (int)state.waitserver;

    internal Hashtable mClients = new Hashtable();
    internal Dictionary<NetworkViewID, CLightWeightPlayer> mLightWeightClients = new Dictionary<NetworkViewID, CLightWeightPlayer>();

    public CNetworkObject()
        : base()
    {

    }

    internal virtual void init()
    {
        gameObject.AddComponent<NetworkView>();
    }

    internal virtual void update()
    {

    }
    internal virtual void render()
    {

    }



    [RPC]
    void serverUpdate(Vector3 aPlayerPos, NetworkMessageInfo aPlayerInfo)
    {
        //WE TELL OTHER PLAYERS WERE THIS PLAYER IS 
        //this sends an update message to all clients (not to me)
        NetworkPlayer client = aPlayerInfo.sender;
        networkView.RPC("clientUpdate", RPCMode.Others, aPlayerPos, aPlayerInfo.networkView.viewID);

        ServerUpdate(client, aPlayerPos);
    }

    void ServerUpdate(NetworkPlayer client, Vector3 pos)
    {
        // the server is telling us to update a player
        // again, normally you would do a lot of bounds
        // checking here, but this is just a simple example
        if (mClients.ContainsKey(client))
        {
            var viewId = (NetworkViewID)mClients[client];
            var player = mLightWeightClients[viewId];
            //UPDATE THE PLAYER POSITION IN THE SERVER SIDE
            player.pos.setX(pos.x);
            player.pos.setY(pos.y);
            player.pos.setZ(pos.z);
        }
    }


    private string generatePlayerId()
    {
        return Guid.NewGuid().ToString();
    }



    [RPC]
    void ReceiveOtherClientData(NetworkViewID newPlayerView, byte[] aPosition, string aClientNetworkId)
    {
        Vector3 newClientPosition = (Vector3)CHelper.BytesToObj(aPosition);

        if (mClients.ContainsKey(aClientNetworkId))
        {
            var player = (CPlayer)mClients[aClientNetworkId];
            player.setX(newClientPosition.x);
            player.setY(newClientPosition.y);
            player.setZ(newClientPosition.z);
        }
        // the local GameObject for any player is unknown to
        // the server, so it can only send updates for NetworkPlayer
        // IDs - which we need to translate to a player's local
    }


    [RPC]
    void ClientJoined(NetworkViewID aNewClientId, Vector3 aPosition)
    {
        Vector3 newClientPosition = aPosition; //(Vector3)CHelper.BytesToObj(aPosition);

        CPlayer anotherPlayer = new CPlayer(CLayer.getLayer(ELayer.Player));
        //new CMainClient(somePlayer, ENetworkMode.online, "", EClientType.dummy);
        //anotherPlayer.getGraphic().AddComponent<NetworkView>().viewID = newClientId;
        // to do this, we will add the player to the "players" Hashtable
        // for fast reverse-lookups for player updates
        // Hashtable structure is NetworkPlayer --> GameObject
        mClients.Add(aNewClientId, anotherPlayer);
    }

    //the client wont receive updates until a network id was allocated
    [RPC]
    void ServerSentMyId(NetworkViewID aNewClientId)
    {
        networkView.viewID = aNewClientId;
    }

    [RPC]
    void DisconnectPlayer(NetworkViewID aClientId)
    {
        mClients.Remove(aClientId);
    }


    [RPC]
    void SendInfoToClients(NetworkMessageInfo request)
    {
        if (Network.isServer)
        {
            //dont login twice :
            foreach (DictionaryEntry de in mClients)
            {
                NetworkViewID clientId = (NetworkViewID)de.Key;
                NetworkPlayer client = (NetworkPlayer)de.Value;
                // only tell the requestor about others
                // we make this comparison using the
                // server-assigned index number of the 
                // player instead of the ipAddress because
                // more than one player could be playing
                // under one ipAddress -- ToString()
                // returns this player index number
                if (client.guid.ToString() != request.sender.guid.ToString())
                {
                    networkView.RPC("ReceiveOtherClientData", request.sender, clientId, client);
                }
            }
        }
    }

}

