﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;



public enum ENetworkMode
{
    local,
    online
}
public enum EClientType
{
    dummy,
    real
}


public class CMainClient : CNetworkObject
{

    private int mPlayerCount = 0;
    public Vector3 mPos = new Vector3(0, 0, 0);
    public NetworkPlayer netPlayer;

    private string mLocalAddress = "127.0.0.1";
    private string mServerAddress = "127.0.0.1";
    private string mServerName = "";
    static CMainClient mInst = null;



    public CMainClient()
        : base()
    {

    }

    public CMainClient inst()
    {
        return mInst;
    }

    internal new void init(ENetworkMode aMode, string aServerAddress, string aServerName, EClientType aClient)
    {
        if (mInst == null)
        {
            mInst = this;

            if (aMode == ENetworkMode.local)
            {
                mServerAddress = mLocalAddress;
                mServerName = aServerName;
            }
            else if (aMode == ENetworkMode.online)
            {
                mServerAddress = aServerAddress;
                mServerName = aServerAddress;
            }
            CVars.inst().setMyNetworkClientData(this);
        }
        else
        {
            throw new Exception("You are trying to instantiate the server twice");
        }

        base.init();
    }

    public void clientUpdate()
    {
        base.update();

        //SEND OUR DATA TO THE SERVER EVERY SOME TIME
        if (Network.isClient && Time.realtimeSinceStartup > nextNetworkUpdateTime)
        {
            nextNetworkUpdateTime = Time.realtimeSinceStartup + networkUpdateIntervalMax;

            networkView.RPC("serverUpdate", RPCMode.Server, CVars.inst().getPlayer().getPos().toVec3());
        }
    }

    public void OnConnectedToServer()
    {
        Debug.Log("Connected to the server");
    }

    public void refreshHostList()
    {
        MasterServer.RequestHostList(mServerName);
    }

    void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        if (msEvent == MasterServerEvent.HostListReceived)
        {
            hostList = MasterServer.PollHostList();

            CNetworkHud.inst().doHudRefresh();
        }

    }

    public void connectToServer(HostData aServerData)
    {
        var ip = aServerData.ip;
        var port = aServerData.port;

        if (!Network.isServer)
        {
            Network.Connect(ip, port);
            GameState = (int)state.waitserver;
        }
        else
        {
            Network.Connect(mLocalAddress, port);
            GameState = (int)state.waitserver;
        }
    }


    //void OnConnectedToServer()
    //{
    //    // wait until we are fully connected
    //    // to the server before making the 
    //    // player list request
    //    networkView.RPC("serverUpdate", RPCMode.Server);
    //}








}
