﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;


public class CMainServer : CNetworkObject
{
    private string mServerName = "UniqueGameName";
    private string mShardName = "RoomName";

    private int mPlayerCount = 0;
    private static string mAddress = "";
    static CMainServer mInst = null;

    public CMainServer()
        : base()
    {

    }

    public CMainServer inst()
    {
        return mInst;
    }

    //server update doesnt wait for the user
    void Update()
    {

    }


    void OnPlayerConnected(NetworkPlayer aClient)
    {
        if (Network.isServer)
        {
            mPlayerCount++;

            NetworkViewID newViewID = Network.AllocateViewID();


            // tell everyone to create the new player
            foreach (DictionaryEntry de in mClients)
            {
                NetworkPlayer client = (NetworkPlayer)de.Value;
                networkView.RPC("ClientJoined", client, newViewID, Vector3.zero, aClient.guid);
            }

            networkView.RPC("ServerSentMyId", aClient, newViewID);
            //ONLY THE SERVER STORES THE CLIENT DATA
            mClients.Add(aClient, newViewID);
            mLightWeightClients.Add(newViewID, new CLightWeightPlayer());

            Debug.Log("Player " + newViewID.ToString() + " connected from " + aClient.ipAddress + ":" + aClient.port);
            Debug.Log("There are now " + mPlayerCount + " players.");
        }
    }

    void OnPlayerDisconnected(NetworkPlayer aClient)
    {
        if (Network.isServer)
        {
            mPlayerCount--;

            Debug.Log("Player " + aClient.ToString() + " disconnected.");
            Debug.Log("There are now " + mPlayerCount + " players.");

            // we send this to everyone, including to
            // ourself (the server) to clean-up

            if (mClients.ContainsKey(aClient))
            {
                mClients.Remove(aClient);
            }

            networkView.RPC("DisconnectPlayer", RPCMode.Others, aClient);
        }
    }


    private string generatePlayerId()
    {
        return Guid.NewGuid().ToString();
    }


    internal new void init(string aServerName, string aShardName)
    {
        if (mInst == null)
        {
            mInst = this;

            mServerName = aServerName;
            mShardName = aShardName;

            mAddress = CHelper.getLocalIPAddress();

            // gameObject.AddComponent<NetworkView>();

            CVars.inst().setMyNetworkServerData(this);
        }
        else
        {
            throw new Exception("You are trying to instantiate the server twice");
        }

        base.init();
    }


    public void startServer()
    {
        if (!Network.isServer && !Network.isClient)
        {
            Network.InitializeServer(4, 25000, !Network.HavePublicAddress());
            MasterServer.RegisterHost(mServerName, mShardName);
        }
    }


    void OnMasterServerEvent(MasterServerEvent msEvent)
    {
        if (msEvent == MasterServerEvent.RegistrationSucceeded)
            hostList = MasterServer.PollHostList();
    }




    void OnServerInitialized()
    {
        Debug.Log("Server Initializied");
    }
    //void OnServerInitialized()
    //{
    //    Debug.Log("Server Initializied");
    //    SpawnPlayer();
    //}

    //void OnConnectedToServer()
    //{
    //    Debug.Log("Server Joined");
    //    SpawnPlayer();
    //}



    void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info)
    {
        //Vector3 syncPosition = Vector3.zero;
        //if (stream.isWriting)
        //{
        //    syncPosition = rigidbody.position;
        //    stream.Serialize(ref syncPosition);
        //}
        //else
        //{
        //    stream.Serialize(ref syncPosition);
        //    rigidbody.position = syncPosition;
        //}
    }



}
