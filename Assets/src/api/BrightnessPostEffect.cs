using UnityEngine;
using System.Collections;

public class BrightnessPostEffect : MonoBehaviour {
	public float Brightness = 0.0f; 
	
	public Shader RenderShader {
		get {
			if(renderShader == null)
				renderShader = Resources.Load("BrightnessShader") as Shader;
			return renderShader;
		}
	}
	private Shader	 renderShader = null;
	public Material RenderMaterial {
		get {
			if(renderMaterial == null){
				renderMaterial = new Material (RenderShader);
				renderMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return renderMaterial;
		}
	}
	private Material renderMaterial = null;
	
	protected virtual void Start ()
	{
		// Disable if we don't support image effects
		if (!SystemInfo.supportsImageEffects || RenderShader == null) {
			enabled = false;
			return;
		}
	}
	
	protected virtual void OnDisable() {
		/*if( RenderMaterial ) {
			DestroyImmediate( RenderMaterial );
		}*/
	}
	
	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		if(RenderShader == null){
			enabled = false;
			return;
		}
		RenderMaterial.SetFloat ("_Brightness", Brightness);
		Graphics.Blit(source, destination, RenderMaterial);
	}
}
