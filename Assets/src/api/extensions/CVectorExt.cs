
using System;
using System.Collections.Generic;
public class CVectorExt : CGameObject
{
    private double mTime = 0;
    public List<CGenericGraphic> arr;
    public bool RemoveOnceOutOfRange = false;
    private bool inRange = false;
    private List<CGenericGraphic> itemsToRemove;

    public CVectorExt(bool aRemoveOnceOutOfRange = false, List<CGenericGraphic> aVector = null)
    {
        if (aVector == null)
        {
            arr = new List<CGenericGraphic>();
            itemsToRemove = new List<CGenericGraphic>();
        }
        else
        {
            arr = aVector;
        }
        RemoveOnceOutOfRange = aRemoveOnceOutOfRange;
    }

    public int update(bool isTile = false)
    {
        var howManyActive = 0;
        base.update();
        mTime++;
        for (int i = arr.Count - 1; i >= 0; i--)
        {
            if (arr[i].isActive())
            {
                if (arr[i].isDead())
                {
                    arr[i].destroy();
                    arr.RemoveAt(i);
                }
                else
                {
                    if (isTile)
                    {
                        if (((CTile)arr[i]).amIUseful())
                        {
                            howManyActive++;
                            arr[i].update();
                        }
                    }
                    else
                    {

                        howManyActive++;
                        arr[i].update();
                    }
                }
            }
        }
        return howManyActive;
    }

    override public void render()
    {
        base.render();
        for (int i = 0; arr.Count > i; i++)
        {
            if (arr[i].isActive())
            {
                arr[i].render();
            }
        }
    }

    //check automatically for collisions, objects must have setted their  shape, run update for all of the arr childs
    public void updateWithCollision(CGameObject collisionObject, EMessage aMessageForFirstObject, EMessage aMessageForSecondObject)
    {
        if (collisionObject is CVectorExt)
        {
            CVectorExt converted = collisionObject as CVectorExt;

            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].isActive())
                {
                    int subCount = 0;
                    while (subCount < converted.arr.Count)
                    {
                        //update this current object given its active
                        arr[i].update();
                        if (converted.arr[subCount].isActive())
                        {
                            if (converted.arr[subCount].getShapeType() == EShapes.CIRCLE && this.getShapeType() == EShapes.CIRCLE)
                            {
                                if (CMath.circleCircleCollision(arr[i].getCircle(), converted.arr[subCount].getCircle()))
                                {
                                    converted.arr[subCount].sendMessage(aMessageForSecondObject);
                                    arr[i].sendMessage(aMessageForFirstObject);
                                }
                            }
                            else if (converted.arr[subCount].getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.CIRCLE)
                            {
                                if (CMath.rectCircCollision(arr[i].getCircle(), converted.arr[subCount].getRectangle()))
                                {
                                    converted.arr[i].sendMessage(aMessageForSecondObject);
                                    arr[i].sendMessage(aMessageForFirstObject);
                                }
                            }
                            else if (converted.arr[subCount].getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.RECT)
                            {
                                if (CMath.rectRectCollision(converted.arr[subCount].getRectangle(), arr[i].getRectangle()))
                                {
                                    converted.arr[i].sendMessage(aMessageForSecondObject);
                                    arr[i].sendMessage(aMessageForFirstObject);
                                }
                            }
                        }
                        subCount++;
                    }
                }
            }
        }
        else
        {
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].isActive() && collisionObject.isActive())
                {
                    //update this current object given its active 
                    arr[i].update();

                    if (collisionObject.getShapeType() == EShapes.CIRCLE && this.getShapeType() == EShapes.CIRCLE)
                    {
                        if (CMath.circleCircleCollision(arr[i].getCircle(), collisionObject.getCircle()))
                        {
                            collisionObject.sendMessage(aMessageForSecondObject);
                            arr[i].sendMessage(aMessageForFirstObject);
                        }
                    }
                    else if (collisionObject.getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.CIRCLE)
                    {
                        if (CMath.rectCircCollision(arr[i].getCircle(), collisionObject.getRectangle()))
                        {
                            collisionObject.sendMessage(aMessageForSecondObject);
                            arr[i].sendMessage(aMessageForFirstObject);
                        }
                    }

                }
            }
        }
    }

    public void checkCollisions(CGameObject collisionObject, EMessage aMessageForFirstObject, EMessage aMessageForSecondObject)
    {
        for (int i = 0; i < arr.Count; i++)
        {
            if (arr[i].isActive() && collisionObject.isActive())
            {
                if (collisionObject.getShapeType() == EShapes.CIRCLE && this.getShapeType() == EShapes.CIRCLE)
                {
                    if (CMath.circleCircleCollision(collisionObject.getCircle(), arr[i].getCircle()))
                    {
                        collisionObject.sendMessage(aMessageForSecondObject);
                        arr[i].sendMessage(aMessageForFirstObject);
                    }
                }
                else if (collisionObject.getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.CIRCLE)
                {
                    if (CMath.rectCircCollision(arr[i].getCircle(), collisionObject.getRectangle()))
                    {
                        collisionObject.sendMessage(aMessageForSecondObject);
                        arr[i].sendMessage(aMessageForFirstObject);
                    }
                }
                else if (collisionObject.getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.RECT)
                {
                    if (CMath.rectRectCollision(arr[i].getRectangle(), collisionObject.getRectangle()))
                    {
                        collisionObject.sendMessage(aMessageForSecondObject);
                        arr[i].sendMessage(aMessageForFirstObject);
                    }
                }
            }

        }
    }

    public void checkIfRenderNeedBe()
    {
        var recCamera = new CRect(CCamera.inst().getWidth(), CCamera.inst().getHeight(), CCamera.inst().getX() - CCamera.inst().getPivot().x, CCamera.inst().getY() - CCamera.inst().getPivot().y);
        var recObject = new CRect();
        for (int i = 0; i < arr.Count; i++)
        {
            recObject = new CRect((uint)arr[i].getHeight(), (uint)arr[i].getWidth(), arr[i].getX() - (uint)arr[i].getPivot().x, arr[i].getY() - (uint)arr[i].getPivot().y);
            inRange = CMath.rectRectCollision(recCamera, recObject);
            if (inRange)
            {
                if (!arr[i].isActive())
                {
                    arr[i].activate();
                    arr[i].redirect();
                }
            }
            else
            {
                if (arr[i].isActive())
                {
                    arr[i].deactivate();

                    if (RemoveOnceOutOfRange)
                    {
                        remove();
                    }
                }
            }
        }
    }

    public CGenericGraphic collides(CGameObject collisionObject)
    {
        int i = 0;
        bool tEnd = false;
        CGenericGraphic tCollidedObject = null;
        while (i < arr.Count && !tEnd)
        {
            if (collisionObject.getShapeType() == EShapes.CIRCLE && this.getShapeType() == EShapes.CIRCLE)
            {
                // TODO: Corregir colisiones
                if (CMath.circleCircleCollision(arr[i].getCircle(), collisionObject.getCircle()))
                {
                    tCollidedObject = arr[i];
                    tEnd = true;
                }
            }
            else if (collisionObject.getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.CIRCLE)
            {
                if (CMath.rectCircCollision(arr[i].getCircle(), collisionObject.getRectangle()))
                {
                    tCollidedObject = arr[i];
                    tEnd = true;
                }
            }
            else if (collisionObject.getShapeType() == EShapes.RECT && this.getShapeType() == EShapes.RECT)
            {
                if (CMath.rectRectCollision(arr[i].getRectangle(), collisionObject.getRectangle()))
                {
                    tCollidedObject = arr[i];
                    tEnd = true;
                }
            }
            i++;
        }
        return tCollidedObject;
    }

    public CGenericGraphic getClosest(CGameObject aGameObject)
    {
        uint tDistance = (uint)CMath.INFINITY;
        CGenericGraphic closest = null;
        bool isConvertible = false;
        for (int j = arr.Count - 1; j >= 0; j--)
        {
            uint tNewDist = CMath.dist(arr[j].getX(), arr[j].getY(), aGameObject.getX(), aGameObject.getY());

            if (tNewDist < tDistance)
            {
                tDistance = tNewDist;
                closest = arr[j];
            }

        }
        return closest;
    }

    public void remove(CGenericGraphic objectToRemove = null)
    {
        if (objectToRemove != null)
        {
            arr.Remove(objectToRemove);
        }
        else
        {
            if (arr.Count > 0)
            {
                //logic dictates that the object  added is the first to go off the array as it is the first to leave the screen
                arr.RemoveAt(0);
            }
            else
            {
                throw new Exception("There is nothing to remove");
            }
        }
    }

    public void append(CGenericGraphic aGraphic)
    {
        setShape(aGraphic.getShapeType());
        arr.Add(aGraphic);
    }


    public CGenericGraphic getByName(String aName)
    {
        for (int j = arr.Count - 1; j >= 0; j--)
        {
            if (arr[j].getName() == aName)
            {
                return arr[j];
            }
        }
        throw new Exception("There are no objects with that name in the collection");
    }

    public CGenericGraphic getByGuid(double aGuid)
    {
        for (int j = arr.Count - 1; j >= 0; j--)
        {
            if (arr[j].getGuid() == aGuid)
            {
                return arr[j];
            }
        }
        throw new Exception("There are no objects with that name in the collection");
    }
}