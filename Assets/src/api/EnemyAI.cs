using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {
	private Transform target;
	public float rotationSpeed = 0;
	public float moveSpeed = 0;
	
	
	// Use this for initialization
	void Start () {
		target = GameObject.FindWithTag("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		Debug.DrawLine(target.position, target.position, Color.green);
		
		transform.rotation = Quaternion.Slerp(transform.rotation,
			Quaternion.LookRotation(target.position - transform.position), 
			Time.deltaTime * rotationSpeed);
		
		transform.position += transform.forward * moveSpeed * Time.deltaTime;
	}
}
