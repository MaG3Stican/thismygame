﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public struct SHudProperties
{
    public SHudProperties(string aText)
    {
        pos = new CVector3D();
        id = "";
        text = aText;
        appendToMainCanvas = false;
        centeredX = false;
        centeredY = false;
        font = (Font)Resources.GetBuiltinResource(typeof(Font), "Arial.ttf");
        fontSize = 20;
        color = Color.green;
        texture = null;
        fontPro = Resources.Load<TextMeshProFont>(CAssets.HORROR_FONT);
        size = new CVector2D();
        layer = null;
    }

    public CVector3D pos;
    public string id;
    public string text;
    public bool appendToMainCanvas;
    public bool centeredX;
    public bool centeredY;
    public Font font;
    public TextMeshProFont fontPro;
    public int fontSize;
    public Color color;
    public CVector2D size;
    public Texture texture;

    public CLayer layer;
}


public class CCanvas : CGenericGraphic
{

    private GameObject mMainCanvas = new GameObject();
    private List<GameObject> mPileOfUselessGameObjects = new List<GameObject>();

    Dictionary<string, Component> mGuiItems = new Dictionary<string, Component>();

    private static CCanvas mInst;
    private RectTransform mRec;



    public CCanvas()
    {
        if (CVars.inst().getCanvas() != null)
        {
            throw new Exception("You can only instantiate the canvas once");
        }
        setGraphic(CHelper.createAndGetCanvas());

        mMainCanvas = getGraphic();
        mMainCanvas.name = "-Main Canvas-";
        var canvas = mMainCanvas.GetComponent<Canvas>();
        mRec = mMainCanvas.GetComponent<RectTransform>();
        mRec.sizeDelta = new Vector2(CCamera.inst().getWidth(), CCamera.inst().getHeight());
        var vec = Vector3.zero;
        var layer = CLayer.getLayer(ELayer.Hud);
        vec.z = layer.Z;
        setLayer(layer);
        mMainCanvas.transform.position = vec;
        canvas.renderMode = RenderMode.WorldSpace;
        CVars.inst().setCanvas(this);


        mInst = this;
    }

    public static CCanvas inst()
    {
        return mInst;
    }

    public TextMeshProUGUI addSmartLabel(SHudProperties aProps)
    {
        GameObject holder = CHelper.Instantiate(Resources.Load<GameObject>("Prefabs/label")) as GameObject;
        mPileOfUselessGameObjects.Add(holder);
        var cnvas = holder.AddComponent<Canvas>();

        var text = holder.AddComponent<TextMeshProUGUI>();

        text.text = aProps.text;
        text.fontSize = aProps.fontSize;

        text.font = aProps.fontPro;
        text.color = aProps.color;
        text.name = aProps.text;
        text.enableWordWrapping = true;
        text.ForceMeshUpdate();

        /* if (aProps.material != null)
             text.material = aProps.material;*/

        var rec = text.GetComponent<RectTransform>();
        rec.sizeDelta = aProps.size.toVec2();

        var p = aProps.pos.toVec3();


        if (aProps.centeredX)
        {

        }

        if (!aProps.appendToMainCanvas)
        {
            //the pivot of the canvas is in the middle so we have to do this to fix it
            p.y -= (float)aProps.size.y;
            p.x -= (float)aProps.size.x;

            p.y *= -1;

            if (aProps.layer != null)
                p.z = aProps.layer.Z;
            else
                p.z = getLayer().Z;


            text.transform.position = p;
        }
        if (aProps.appendToMainCanvas)
        {
            text.transform.parent = mMainCanvas.transform;

            p = aProps.pos.toVec3();

            if (aProps.layer != null)
                p.z = aProps.layer.Z;
            else
                p.z = getLayer().Z;

            p.y -= (float)CCamera.inst().getPivot().y;
            p.y *= -1;
            p.x -= (float)CCamera.inst().getPivot().x;
            //this is done because if the canvas was moved, the position within the canvas is not calculated properly by unity
            p.y += mMainCanvas.transform.position.y;
            p.x += mMainCanvas.transform.position.x;

            text.transform.position = p;
        }

        mGuiItems.Add(aProps.id, text);
        return text;
    }

    public Text addLabel(SHudProperties aProps)
    {
        GameObject holder = new GameObject();
        mPileOfUselessGameObjects.Add(holder);
        var cnvas = holder.AddComponent<Canvas>();

        var text = holder.AddComponent(typeof(Text)) as Text;
        text.text = aProps.text;
        text.font = aProps.font;
        text.fontSize = aProps.fontSize;
        text.color = aProps.color;
        text.name = aProps.text;
        text.resizeTextForBestFit = true;


        var p = aProps.pos.toVec3();


        if (aProps.centeredX)
        {

        }

        if (!aProps.appendToMainCanvas)
        {
            p.y *= -1;
            p.z = getLayer().Z;
            text.transform.position = p;
        }
        if (aProps.appendToMainCanvas)
        {
            text.transform.parent = mMainCanvas.transform;

            p = aProps.pos.toVec3();
            p.z = getLayer().Z;
            p.y -= (float)CCamera.inst().getPivot().y;
            p.y *= -1;
            p.x -= (float)CCamera.inst().getPivot().x;
            //this is done because if the canvas was moved, the position within the canvas is not calculated properly by unity
            p.y += mMainCanvas.transform.position.y;
            p.x += mMainCanvas.transform.position.x;

            text.transform.position = p;
        }

        mGuiItems.Add(aProps.id, text);
        return text;
    }


    public UnityEngine.UI.Button addButton(CVector3D aPos, string aId, string aText, bool appendToMainCanvas = false)
    {
        var resource = Resources.Load<GameObject>("Prefabs/btnPrefab");
        var copy = CHelper.Instantiate(resource) as GameObject;
        var canvas = copy.GetComponent<Canvas>();
        canvas.renderMode = RenderMode.WorldSpace;
        Button btn = copy.GetComponentInChildren<Button>();

        var text = copy.GetComponentInChildren<Text>();
        text.text = aText;
        //Remove the existing events
        btn.onClick.RemoveAllListeners();
        //Add your new event

        //Get the event trigger attached to the UI object
        EventTrigger eventTrigger = copy.GetComponentInChildren<EventTrigger>();

        //Create a new entry. This entry will describe the kind of event we're looking for
        // and how to respond to it
        EventTrigger.Entry entry = new EventTrigger.Entry();

        //This event will respond to a drop event
        entry.eventID = EventTriggerType.Drop;

        //Create a new trigger to hold our callback methods
        entry.callback = new EventTrigger.TriggerEvent();

        //Create a new UnityAction, it contains our DropEventMethod delegate to respond to events
        UnityEngine.Events.UnityAction<BaseEventData> callback =
            new UnityEngine.Events.UnityAction<BaseEventData>(DropEventMethod);

        //Add our callback to the listeners
        entry.callback.AddListener(callback);

        //Add the EventTrigger entry to the event trigger component
        eventTrigger.delegates.Add(entry);


        var p = aPos.toVec3();
        if (!appendToMainCanvas)
        {
            p.y *= -1;
            p.z = getLayer().Z;
            btn.transform.position = p;
        }
        if (appendToMainCanvas)
        {
            btn.transform.parent = mMainCanvas.transform;

            p = aPos.toVec3();
            p.z = getLayer().Z;
            p.y -= (float)CCamera.inst().getPivot().y;
            p.y *= -1;
            p.x -= (float)CCamera.inst().getPivot().x;
            //this is done because if the canvas was moved, the position within the canvas is not calculated properly by unity
            p.y += mMainCanvas.transform.position.y;
            p.x += mMainCanvas.transform.position.x;

            btn.transform.position = p;
        }

        mGuiItems.Add(aId, btn);



        return btn;
    }



    public UnityEngine.UI.InputField addTextBox(CVector3D aPos, string aId, string aText, bool appendToMainCanvas = false, bool aCentered = false, Font aFont = null)
    {
        GameObject holder = new GameObject();
        mPileOfUselessGameObjects.Add(holder);
        var canvas = holder.AddComponent<Canvas>();

        var input = holder.AddComponent(typeof(InputField)) as InputField;


        var p = aPos.toVec3();
        p.y *= -1;
        p.z = -10;
        input.transform.position = p;

        mGuiItems.Add(aId, input);

        if (appendToMainCanvas)
        {
            //holder.transform.parent = mMainCanvas.transform;
            canvas.transform.parent = mMainCanvas.transform;
        }

        return input;
    }

    public void DropEventMethod(UnityEngine.EventSystems.BaseEventData baseEvent)
    {
        Debug.Log(baseEvent.selectedObject.name + " triggered an event!");
    }

    public delegate void EventDelegate(UnityEngine.EventSystems.BaseEventData baseEvent);


    public T getGuiItem<T>(string aId) where T : Component
    {
        if (mGuiItems.ContainsKey(aId))
        {
            Component uiItem;
            mGuiItems.TryGetValue(aId, out uiItem);

            return uiItem as T;
        }
        else
        {
            return null;
        }
    }
    public void removeGuiItem(string aId)
    {
        if (mGuiItems.ContainsKey(aId))
        {
            mGuiItems.Remove(aId);
        }
    }


    public void copyTextToClipBoard(string aText)
    {
        TextEditor te = new TextEditor();
        te.content = new GUIContent(aText);
        te.SelectAll();
        te.Copy();
    }



    public override void update()
    {

    }

    public override void render()
    {
        mRec = mMainCanvas.GetComponent<RectTransform>();
        mRec.sizeDelta = new Vector2(CCamera.inst().getWidth(), CCamera.inst().getHeight());

        var cam = CCamera.inst();
        Vector3 pos = cam.getPos().toVec3();
        pos.y *= -1;
        pos.z = getLayer().Z;
        mMainCanvas.transform.position = pos;
    }
}
