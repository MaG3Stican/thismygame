﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class CLight : CGenericGraphic
{

    private float start;
    private float end;
    private float t = 0.0f;
    private bool mFade = false;

    private bool mGrowing = false;
    private Light mLight;
    public CLight(CVector3D aPos, uint aRange)
    {

        setShape(EShapes.CIRCLE);

        setRadious((uint)(aRange * 0.5));

        setGraphic((GameObject)CHelper.Instantiate(Resources.Load<GameObject>("Prefabs/pointLight")));

        setPivot(Convert.ToInt32(CGameConstants.TILE_WIDTH * 0.5), Convert.ToInt32(CGameConstants.TILE_HEIGHT * 0.5));


        mMoveToCenter = true;

        mLight = getGraphic().GetComponent<Light>();

        getGraphic().name = "Light";
        mLight.range = aRange;
        mLight.intensity = 0.74f;

        setPos(aPos);

        setLayer(CLayer.getLayer(ELayer.Light));


        activate();

        moveToCenter();


        base.update();
        base.render();

    }


    public void fade()
    {

    }

    public void stopFade()
    {
    }

    public override void deactivate()
    {
        base.deactivate();
    }




    public override void update()
    {
        //base.update();
    }

    public override void render()
    {


        if (mLight.intensity > 0.34 && mGrowing == false)
        {
            mLight.intensity -= 0.01f;
        }
        else if (mLight.intensity < 0.34)
        {
            mGrowing = true;
        }
        if (mLight.intensity < 0.74 && mGrowing == true)
        {
            mLight.intensity += 0.01f;
        }
        else if (mLight.intensity > 0.74)
        {
            mGrowing = false;
        }



        stopMove();

        //base.render();

    }


    public override void destroy()
    {
        base.destroy();
    }

    public static GameObject CastLight(bool aFilled, int aRadious, EShapes aShape, CVector2D aPosition)
    {

        var cameraPosition = -10;
        var obj = CHelper.getMeshed2DReadyObject("circle", "Sprites/prefabs/meshedObject");
        float thetaScale = 0.09f;
        float points = (float)(3 * Math.PI) / thetaScale;

        var numSegments = 0;
        if (aShape == EShapes.CIRCLE)
        {
            numSegments = 80;
        }
        else if (aShape == EShapes.RECT)
        {
            numSegments = 4;
        }
        else if (aShape == EShapes.TRIANGLE)
        {
            numSegments = 3;
        }
        int radiousSize = aRadious;
        LineRenderer lineRenderer = obj.GetComponent<LineRenderer>();

        if (!aFilled)
        {
            Color c1 = new Color(0.5f, 0.5f, 0.5f, 1f);
            lineRenderer.SetVertexCount((int)points);

            lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
            lineRenderer.SetColors(c1, c1);
            lineRenderer.SetWidth(0.05f, 0.05f);
            lineRenderer.SetVertexCount(numSegments + 1);
        }

        float deltaTheta = (float)((2.0 * Mathf.PI) / numSegments);
        float theta = 0;

        MeshFilter meshFilter = obj.GetComponent<MeshFilter>();

        MeshRenderer renderer = (MeshRenderer)meshFilter.renderer;


        renderer.material = Resources.Load("Materials/Additive", typeof(Material)) as Material;


        //TEXTURE SETTINGS
        int texWidth = aRadious * 2;
        int texHeight = aRadious * 2;
        //MASK SETTINGS
        float maskThreshold = 2.0f;
        //REFERENCES
        //Texture2D mask;
        //mask = new Texture2D(texWidth, texHeight, TextureFormat.RGBA32, true);
        //Vector2 maskCenter = new Vector2((float)(texWidth * 0.5), (float)(texHeight * 0.5));
        //for (int y = 0; y < aRadious * 2; ++y)
        //{
        //    for (int x = 0; x < aRadious * 2; ++x)
        //    {

        //        float distFromCenter = Vector2.Distance(maskCenter, new Vector2(x, y));
        //        float maskPixel = (float)((0.5 - (distFromCenter / texWidth)) * maskThreshold);
        //        mask.SetPixel(x, y, new Color(maskPixel, maskPixel, maskPixel, 1.0f));
        //    }
        //}
        //mask.Apply();

        Texture2D a = new Texture2D(2, 1);
        a.SetPixel(1, 1, Color.black);
        a.SetPixel(2, 1, Color.red);
        a.filterMode = FilterMode.Bilinear;
        a.wrapMode = TextureWrapMode.Clamp;
        a.Apply();

        renderer.material.SetTexture(1233, a);



        Mesh mesh = new Mesh();
        mesh = meshFilter.mesh;
        mesh.Clear();

        var vertices = new List<Vector3>();
        var triangles = new List<int>();
        var uv = new List<Vector2>();
        var normals = new List<Vector3>();

        Vector3 center = new Vector3();


        center.z = cameraPosition;

        for (int i = 0; i < numSegments; i++)
        {
            float x = radiousSize * Mathf.Cos(theta);
            float y = radiousSize * Mathf.Sin(theta);
            Vector3 pos = new Vector3(x, y, cameraPosition);


            if (!aFilled)
                lineRenderer.SetPosition(i, pos);

            //get future point
            var thetaHolder = theta + deltaTheta;
            float xx = radiousSize * Mathf.Cos(thetaHolder);
            float yy = radiousSize * Mathf.Sin(thetaHolder);
            Vector3 pospos = new Vector3(xx, yy, cameraPosition);
            //triangle
            vertices.Add(pos);
            vertices.Add(center);
            vertices.Add(pospos);

            theta += deltaTheta;
        }


        aPosition = aPosition.opposite();
        obj.transform.position = new Vector3((float)aPosition.x, (float)aPosition.y, cameraPosition);


        System.Random rand = new System.Random();

        int tri = 0;

        foreach (var v in vertices)
        {
            triangles.Add(tri);
            tri++;
        }

        vertices.ForEach(b => uv.Add(new Vector2(rand.Next(2), rand.Next(2))));

        var normalized = vertices[vertices.Count - 1].normalized;
        normalized *= -1;
        vertices.ForEach(b => normals.Add(normalized));


        mesh.vertices = new Vector3[vertices.Count];
        mesh.vertices = vertices.ToArray();

        mesh.uv = new Vector2[uv.Count];
        mesh.uv = uv.ToArray();

        mesh.triangles = new int[triangles.Count];
        mesh.triangles = triangles.ToArray();

        mesh.normals = new Vector3[normals.Count];
        mesh.normals = normals.ToArray();

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
        mesh.Optimize();
        meshFilter.mesh = mesh;




        return obj;
    }

}

