
using System;
using System.Collections.Generic;
using UnityEngine;

public class CLayer
{

    public static uint masterLayer = 0;
    public float Z = 0;
    public ELayer mName;
    private static bool initialized = false;
    private static List<CLayer> layers = new List<CLayer>();

    public CLayer()
    {

    }

    public static void init()
    {

    }

    //TODO : the layer index should be given in the position they are added
    public static CLayer addLayer(ELayer aLayer = ELayer.None, bool noMaster = false)
    {
        if (!initialized)
        {
            init();
        }
        CLayer layer = new CLayer();

        layers.Add(layer);
        layer.mName = aLayer;
        layer.Z = (int)aLayer;

        return layer;
    }

    public static void refreshLayers()
    {
    }

    public void removeLayer()
    {

    }

    public static CLayer getLayer(ELayer name)
    {
        foreach (CLayer lay in CLayer.layers)
        {
            if (lay.mName == name)
            {
                return lay;
            }
        }
        //if it does not exist, lets add it
        var result = addLayer(name);

        return result;
    }
}








