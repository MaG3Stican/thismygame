using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
    public int maxHealth = 100;
    public int currentHealth = 95;

	private const float marginTopProportion = 0.07F;
	private const float marginLeftProportion = 0.02F;
	private const float maxWidthProportion = 0.5F;
	private const int height = 24;
	
	
    private Rect renderingRect;

	void Start(){
	}
	
	// Update is called once per frame
	void Update () {
		AdjustHealth(0);
	}

    void OnGUI()
    {
        string msg = string.Format("{0}/{1}", currentHealth, maxHealth);
    	GUI.Box(renderingRect, msg);
    }
	
    void AdjustHealth(int adjustment)
    {
        currentHealth += adjustment;
		if(currentHealth < 0)
			currentHealth  = 0;
		if(maxHealth < 0)
			maxHealth = 1;
		
		float marginLeft = Screen.height * marginLeftProportion;
        float marginTop = Screen.height *  marginTopProportion;
        float width = ((float)currentHealth / maxHealth) * Screen.width * maxWidthProportion;
        renderingRect = new Rect(marginLeft, marginTop, width, height);
    }
}
