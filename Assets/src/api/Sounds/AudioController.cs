using UnityEngine;
using System.Collections;
public enum AudioCategory
{
		Silence,
		Steps,
	    Runs
	    
}


public class AudioController
{
		AudioClip audioClip;
		public AudioSource audioSource;
	    public AudioCategory currentlyPlaying;

		public AudioController (AudioType pAudioType, AudioSource pAudioSource)
		{
				
		}

	public AudioController (AudioSource pSource)
		{
		audioSource = pSource;
		}

		// Use this for initialization
		void Start ()
		{
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}

		public void PlaySteps ()
		{
				if (!audioSource.isPlaying) {
						audioClip = AudioSteps.getStepClip;
						audioSource.clip = audioClip;
						audioSource.Play ();
				}
				currentlyPlaying = AudioCategory.Steps;
		        
		}
		public void PlayRuns ()
		{
				if (!audioSource.isPlaying) {
						audioClip = Runs.getRunClip;
						audioSource.clip = audioClip;
						audioSource.Play ();
				}
				currentlyPlaying = AudioCategory.Runs;
		
		}
	public void StopCurrentAudio ()
		{
				if (audioSource.isPlaying) {
			
						audioSource.Stop ();
				}
		}

}
