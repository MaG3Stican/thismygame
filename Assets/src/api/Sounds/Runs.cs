﻿using UnityEngine;
using System.Collections;

public class Runs : MonoBehaviour {

	static AudioClip clip;
	
	public static AudioClip getRunClip
	{
		get { 
			if (clip == null)
			{
				clip = (AudioClip) Resources.Load("Audio/Running01");
				return clip;
			}
			else{
				return clip;
			}
		}
	}
}
