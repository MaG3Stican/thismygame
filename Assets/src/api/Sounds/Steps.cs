﻿using UnityEngine;
using System.Collections;

public class AudioSteps
{
	static AudioClip clip;

		public static AudioClip getStepClip
	    {
		get { 
			if (clip == null)
			{
				clip = (AudioClip) Resources.Load("Audio/Steps01");
				return clip;
			}
			else{
				return clip;
			}
			}
	    }


}
