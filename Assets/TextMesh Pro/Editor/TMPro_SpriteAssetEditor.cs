// Copyright (C) 2014 Stephan Bouchard - All Rights Reserved
// This code can only be used under the standard Unity Asset Store End User License Agreement
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms


using UnityEngine;
using UnityEditor;
using System.Collections;
//using System.Collections.Generic;



namespace TMPro.EditorUtilities
{

    [CustomEditor(typeof(SpriteAsset))]
    public class TMPro_SpriteAssetEditor : Editor
    {
        private struct UI_PanelState
        {
            public static bool spriteAssetInfoPanel = true;
            public static bool spriteInfoPanel = false;          
        }

        private int m_page = 0;


        private const string k_UndoRedo = "UndoRedoPerformed";

        private GUISkin mySkin;
        //private GUIStyle SquareAreaBox85G;
        private GUIStyle GroupLabel;
        private GUIStyle SectionLabel;

        private SerializedProperty m_spriteAtlas_prop;
        private SerializedProperty m_spriteInfoList_prop;

        private bool isAssetDirty = false;
      
        private string[] uiStateLabel = new string[] { "<i>(Click to expand)</i>", "<i>(Click to collapse)</i>" };
        

        public void OnEnable()
        {
            m_spriteAtlas_prop = serializedObject.FindProperty("spriteSheet");
            m_spriteInfoList_prop = serializedObject.FindProperty("spriteInfoList");           

            // Find to location of the TextMesh Pro Asset Folder (as users may have moved it)
            string tmproAssetFolderPath = TMPro_EditorUtility.GetAssetLocation();      

            // GUI Skin 
            if (EditorGUIUtility.isProSkin)
                mySkin = AssetDatabase.LoadAssetAtPath(tmproAssetFolderPath + "/GUISkins/TMPro_DarkSkin.guiskin", typeof(GUISkin)) as GUISkin;
            else
                mySkin = AssetDatabase.LoadAssetAtPath(tmproAssetFolderPath + "/GUISkins/TMPro_LightSkin.guiskin", typeof(GUISkin)) as GUISkin;

            if (mySkin != null)
            {
                SectionLabel = mySkin.FindStyle("Section Label");
                GroupLabel = mySkin.FindStyle("Group Label");
                //SquareAreaBox85G = mySkin.FindStyle("Square Area Box (85 Grey)");
            }            
        }


        public override void OnInspectorGUI()
        {

            //Debug.Log("OnInspectorGUI Called.");
            Event evt = Event.current;
            string evt_cmd = evt.commandName; // Get Current Event CommandName to check for Undo Events

            serializedObject.Update();

            EditorGUIUtility.labelWidth = 135;

            // HEADER
            GUILayout.Label("<b>TextMeshPro - Sprite Asset</b>", SectionLabel);


            // TEXTMESHPRO SPRITE INFO PANEL
            GUILayout.Label("Sprite Info", SectionLabel);
            EditorGUI.indentLevel = 1;

            GUI.enabled = false; // Lock UI
      
            EditorGUILayout.PropertyField(m_spriteAtlas_prop , new GUIContent("Sprite Atlas"));

            // SPRITE LIST                                      
            GUI.enabled = true; // Unlock UI 
            GUILayout.Space(10);
            EditorGUI.indentLevel = 0;

           
            if (GUILayout.Button("Sprite List\t\t" + (UI_PanelState.spriteInfoPanel ? uiStateLabel[1] : uiStateLabel[0]), SectionLabel))
                UI_PanelState.spriteInfoPanel = !UI_PanelState.spriteInfoPanel;

            if (UI_PanelState.spriteInfoPanel)
            {
                int arraySize = m_spriteInfoList_prop.arraySize;
                int itemsPerPage = (Screen.height - 240) / 80;              

                if (arraySize > 0)
                {                  
                    // Display each SpriteInfo entry using the SpriteInfo property drawer.                                                                                      
                    for (int i = itemsPerPage * m_page; i < arraySize && i < itemsPerPage * (m_page + 1); i++)
                    {                      
                        EditorGUILayout.BeginVertical(GroupLabel, GUILayout.Height(70));
                        
                        SerializedProperty spriteInfo = m_spriteInfoList_prop.GetArrayElementAtIndex(i);
                        EditorGUI.BeginChangeCheck();
                        EditorGUILayout.PropertyField(spriteInfo);
                        EditorGUILayout.EndVertical();                                    
                    }              
                }

                int shiftMultiplier = evt.shift ? 10 : 1; // Page + Shift goes 10 page forward
                
                Rect pagePos = EditorGUILayout.GetControlRect(false, 20);
                pagePos.width /= 3;

                // Previous Page
                if (m_page > 0) GUI.enabled = true;
                else GUI.enabled = false;

                if (GUI.Button(pagePos, "Previous"))
                    m_page -= 1 * shiftMultiplier;

                // PAGE COUNTER
                GUI.enabled = true;
                pagePos.x += pagePos.width;
                GUI.Label(pagePos, "Page " + (m_page + 1) + " / " + ((arraySize / itemsPerPage) + 1), GUI.skin.button);

                // Next Page
                pagePos.x += pagePos.width;
                if (itemsPerPage * (m_page + 1) < arraySize) GUI.enabled = true;
                else GUI.enabled = false;
                           
                if (GUI.Button(pagePos, "Next"))
                    m_page += 1 * shiftMultiplier;

                // Clamp page range
                m_page = Mathf.Clamp(m_page, 0, arraySize / itemsPerPage);           
            }

            
            if (serializedObject.ApplyModifiedProperties() || evt_cmd == k_UndoRedo || isAssetDirty)
            {
                //Debug.Log("Serialized properties have changed.");
                //TMPro_EventManager.ON_FONT_PROPERTY_CHANGED(true, m_fontAsset);

                isAssetDirty = false;
                EditorUtility.SetDirty(target);
                //TMPro_EditorUtility.RepaintAll(); // Consider SetDirty
            }

        }
    }
}