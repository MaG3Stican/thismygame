Shader "Custom/nolight2d"
{
 Properties
 {
  [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
  _NormalsTex ("Sprite Normals", 2D) = "bump" {}
  _CelRamp ("Cel shading ramp", 2D) = "white" {}
  _Color ("Tint", Color) = (1,1,1,1)
  [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
 }
 
 SubShader
 {
  Tags
  { 
   "Queue"="Transparent"
   "IgnoreProjector"="False"
   "RenderType"="Transparent"
   "PreviewType"="Plane"
   "CanUseSpriteAtlas"="True"
  }
 
  Cull Off
  Lighting On
  ZWrite Off
  Fog { Mode Off }
  Blend SrcAlpha OneMinusSrcAlpha
 
  CGPROGRAM
  #pragma surface surf CustomLambert alpha vertex:vert
  #pragma multi_compile DUMMY PIXELSNAP_ON
 
  sampler2D _MainTex;
  sampler2D _NormalsTex;
  sampler2D _CelRamp;
  fixed4 _Color;
 
  struct Input
  {
   float2 uv_MainTex;
   fixed4 color;
  };
 
  void surf (Input IN, inout SurfaceOutput o)
  {
   fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * IN.color;
   o.Albedo = c.rgb;
   o.Normal = UnpackNormal (tex2D (_NormalsTex, IN.uv_MainTex));
   o.Alpha = c.a;
  }
  ENDCG
 }
 
 Fallback "Transparent/VertexLit"
}