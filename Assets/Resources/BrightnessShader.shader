Shader "PostProcessing/Brightness" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "" {}
		_Brightness ("Brightness", Range (-1.0,1.0)) = 0
	}
	SubShader {
		Pass {
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			uniform float _Brightness;
			sampler2D _MainTex;
			
			struct v2f {
				float4 pos : POSITION;
				half2 uv : TEXCOORD0;
			};
			
			v2f vert( appdata_img v ) 
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.texcoord.xy;
				return o;
			} 
			
			float4 frag(v2f i): COLOR {
				fixed4 color = tex2D(_MainTex, i.uv); 
				color.rgb += _Brightness;	
				return color;		
			}
			
			ENDCG
		}
	} 
	
	Fallback "Diffuse"
}
